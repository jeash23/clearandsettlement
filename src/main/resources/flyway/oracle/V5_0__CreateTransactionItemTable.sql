CREATE TABLE SETTLE_INTERFACE_FILE (
  RECORD_ID VARCHAR2(255) NOT NULL,
  ORIGINAL_FILENAME VARCHAR2(255) NOT NULL,
  STORAGE_DIRECTORY VARCHAR2(255) NOT NULL,
  STORAGE_FILENAME VARCHAR2(255) NOT NULL,
  FILE_CHECKSUM VARCHAR2(255),
  ACQUIRED_TIMESTAMP TIMESTAMP NOT NULL,
  PROCESSED_TX_COUNT INTEGER DEFAULT 0,
  DECLARED_TX_COUNT INTEGER DEFAULT 0,
  CALCULATED_TX_AMOUNT DECIMAL,
  DECLARED_TX_AMOUNT DECIMAL,
  PROCESSING_STATE VARCHAR2(20) NOT NULL,
  PRIMARY KEY(RECORD_ID)
);

CREATE TABLE SETTLE_PURSE_COUNTER (
  CUSTUMER_UID VARCHAR2(20) NOT NULL,
  CURRENT_COUNTER INTEGER NOT NULL,
  LAST_UPDATED TIMESTAMP NOT NULL,
  PRIMARY KEY(CUSTUMER_UID)
);

CREATE TABLE SETTLE_OFFLINE_TRX (
  RECORD_ID VARCHAR2(255) NOT NULL,
  CUSTOMER_UID VARCHAR2(20) NOT NULL,
  CUSTOMER_MSISDN VARCHAR2(20),
  MERCHANT_ID VARCHAR2(100) NOT NULL,
  MERCHANT_SHORTCODE VARCHAR2(100),
  TERMINAL_ID VARCHAR2(100) NOT NULL,
  TRANSACTION_ID VARCHAR2(255) NOT NULL,
  TRANSACTION_AMOUNT DECIMAL NOT NULL,
  TRANSACTION_DATE TIMESTAMP NOT NULL,
  LAST_BALANCE DECIMAL NOT NULL,
  PURSE_COUNTER INTEGER NOT NULL,
  LOADED_DATE TIMESTAMP NOT NULL,
  SETTLEMENT_FILE_ID VARCHAR2(255),
  IS_SETTLED NUMBER(1) DEFAULT 0,
  SETTLEMENT_DATE TIMESTAMP,
  PRIMARY KEY(RECORD_ID)
);

CREATE TABLE SETTLE_MERCHANT_SHORT_CODE (
  MERCHANT_ID VARCHAR2(100) NOT NULL,
  SHORT_CODE VARCHAR2(100) NOT NULL,
  MERCHANT_NAME VARCHAR2(255),
  PRIMARY KEY(MERCHANT_ID, SHORT_CODE)
);

INSERT ALL
  INTO SETTLE_MERCHANT_SHORT_CODE(MERCHANT_ID, SHORT_CODE, MERCHANT_NAME) VALUES ('55555', 'transjakarta', 'Transajakarta')
  INTO SETTLE_MERCHANT_SHORT_CODE(MERCHANT_ID, SHORT_CODE, MERCHANT_NAME) VALUES ('55556', 'transjogjakarta', 'Transjogjakarta')
SELECT 1 FROM DUAL;

CREATE TABLE SETTLE_SETTLEMENT_ITEM_SUMMARY (
  RECORD_ID VARCHAR2(255) NOT NULL,
  SETTLEMENT_FILE_ID VARCHAR2(255) NOT NULL,
  MERCHANT_ID VARCHAR2(100) NOT NULL,
  MERCHANT_SHORT_CODE VARCHAR2(100) NOT NULL,
  MERCHANT_NAME VARCHAR2(100),
  TRANSACTION_COUNT INTEGER NOT NULL,
  TRANSACTION_AMOUNT DECIMAL NOT NULL,
  SUMMARIZED_TIMESTAMP TIMESTAMP NOT NULL,
  SETTLEMENT_STATUS VARCHAR2(15) DEFAULT 'NEW',
  SETTLEMENT_TIMESTAMP TIMESTAMP,
  PRIMARY KEY(RECORD_ID)
);