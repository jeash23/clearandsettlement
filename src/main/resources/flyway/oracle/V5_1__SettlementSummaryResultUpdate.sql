-- Add column to store push summary result.
ALTER TABLE SETTLE_SETTLEMENT_ITEM_SUMMARY ADD (SETTLEMENT_RESULT_TYPE INTEGER);
ALTER TABLE SETTLE_SETTLEMENT_ITEM_SUMMARY ADD (SETTLEMENT_RESULT_CODE VARCHAR2(20));
ALTER TABLE SETTLE_SETTLEMENT_ITEM_SUMMARY ADD (SETTLEMENT_RESULT_DESC VARCHAR2(255));
ALTER TABLE SETTLE_SETTLEMENT_ITEM_SUMMARY ADD (SETTLEMENT_RESULT_TRX_ID VARCHAR2(20));