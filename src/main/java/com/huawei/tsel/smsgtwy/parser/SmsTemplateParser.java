package com.huawei.tsel.smsgtwy.parser;

import com.huawei.tsel.smsgtwy.dto.SmsTemplate;

import java.util.Map;

/**
 * <p>Contract for object responsible for extracting content of {@link com.huawei.tsel.smsgtwy.dto.SmsMessage}.
 * i.e replacing all placeholders in template.
 *
 * @author zakyalvan
 * @since 20 Jan 2017
 */
public interface SmsTemplateParser {
    /**
     * Build message content.
     *
     * @param template
     * @param parameters
     * @return
     */
    String buildContent(SmsTemplate template, Map<String, Object> parameters);
}
