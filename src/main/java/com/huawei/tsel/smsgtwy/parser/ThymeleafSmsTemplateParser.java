package com.huawei.tsel.smsgtwy.parser;

import java.util.Map;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring4.processor.attr.SpringSelectFieldAttrProcessor;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;

import com.huawei.tsel.smsgtwy.dto.SmsTemplate;

/**
 * 
 * @author zakyalvan
 * @since 15 Aug 2017
 */
public class ThymeleafSmsTemplateParser implements SmsTemplateParser {
	
	
	private final TemplateEngine templateEngine;
	
	public ThymeleafSmsTemplateParser() {
		templateEngine = new TemplateEngine();
		
		SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
		
		templateEngine.setTemplateResolver(templateResolver);
	}
	
	@Override
	public String buildContent(SmsTemplate template, Map<String, Object> parameters) {
		return null;
	}
}
