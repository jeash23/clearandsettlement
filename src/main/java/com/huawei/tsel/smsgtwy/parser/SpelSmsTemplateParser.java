package com.huawei.tsel.smsgtwy.parser;

import com.huawei.tsel.smsgtwy.dto.SmsTemplate;
import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.number.NumberStyleFormatter;
import org.springframework.format.support.FormattingConversionService;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Default implementation of {@link SmsTemplateParser}, using SpEL for parsing message.
 *
 * @author zakyalvan
 * @since 20 Jan 2017
 */
public class SpelSmsTemplateParser implements SmsTemplateParser {
    private final FormattingConversionService conversionService;

    public SpelSmsTemplateParser() {
        conversionService = new FormattingConversionService();
        conversionService.addFormatter(new DateFormatter("dd MMM yyyy HH:mm"));
        conversionService.addFormatter(new NumberStyleFormatter("#,###.##"));
    }

    @Override
    public String buildContent(SmsTemplate template, Map<String, Object> parameters) {
        ExpressionParser parser = new SpelExpressionParser();
        TemplateParserContext parserContext = new TemplateParserContext(template.getPlaceholderPrefix(), template.getPlaceholderSuffix());
        Expression expression = parser.parseExpression(template.getContent(), parserContext);

        Map<String, String> formattedParameters = new HashMap<>();
        for (String key : parameters.keySet()) {
            Object parameter = parameters.get(key);
            String formattedParameter = (parameter != null) ? conversionService.convert(parameter, String.class) : ":null:";
            formattedParameters.put(key, formattedParameter);
        }

        StandardEvaluationContext evalContext = new StandardEvaluationContext(formattedParameters);
        evalContext.addPropertyAccessor(new MapAccessor());
        return expression.getValue(evalContext, String.class);
    }
}
