package com.huawei.tsel.smsgtwy.flow;

import com.huawei.tsel.smsgtwy.dto.SmsDelivery;
import com.huawei.tsel.smsgtwy.dto.SmsMessage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.messaging.MessageChannel;

/**
 * <p>Integration channels for delivery sms messages.
 *
 * @author zakyalvan
 * @since 16 Mar 2017
 */
@Configuration
public class SmsGatewayChannels {
    /**
     * A {@link MessageChannel} to be used as request channel for sms delivery process.
     *
     * @return
     */
    @Bean(SmsGatewaySettings.SMS_DELIVERY_REQUEST_CHANNEL)
    public MessageChannel deliveryRequest() {
        return MessageChannels.direct().datatype(SmsMessage.class).get();
    }

    /**
     * A {@link MessageChannel} to be used as reply channel for sms delivery process.
     *
     * @return
     */
    @Bean(SmsGatewaySettings.SMS_DELIVERY_REPLY_CHANNEL)
    public MessageChannel deliveryReply() {
        return MessageChannels.direct().datatype(SmsDelivery.class).get();
    }

    /**
     * A {@link MessageChannel} where {@link org.springframework.messaging.support.ErrorMessage} related to
     * sms delivery should be routed by default.
     *
     * @return
     */
    @Bean(SmsGatewaySettings.SMS_DELIVERY_ERROR_CHANNEL)
    public PublishSubscribeChannel deliveryError() {
        return MessageChannels.publishSubscribe().datatype(Throwable.class).get();
    }
}
