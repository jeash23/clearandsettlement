package com.huawei.tsel.smsgtwy.flow;

import com.huawei.tsel.smsgtwy.dto.SmsMessage;

/**
 * Exception to be thrown when attempt to send sms is failed.
 *
 * @author zakyalvan
 * @since 20 Mar 2017
 */
@Deprecated
public class FailedDeliveryResponseException extends SmsDeliveryException {
    public FailedDeliveryResponseException(SmsMessage sms) {
        super(sms);
    }

    public FailedDeliveryResponseException(String message, SmsMessage sms) {
        super(message, sms);
    }

    public FailedDeliveryResponseException(String message, SmsMessage sms, Throwable cause) {
        super(message, sms, cause);
    }
}