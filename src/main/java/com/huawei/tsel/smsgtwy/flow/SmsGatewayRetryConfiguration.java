package com.huawei.tsel.smsgtwy.flow;

import com.huawei.tsel.common.integration.RequestRetrySettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.handler.advice.RequestHandlerRetryAdvice;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.BackOffPolicy;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.backoff.NoBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author zakyalvan
 * @since 18 May 2017
 */
@Configuration
public class SmsGatewayRetryConfiguration {
    @Autowired
    private SmsGatewaySettings gatewaySettings;

    /**
     * <p>A {@link RequestHandlerRetryAdvice} type to be attached on http outbound gateway for
     * checking btpn-tcash customer linkage.
     *
     * @return
     */
    @Bean("smsGatewayRetryAdvice")
    RequestHandlerRetryAdvice smsGatewayRetryAdvice() {
        RequestHandlerRetryAdvice retryAdvice = new RequestHandlerRetryAdvice();
        retryAdvice.setRetryTemplate(smsGatewayRetryTemplate());
        return retryAdvice;
    }

    @Bean
    RetryTemplate smsGatewayRetryTemplate() {
        RetryTemplate retryTemplate = new RetryTemplate();
        retryTemplate.setRetryPolicy(smsGatewayRetryPolicy());
        retryTemplate.setBackOffPolicy(smsGatewayBackoffPolicy());

        /**
         * Throw original exception instead of default {@link org.springframework.retry.ExhaustedRetryException}
         * exception type.
         */
        retryTemplate.setThrowLastExceptionOnExhausted(true);
        return retryTemplate;
    }

    @Bean
    RetryPolicy smsGatewayRetryPolicy() {
        Map<Class<? extends Throwable>, Boolean> retryableExceptions = new HashMap<>();
        for(Class<? extends Throwable> retryableException : gatewaySettings.getRetry().getRetryableExceptions()) {
            retryableExceptions.put(retryableException, true);
        }
        for(Class<? extends Throwable> unretryableException : gatewaySettings.getRetry().getUnretryableExceptions()) {
            retryableExceptions.put(unretryableException, false);
        }

        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(gatewaySettings.getRetry().getMaxAttempts(), retryableExceptions, gatewaySettings.getRetry().isTraverseExceptionCause());
        return retryPolicy;
    }

    /**
     * <p>Backoff policy of retry process.
     *
     * @return
     */
    @Bean
    BackOffPolicy smsGatewayBackoffPolicy() {
        if(gatewaySettings.getRetry().getBackOffType() == RequestRetrySettings.RetryBackoff.EXPONENTIAL) {
            ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
            backOffPolicy.setInitialInterval(gatewaySettings.getRetry().getPeriodMillis());
            backOffPolicy.setMultiplier(gatewaySettings.getRetry().getBackOffMultiplier());
            return backOffPolicy;
        }
        else if(gatewaySettings.getRetry().getBackOffType() == RequestRetrySettings.RetryBackoff.FIXED) {
            FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
            backOffPolicy.setBackOffPeriod(gatewaySettings.getRetry().getPeriodMillis());
            return backOffPolicy;
        }
        else {
            NoBackOffPolicy backOffPolicy = new NoBackOffPolicy();
            return backOffPolicy;
        }
    }
}
