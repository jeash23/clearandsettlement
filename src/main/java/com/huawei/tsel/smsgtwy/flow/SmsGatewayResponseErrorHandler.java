package com.huawei.tsel.smsgtwy.flow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * <p>Handle error notification body from sms gateway server, i.e. with content "NotValidClient" and "Failed".
 * This {@link org.springframework.web.client.ResponseErrorHandler} will throw {@link SmsGatewayResponseException}
 * in case error other than status code error.
 *
 * @author zakyalvan
 * @since 30 Mar 2017
 */
class SmsGatewayResponseErrorHandler extends DefaultResponseErrorHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(SmsGatewayResponseErrorHandler.class);

    private boolean statusCodeError = false;

    private ResponseErrorType errorType;

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        if(statusCodeError = super.hasError(response)) {
            return true;
        }

        byte[] responseBytes = getResponseBody(response);
        if(responseBytes.length == 0) {
            LOGGER.error("Response body length is zero byte, classified as invalid response");
            errorType = ResponseErrorType.EMPTY_RESPONSE_BODY;
            return true;
        }

        Charset responseCharset = getCharset(response);
        String responseString = new String(responseBytes, responseCharset != null ? responseCharset : StandardCharsets.UTF_8);

        LOGGER.trace("Response body from sms gateway server '{}'", responseString);
        if(responseString.trim().equalsIgnoreCase("NotValidClient")) {
            LOGGER.error("Not a valid client.");
            errorType = ResponseErrorType.NOT_VALID_CLIENT;
            return true;
        }
        else if(responseString.trim().equalsIgnoreCase("Failed.")) {
            LOGGER.error("Sms delivery failed.");
            errorType = ResponseErrorType.DELIVERY_FAILED;
            return true;
        }

        LOGGER.trace("No error on response of sms message request to sms gateway server");
        return false;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        LOGGER.trace("Handle error of invocation of sms gateway address");
        if(statusCodeError) {
            super.handleError(response);
            return;
        }

        if(errorType != null) {
            throw new SmsGatewayResponseException(response, errorType);
        }
    }

    /**
     * An enumeration related to error which can happen on requesting to send sms.
     */
    public enum ResponseErrorType {
        EMPTY_RESPONSE_BODY("Empty body received as notification, or can not read sms gateway notification body"),
        NOT_VALID_CLIENT("Invalid client trying to send sms message"),
        DELIVERY_FAILED("Sms delivery failed");

        private String message;
        ResponseErrorType(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}
