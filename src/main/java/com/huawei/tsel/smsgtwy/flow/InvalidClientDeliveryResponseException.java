package com.huawei.tsel.smsgtwy.flow;

import com.huawei.tsel.smsgtwy.dto.SmsMessage;

/**
 * <p>Exception to be thrown when 'NotValidClient' got from sms gateway.
 *
 * @author zakyalvan
 * @since 20 Mar 2017
 */
@Deprecated
@SuppressWarnings("Serial")
public class InvalidClientDeliveryResponseException extends SmsDeliveryException {
    public InvalidClientDeliveryResponseException(SmsMessage sms) {
        super(sms);
    }

    public InvalidClientDeliveryResponseException(String message, SmsMessage sms) {
        super(message, sms);
    }

    public InvalidClientDeliveryResponseException(String message, SmsMessage sms, Throwable cause) {
        super(message, sms, cause);
    }
}