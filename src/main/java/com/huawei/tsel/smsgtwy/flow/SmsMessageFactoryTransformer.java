package com.huawei.tsel.smsgtwy.flow;

import com.huawei.tsel.smsgtwy.dto.SmsMessage;
import com.huawei.tsel.smsgtwy.dto.SmsTemplate;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

/**
 * <p>A {@link GenericTransformer} which responsible to create a {@link SmsMessage} from any source object.
 * You need to provide all parameters defined on constructor.
 *
 * @author zakyalvan
 * @since 23 Mar 2017
 */
public class SmsMessageFactoryTransformer<S> implements GenericTransformer<S, SmsMessage> {
    private final Function<S, String> destinationFunction;
    private final Function<S, String> senderFunction;
    private final Function<S, String> templateFunction;
    private final Function<S, Map<String, Object>> parametersFunction;

    public SmsMessageFactoryTransformer(Function<S, String> destinationFunction, Function<S, String> senderFunction, Function<S, String> templateFunction, Function<S, Map<String, Object>> parametersFunction) {
        this.destinationFunction = Objects.requireNonNull(destinationFunction);
        this.senderFunction = Objects.requireNonNull(senderFunction);
        this.templateFunction = Objects.requireNonNull(templateFunction);
        this.parametersFunction = Objects.requireNonNull(parametersFunction);
    }

    @Override
    public SmsMessage transform(S source) {
        Assert.notNull(source, "Source parameter must not be null");
        return new SmsMessage(senderFunction.apply(source), destinationFunction.apply(source), new SmsTemplate(templateFunction.apply(source)), parametersFunction.apply(source));
    }
}
