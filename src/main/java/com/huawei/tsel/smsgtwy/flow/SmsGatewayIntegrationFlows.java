package com.huawei.tsel.smsgtwy.flow;

import com.huawei.tsel.smsgtwy.dto.SmsDelivery;
import com.huawei.tsel.smsgtwy.dto.SmsMessage;
import com.huawei.tsel.smsgtwy.dto.SmsTemplate;
import com.huawei.tsel.smsgtwy.parser.SmsTemplateParser;
import com.huawei.tsel.smsgtwy.parser.SpelSmsTemplateParser;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.handler.advice.RequestHandlerRetryAdvice;
import org.springframework.integration.http.outbound.HttpRequestExecutingMessageHandler;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.MessagingException;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>Sms delivery configuration. This refactoring to be stand alone components because
 * more than one components requiring it.
 *
 * @author zakyalvan
 * @since 16 Mar 2017
 */
@Configuration
public class SmsGatewayIntegrationFlows {
    private static final Logger LOGGER = LoggerFactory.getLogger(SmsGatewayIntegrationFlows.class);

    private static final String CACHED_SMS_MESSAGE = "OriginalSms";

    @Autowired
    private SmsGatewaySettings gatewaySettings;

    @Autowired
    private SmsGatewayChannels gatewayChannels;

    @Autowired
    @Qualifier("smsGatewayRetryAdvice")
    private RequestHandlerRetryAdvice gatewayRetryAdvice;

    @Bean
    SmsTemplateParser smsMessageParser() {
        SpelSmsTemplateParser builder = new SpelSmsTemplateParser();
        return builder;
    }

    /**
     * <p>A {@link RestTemplate} to be used on send request to sms gateway. Please note,
     * we are using {@link BufferingClientHttpRequestFactory} to enable reading notification body
     * multiple times (including in {@link SmsGatewayResponseErrorHandler error handler}).
     *
     * @return
     */
    @Bean
    RestTemplate deliverSmsRestTemplate() {
        RestTemplate restTemplate = new RestTemplate() {
            @Override
            public void setRequestFactory(ClientHttpRequestFactory requestFactory) {
                if(requestFactory instanceof BufferingClientHttpRequestFactory) {
                    super.setRequestFactory(requestFactory);
                }
                else {
                    super.setRequestFactory(new BufferingClientHttpRequestFactory(requestFactory));
                }
            }
        };
        restTemplate.setRequestFactory(deliverSmsHttpRequestFactory());
        restTemplate.setErrorHandler(new SmsGatewayResponseErrorHandler());
        return restTemplate;
    }

    @Bean
    OkHttp3ClientHttpRequestFactory deliverSmsHttpRequestFactory() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .retryOnConnectionFailure(false)
                .hostnameVerifier((hostname, session) -> {
                    LOGGER.warn("FIXME : Https host name verification bypassed...");
                    return true;
                });

        OkHttp3ClientHttpRequestFactory requestFactory = new OkHttp3ClientHttpRequestFactory(builder.build());
        return requestFactory;
    }

    /**
     * <p>Message handler which responsible for invoking sms gateway using http.
     * This message handler require {@link org.springframework.messaging.Message} with payload {@link SmsMessage}
     * must return message with payload {@link SmsDelivery}.
     *
     * @return
     */
    @Bean
    HttpRequestExecutingMessageHandler deliverSmsHandler() {
        String smsGatewayUri = gatewaySettings.getEndpoint().getAddress().toUriComponentsBuilder()
                .queryParam("user", gatewaySettings.getEndpoint().getUsername())
                .queryParam("pass", gatewaySettings.getEndpoint().getPassword())
                .queryParam("text", "{content}")
                .queryParam("from", "{sender}")
                .queryParam("to", "{destination}")
                .build().toUriString();

        HttpRequestExecutingMessageHandler handler = Http.outboundGateway(smsGatewayUri, deliverSmsRestTemplate())
                .encodeUri(true)
                .httpMethod(HttpMethod.GET)
                .expectedResponseType(String.class)
                .<SmsMessage> uriVariablesFunction(message -> {
                    Map<String, Object> variables = new HashMap<>();

                    String customSender = message.getPayload().getSender();
                    variables.put("sender", StringUtils.hasText(customSender) ? customSender : gatewaySettings.getEndpoint().getDefaultSender());
                    variables.put("destination", message.getPayload().getDestination());

                    SmsMessage sms = message.getPayload();

                    SmsTemplate template = sms.getTemplate();
                    if(template.hasPlacehoders()) {
                        Map<String, Object> parameters = sms.getParameters();
                        String content = smsMessageParser().buildContent(template, parameters);
                        variables.put("content", content);
                    }
                    else {
                        variables.put("content", template.getContent());
                    }

                    return variables;
                })
                .get();

        if(gatewaySettings.getRetry().isEnabled()) {
            LOGGER.info("Retry of sms gateway invocation is enabled, set retry advice for http outbound gateway");
            handler.setAdviceChain(Arrays.asList(gatewayRetryAdvice));
        }

        return handler;
    }

    /**
     * <p>Integration flow for sending sms message. Please note, this {@link IntegrationFlow}
     * input channel requiring {@link SmsMessage} payload type and resulting {@link SmsDelivery}
     * on its output channel.
     *
     * @return
     */
    @Bean
    IntegrationFlow smsDeliveryFlow() {
        return flow -> flow.channel(gatewayChannels.deliveryRequest())
                .gateway(deliveryMainFlow(), specs -> specs.errorChannel(gatewayChannels.deliveryError())
                        .replyChannel(gatewayChannels.deliveryReply()));
    }

    @Bean
    IntegrationFlow deliveryMainFlow() {
        return flow -> flow.enrich(specs -> specs.header(MessageHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE)
                        .headerFunction(CACHED_SMS_MESSAGE, message -> message.getPayload()))
                .handle(deliverSmsHandler())
                .<String> handle((payload, headers) -> {
                    LOGGER.trace("Send sms request sent to gateway with response body : {}", payload.trim());
                    return new SmsDelivery(SmsDelivery.DeliveryStatus.SUCCESS, new Date(), (SmsMessage) headers.get(CACHED_SMS_MESSAGE));
                });
    }

    @Bean
    IntegrationFlow deliveryErrorFlow() {
        return flow -> flow.channel(gatewayChannels.deliveryError())
                .<MessagingException> handle((payload, headers) -> {
                    LOGGER.error("Handle sms message delivery error", payload);
                    return new SmsDelivery(SmsDelivery.DeliveryStatus.FAILED, new Date(), (SmsMessage) payload.getFailedMessage().getPayload());
                });
    }
}
