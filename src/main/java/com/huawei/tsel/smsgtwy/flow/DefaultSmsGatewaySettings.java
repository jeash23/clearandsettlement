package com.huawei.tsel.smsgtwy.flow;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 
 * @author zakyalvan
 * @since 5 Feb 2017
 * @deprecated Deprecated in favor of {@link SmsGatewaySettings}.
 */
@Primary
@Component
@Deprecated
@ConfigurationProperties(prefix="huawei.default.sms-gateway")
@SuppressWarnings("serial")
public class DefaultSmsGatewaySettings implements Serializable {
	@Valid
	@NotNull
	@NestedConfigurationProperty
	protected EndpointAddress endpoint = new EndpointAddress("http", "10.2.224.148", 30002, "cgi/bin/sendsms");

	@NotBlank
	protected String username = "mfs";
	
	@NotBlank
	protected String password = "mfs123";
	
	@NotBlank
	protected String defaultSender = "2828";
	
	public EndpointAddress getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(EndpointAddress endpoint) {
		this.endpoint = endpoint;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getDefaultSender() {
		return defaultSender;
	}
	public void setDefaultSender(String defaultSender) {
		this.defaultSender = defaultSender;
	}

	/**
	 * Endpoint server address configuration.
	 */
	public static class EndpointAddress implements Serializable {
		@NotBlank
		private String scheme;
		
		@NotBlank
		private String host;
		
		@NotNull
		private Integer port;
		
		@NotBlank
		private String path;
		
		public EndpointAddress() {}
		public EndpointAddress(String scheme, String host, Integer port, String path) {
			this.scheme = scheme;
			this.host = host;
			this.port = port;
			this.path = path;
		}

		public String getScheme() {
			return scheme;
		}
		public void setScheme(String scheme) {
			this.scheme = scheme;
		}
		
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		
		public Integer getPort() {
			return port;
		}
		public void setPort(Integer port) {
			this.port = port;
		}
		
		public String getPath() {
			return path;
		}
		public void setPath(String path) {
			this.path = path;
		}
		
		/**
		 * Create {@link UriComponentsBuilder} based on provided setting values.
		 * 
		 * @return
		 */
		public UriComponentsBuilder createUriBuilder() {
			return UriComponentsBuilder.newInstance()
					.scheme(scheme).host(host).port(port).path(path);
		}
	}
}
