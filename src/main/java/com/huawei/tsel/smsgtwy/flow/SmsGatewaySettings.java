package com.huawei.tsel.smsgtwy.flow;

import com.huawei.tsel.common.integration.EndpointAddress;
import com.huawei.tsel.common.integration.RequestRetrySettings;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>New configuration for sms gateway.
 *
 * @author zakyalvan
 * @since 20 Mar 2017
 */
@Component
@Validated
@ConfigurationProperties(prefix = "huawei.sms-gateway")
@Data
@SuppressWarnings("serial")
public class SmsGatewaySettings implements Serializable {
    public static final String SMS_DELIVERY_REQUEST_CHANNEL = "MessageChannel.deliveryRequest";
    public static final String SMS_DELIVERY_REPLY_CHANNEL = "MessageChannel.deliveryReply";
    public static final String SMS_DELIVERY_ERROR_CHANNEL = "MessageChannel.deliveryError";

    @Valid
    @NotNull
    @NestedConfigurationProperty
    private EndpointAccessSettings endpoint = new EndpointAccessSettings();

    @Valid
    @NotNull
    @NestedConfigurationProperty
    private RequestRetrySettings retry = new RequestRetrySettings();

    /**
     * Endpoint settings.
     */
    @Data
    public static class EndpointAccessSettings {
        @NotBlank
        protected String username = "mfs";

        @NotBlank
        protected String password = "mfs123";

        @NotBlank
        protected String defaultSender = "TCASH";

        @Valid
        @NotNull
        EndpointAddress address = new EndpointAddress("http", "10.2.224.148", 30002, "cgi/bin/sendsms");;
    }
}
