package com.huawei.tsel.smsgtwy.flow;

import com.huawei.tsel.smsgtwy.dto.SmsMessage;
import lombok.Getter;

import java.util.Objects;

/**
 * <p>Exception to be thrown when delivery of sms message is failed.
 *
 * @author zakyalvan
 * @since 16 Mar 2017
 */
@SuppressWarnings("serial")
public abstract class SmsDeliveryException extends RuntimeException {
    private static final String DEFAULT_MESSAGE = "Sending sms message failed";

    /**
     * Sms message which failed to sent.
     */
    @Getter
    private final SmsMessage sms;

    public SmsDeliveryException(SmsMessage sms) {
        super(DEFAULT_MESSAGE);
        this.sms = Objects.requireNonNull(sms);
    }

    public SmsDeliveryException(String message, SmsMessage sms) {
        super(message);
        this.sms = Objects.requireNonNull(sms);
    }

    public SmsDeliveryException(String message, SmsMessage sms, Throwable cause) {
        super(message, cause);
        this.sms = Objects.requireNonNull(sms);
    }
}
