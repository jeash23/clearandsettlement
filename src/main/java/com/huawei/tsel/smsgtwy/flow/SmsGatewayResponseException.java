package com.huawei.tsel.smsgtwy.flow;

import org.springframework.http.client.ClientHttpResponse;

import java.util.Objects;

/**
 * <p>Exception to be thrown when error of notification body error. Normally, this exception type
 * will be thrown by {@link SmsGatewayResponseErrorHandler} in case error type defined in {@link com.huawei.tsel.smsgtwy.flow.SmsGatewayResponseErrorHandler.ResponseErrorType}.
 *
 * @author zakyalvan
 * @since 30 Mar 2017
 * @see SmsGatewayResponseErrorHandler
 */
public class SmsGatewayResponseException extends RuntimeException {
    private final ClientHttpResponse response;
    private final SmsGatewayResponseErrorHandler.ResponseErrorType errorType;

    public SmsGatewayResponseException(ClientHttpResponse response, SmsGatewayResponseErrorHandler.ResponseErrorType errorType) {
        super(Objects.requireNonNull(errorType).getMessage());
        this.response = Objects.requireNonNull(response);
        this.errorType = Objects.requireNonNull(errorType);
    }

    public SmsGatewayResponseException(ClientHttpResponse response, SmsGatewayResponseErrorHandler.ResponseErrorType errorType, Throwable cause) {
        super(Objects.requireNonNull(errorType).getMessage(), cause);
        this.response = Objects.requireNonNull(response);
        this.errorType = Objects.requireNonNull(errorType);
    }

    public ClientHttpResponse getResponse() {
        return response;
    }

    public SmsGatewayResponseErrorHandler.ResponseErrorType getErrorType() {
        return errorType;
    }
}