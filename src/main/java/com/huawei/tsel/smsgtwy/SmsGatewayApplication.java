package com.huawei.tsel.smsgtwy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.annotation.IntegrationComponentScan;

/**
 * @author zakyalvan
 * @since 17 Mar 2017
 */
@SpringBootApplication
@IntegrationComponentScan
public class SmsGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(SmsGatewayApplication.class);
    }
}
