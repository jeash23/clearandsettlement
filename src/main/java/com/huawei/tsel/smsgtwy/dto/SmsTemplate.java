package com.huawei.tsel.smsgtwy.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Template of sms.
 * 
 * @author zakyalvan
 * @since 3 Feb 2017
 */
@SuppressWarnings("serial")
public class SmsTemplate implements Serializable {
	public static final String DEFAULT_PLACEHOLDER_PREFIX = "<";
	public static final String DEFAULT_PLACEHOLDER_SUFFIX = ">";

	private final String placeholderPrefix;
	private final String placeholderSuffix;
	
	private boolean encodeContent = true;
	
	/**
	 * Template content.
	 */
	@NotBlank
	private String content;
	
	/**
	 * Template placeholders.
	 */
	private Set<String> placehoders;
	
	// Default constructor.
	public SmsTemplate() {
		this(null, DEFAULT_PLACEHOLDER_PREFIX, DEFAULT_PLACEHOLDER_SUFFIX);
	}
	@JsonCreator
	public SmsTemplate(@JsonProperty("content") String content) {
		this(content, DEFAULT_PLACEHOLDER_PREFIX, DEFAULT_PLACEHOLDER_SUFFIX);
	}
	@JsonCreator
	public SmsTemplate(@JsonProperty("content") String content, @JsonProperty("placeholderPrefix") String placeholderPrefix, @JsonProperty("placeholderSuffix") String placeholderSuffix) {
		this.placeholderPrefix = placeholderPrefix;
		this.placeholderSuffix = placeholderSuffix;
		setContent(content);
	}

	public String getPlaceholderPrefix() {
		return placeholderPrefix;
	}
	public String getPlaceholderSuffix() {
		return placeholderSuffix;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
		processContent();
	}
	
	public Set<String> getPlacehoders() {
		return placehoders;
	}
	
	public boolean hasPlacehoders() {
		return placehoders != null && placehoders.size() > 0;
	}
	
	private void processContent() {
		Set<String> templatePlaceholders = new HashSet<>();
		
		if(!StringUtils.hasText(content)) {
			return;
		}
		
		String placeholderPattern = placeholderPrefix.concat("(.*?)").concat(placeholderSuffix);
		Pattern pattern = Pattern.compile(placeholderPattern, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(content);
	    
		String normalizedContent = content;
		while(matcher.find()) {
	        String placeholder = matcher.group(1);
	        normalizedContent = normalizedContent.replace(placeholder, placeholder.toUpperCase());
			templatePlaceholders.add(placeholder.toUpperCase());
	    }
		
		this.content = normalizedContent;
		this.placehoders = Collections.unmodifiableSet(templatePlaceholders);
	}

	@Override
	public String toString() {
		return "SmsTemplate [placeholderPrefix=" + placeholderPrefix + ", placeholderSuffix=" + placeholderSuffix
				+ ", content=" + content + ", placehoders=" + placehoders + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((placehoders == null) ? 0 : placehoders.hashCode());
		result = prime * result + ((placeholderPrefix == null) ? 0 : placeholderPrefix.hashCode());
		result = prime * result + ((placeholderSuffix == null) ? 0 : placeholderSuffix.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmsTemplate other = (SmsTemplate) obj;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (placehoders == null) {
			if (other.placehoders != null)
				return false;
		} else if (!placehoders.equals(other.placehoders))
			return false;
		if (placeholderPrefix == null) {
			if (other.placeholderPrefix != null)
				return false;
		} else if (!placeholderPrefix.equals(other.placeholderPrefix))
			return false;
		if (placeholderSuffix == null) {
			if (other.placeholderSuffix != null)
				return false;
		} else if (!placeholderSuffix.equals(other.placeholderSuffix))
			return false;
		return true;
	}
}
