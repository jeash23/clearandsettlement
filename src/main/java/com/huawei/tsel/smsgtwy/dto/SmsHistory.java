package com.huawei.tsel.smsgtwy.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>A Data transfer object representing history of sms delivery.
 *
 * @author zakyalvan
 * @since 22 May 2017
 */
@Data
@SuppressWarnings("serial")
public class SmsHistory implements Serializable {
    @NotNull
    private Long id;

    /**
     * <p>First time attempt on sending sms delivery.
     */
    @NotNull
    private LocalDateTime firstAttempt;

    /**
     * <p>Last attempt of sms delivery.
     */
    private LocalDateTime lastAttempt;

    /**
     * <p>Attempt count on sending sms message.
     */
    private Integer attemptCount;

    /**
     * <p>Flag whether sms delivery is success or not.
     */
    private boolean success = false;

    /**
     * <p>Destination of sms.
     */
    private String destination;

    /**
     * <p>Sender or sender masking.
     */
    private String sender;

    /**
     * Content of sms sent.
     */
    private String content;
}
