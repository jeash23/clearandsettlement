package com.huawei.tsel.smsgtwy.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

/**
 * <p>Type represent sms message, which required by flow defined in {@link com.huawei.tsel.smsgtwy.flow.SmsGatewayIntegrationFlows}.
 *
 * @author zakyalvan
 * @since 20 Jan 2017
 */
@Builder(builderMethodName = "create", buildMethodName = "get")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
@ToString
@EqualsAndHashCode
@SuppressWarnings("serial")
public class SmsMessage implements Serializable {
    private final String sender;

    @NotBlank
    String destination;

    @NotNull
    SmsTemplate template;

    @NotNull
    Map<String, Object> parameters;

    /**
     * Timestamp of sms this message.
     */
    LocalDateTime timestamp = LocalDateTime.now();

    public SmsMessage(String destination, SmsTemplate template) {
        this.sender = null;
        this.destination = destination;
        this.template = template;
        this.parameters = Collections.emptyMap();
    }

    public SmsMessage(String sender, String destination, SmsTemplate template) {
        this.sender = sender;
        this.destination = destination;
        this.template = template;
        this.parameters = Collections.emptyMap();
    }

    public SmsMessage(String destination, SmsTemplate template, Map<String, Object> parameters) {
        this.sender = null;
        this.destination = destination;
        this.template = template;
        this.parameters = parameters;
    }

    public SmsMessage(String sender, String destination, SmsTemplate template, Map<String, Object> parameters) {
        this.sender = sender;
        this.destination = destination;
        this.template = template;
        this.parameters = parameters;
    }
}