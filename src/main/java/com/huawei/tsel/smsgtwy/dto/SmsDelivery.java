package com.huawei.tsel.smsgtwy.dto;

import lombok.Value;

import java.io.Serializable;
import java.util.Date;

/**
 * Delivery information or status of sms sending process.
 *
 * @author zakyalvan
 * @since 20 Jan 2017
 */
@Value
@SuppressWarnings("serial")
public class SmsDelivery implements Serializable {
    DeliveryStatus status;
    Date attemptTimestamp;
    SmsMessage sentMessage;

    /**
     * Sms delivery status.
     */
    public static enum DeliveryStatus {
        SUCCESS, FAILED
    }
}
