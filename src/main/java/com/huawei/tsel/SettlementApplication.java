package com.huawei.tsel;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Main class for launching clear and settlement application, also initializing
 * web container in case deployment on dedicated application server</p>
 *
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@SpringBootApplication
@IntegrationComponentScan
@EnableBatchProcessing
public class SettlementApplication extends SpringBootServletInitializer {
    private boolean dedicatedContainerDeployment = true;

    public static void main(String[] args) {
        SettlementApplication application = new SettlementApplication();
        application.dedicatedContainerDeployment = false;
        SpringApplicationBuilder builder = new SpringApplicationBuilder();
        application.configure(builder).run(args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        builder.sources(SettlementApplication.class);

        Map<String, Object> properties = new HashMap<>();
        if (dedicatedContainerDeployment) {
            properties.put("spring.config.location", "file:${user.home}/config/settlement.properties");
        }
        builder.properties(properties);
        return builder;
    }

    @Bean
    @Primary
    LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        return validator;
    }

    @Bean
    MethodValidationPostProcessor methodValidation() {
        MethodValidationPostProcessor methodValidation = new MethodValidationPostProcessor();
        return methodValidation;
    }
}
