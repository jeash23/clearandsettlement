//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.09.25 at 06:00:31 PM WIB 
//


package com.huawei.tsel.cpsasync.model.result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Query Reminder Schedule的查询结果
 * 
 * <p>Java class for QueryReminderScheduleResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryReminderScheduleResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReminderScheduleInfo" type="{http://cps.huawei.com/synccpsinterface/common}ReminderScheduleInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryReminderScheduleResult", propOrder = {
    "reminderScheduleInfo"
})
public class QueryReminderScheduleResult
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ReminderScheduleInfo")
    protected List<ReminderScheduleInfo> reminderScheduleInfo;

    /**
     * Gets the value of the reminderScheduleInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reminderScheduleInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReminderScheduleInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReminderScheduleInfo }
     * 
     * 
     */
    public List<ReminderScheduleInfo> getReminderScheduleInfo() {
        if (reminderScheduleInfo == null) {
            reminderScheduleInfo = new ArrayList<ReminderScheduleInfo>();
        }
        return this.reminderScheduleInfo;
    }

}
