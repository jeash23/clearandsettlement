//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.09.25 at 06:00:22 PM WIB 
//


package com.huawei.tsel.cpsasync.model.response;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.tsel.cpsasync.model.response package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.tsel.cpsasync.model.response
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GroupMemberRoleListType }
     * 
     */
    public GroupMemberRoleListType createGroupMemberRoleListType() {
        return new GroupMemberRoleListType();
    }

    /**
     * Create an instance of {@link Response }
     * 
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link Response.Header }
     * 
     */
    public Response.Header createResponseHeader() {
        return new Response.Header();
    }

    /**
     * Create an instance of {@link ParameterType }
     * 
     */
    public ParameterType createParameterType() {
        return new ParameterType();
    }

    /**
     * Create an instance of {@link KYCFieldType }
     * 
     */
    public KYCFieldType createKYCFieldType() {
        return new KYCFieldType();
    }

    /**
     * Create an instance of {@link SimpleKYCDataType }
     * 
     */
    public SimpleKYCDataType createSimpleKYCDataType() {
        return new SimpleKYCDataType();
    }

    /**
     * Create an instance of {@link IDRecordType }
     * 
     */
    public IDRecordType createIDRecordType() {
        return new IDRecordType();
    }

    /**
     * Create an instance of {@link IDDetailsDataType }
     * 
     */
    public IDDetailsDataType createIDDetailsDataType() {
        return new IDDetailsDataType();
    }

    /**
     * Create an instance of {@link ContactRecordType }
     * 
     */
    public ContactRecordType createContactRecordType() {
        return new ContactRecordType();
    }

    /**
     * Create an instance of {@link ContactDetailsDataType }
     * 
     */
    public ContactDetailsDataType createContactDetailsDataType() {
        return new ContactDetailsDataType();
    }

    /**
     * Create an instance of {@link CertificateRecordType }
     * 
     */
    public CertificateRecordType createCertificateRecordType() {
        return new CertificateRecordType();
    }

    /**
     * Create an instance of {@link CertificateDetailsDataType }
     * 
     */
    public CertificateDetailsDataType createCertificateDetailsDataType() {
        return new CertificateDetailsDataType();
    }

    /**
     * Create an instance of {@link BankAccountRecordType }
     * 
     */
    public BankAccountRecordType createBankAccountRecordType() {
        return new BankAccountRecordType();
    }

    /**
     * Create an instance of {@link BankAccountDetailsDataType }
     * 
     */
    public BankAccountDetailsDataType createBankAccountDetailsDataType() {
        return new BankAccountDetailsDataType();
    }

    /**
     * Create an instance of {@link AuthenticationType }
     * 
     */
    public AuthenticationType createAuthenticationType() {
        return new AuthenticationType();
    }

    /**
     * Create an instance of {@link AuthenticationDataType }
     * 
     */
    public AuthenticationDataType createAuthenticationDataType() {
        return new AuthenticationDataType();
    }

    /**
     * Create an instance of {@link RoleType }
     * 
     */
    public RoleType createRoleType() {
        return new RoleType();
    }

    /**
     * Create an instance of {@link RoleDataType }
     * 
     */
    public RoleDataType createRoleDataType() {
        return new RoleDataType();
    }

    /**
     * Create an instance of {@link CustomerBeneficiary }
     * 
     */
    public CustomerBeneficiary createCustomerBeneficiary() {
        return new CustomerBeneficiary();
    }

    /**
     * Create an instance of {@link OrganisationBeneficiary }
     * 
     */
    public OrganisationBeneficiary createOrganisationBeneficiary() {
        return new OrganisationBeneficiary();
    }

    /**
     * Create an instance of {@link GroupBeneficiary }
     * 
     */
    public GroupBeneficiary createGroupBeneficiary() {
        return new GroupBeneficiary();
    }

    /**
     * Create an instance of {@link DirectDebitMandateInfo }
     * 
     */
    public DirectDebitMandateInfo createDirectDebitMandateInfo() {
        return new DirectDebitMandateInfo();
    }

    /**
     * Create an instance of {@link IdentifierDef }
     * 
     */
    public IdentifierDef createIdentifierDef() {
        return new IdentifierDef();
    }

    /**
     * Create an instance of {@link IdentifierTypeValue }
     * 
     */
    public IdentifierTypeValue createIdentifierTypeValue() {
        return new IdentifierTypeValue();
    }

    /**
     * Create an instance of {@link PrimaryParty }
     * 
     */
    public PrimaryParty createPrimaryParty() {
        return new PrimaryParty();
    }

    /**
     * Create an instance of {@link ReceiverParty }
     * 
     */
    public ReceiverParty createReceiverParty() {
        return new ReceiverParty();
    }

    /**
     * Create an instance of {@link SimpleProperty }
     * 
     */
    public SimpleProperty createSimpleProperty() {
        return new SimpleProperty();
    }

    /**
     * Create an instance of {@link ReminderScheduleInfo }
     * 
     */
    public ReminderScheduleInfo createReminderScheduleInfo() {
        return new ReminderScheduleInfo();
    }

    /**
     * Create an instance of {@link GroupMemberRoleListType.RoleItem }
     * 
     */
    public GroupMemberRoleListType.RoleItem createGroupMemberRoleListTypeRoleItem() {
        return new GroupMemberRoleListType.RoleItem();
    }

    /**
     * Create an instance of {@link Response.Body }
     * 
     */
    public Response.Body createResponseBody() {
        return new Response.Body();
    }

    /**
     * Create an instance of {@link Response.Header.HeaderExtension }
     * 
     */
    public Response.Header.HeaderExtension createResponseHeaderHeaderExtension() {
        return new Response.Header.HeaderExtension();
    }

}
