//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.09.25 at 06:00:20 PM WIB 
//


package com.huawei.tsel.cpsasync.model.request;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Organization的BankAccountRecordCondition数据
 * 
 * <p>Java class for BankAccountRecordConditionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankAccountRecordConditionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Condition" type="{http://cps.huawei.com/synccpsinterface/common}BankAccountRecordType"/&gt;
 *         &lt;element name="Record" type="{http://cps.huawei.com/synccpsinterface/common}BankAccountRecordType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankAccountRecordConditionType", propOrder = {
    "condition",
    "record"
})
public class BankAccountRecordConditionType
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Condition", required = true)
    protected BankAccountRecordType condition;
    @XmlElement(name = "Record", required = true)
    protected BankAccountRecordType record;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link BankAccountRecordType }
     *     
     */
    public BankAccountRecordType getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankAccountRecordType }
     *     
     */
    public void setCondition(BankAccountRecordType value) {
        this.condition = value;
    }

    /**
     * Gets the value of the record property.
     * 
     * @return
     *     possible object is
     *     {@link BankAccountRecordType }
     *     
     */
    public BankAccountRecordType getRecord() {
        return record;
    }

    /**
     * Sets the value of the record property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankAccountRecordType }
     *     
     */
    public void setRecord(BankAccountRecordType value) {
        this.record = value;
    }

}
