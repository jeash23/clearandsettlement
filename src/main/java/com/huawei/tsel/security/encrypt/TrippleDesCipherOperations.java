package com.huawei.tsel.security.encrypt;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotNull;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * 
 * @author zakyalvan
 * @since 14 Jan 2017
 */
@Validated
@Component("tripple-des")
@ConfigurationProperties(prefix="huawei.security.encryption.key-file.tripple-des")
public class TrippleDesCipherOperations implements CipherOperations, InitializingBean {
	private static final Logger LOGGER = LoggerFactory.getLogger(TrippleDesCipherOperations.class);

	@NotNull
	private Resource secretKeyFile = new ClassPathResource("key.3des.dat");

	@NotBlank
	private String transformation = "DESede/ECB/PKCS5Padding";

	private SecretKeySpec secretKey;

	public Resource getSecretKeyFile() {
		return secretKeyFile;
	}
	public void setSecretKeyFile(Resource secretKeyFile) {
		this.secretKeyFile = secretKeyFile;
	}

	public String getTransformation() {
		return transformation;
	}
	public void setTransformation(String transformation) {
		this.transformation = transformation;
	}

	@Override
	public byte[] encrypt(byte[] raw) throws CipherOperationException {
		LOGGER.trace("Encrypt raw bytes using 3des cipher");
		try {
			Cipher cipher = Cipher.getInstance(transformation);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return cipher.doFinal(raw);
		}
		catch(Exception e) {
			throw new CipherOperationException(e);
		}
	}

	@Override
	public byte[] decrypt(byte[] encrypted) throws CipherOperationException {
		LOGGER.trace("Decrypt encrypted bytes using 3des cipher");
		try {
			Cipher cipher = Cipher.getInstance(transformation);
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return cipher.doFinal(encrypted);
		}
		catch (Exception e) {
			throw new CipherOperationException(e);
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		LOGGER.debug("Validate key and initialize secret key");
		Assert.isTrue(secretKeyFile.exists(), "Given key file is not found");

		try(InputStream keyStream = new BufferedInputStream(secretKeyFile.getInputStream())) {
			byte[] keyBytes = new byte[(int) (Files.size(secretKeyFile.getFile().toPath()))];
			keyStream.read(keyBytes);
			secretKey = new SecretKeySpec(keyBytes, "DESede");
		}
	}
}
