package com.huawei.tsel.security.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * 
 * @author zakyalvan
 * @since 14 Jan 2017
 */
@SuppressWarnings("serial")
public class Credentials implements Serializable {
	private String username;
	private char[] password;

	public Credentials() {}
	@JsonCreator
	public Credentials(@JsonProperty("username") String username, @JsonProperty("password") char[] password) {
		this.username = Objects.requireNonNull(username);
		this.password = Objects.requireNonNull(password);
	}

	public String getUsername() {
		return username;
	}
	void setUsername(String username) {
		this.username = username;
	}

	public char[] getPassword() {
		return password;
	}
	void setPassword(char[] password) {
		this.password = password;
	}
	void setPassword(String password) {
		this.password = password.toCharArray();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(password);
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Credentials other = (Credentials) obj;
		if (!Arrays.equals(password, other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
}