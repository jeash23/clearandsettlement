package com.huawei.tsel.security.user;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Contract for managing user of TEP webservice endpoint.
 * 
 * @author zakyalvan
 * @since 14 Jan 2017
 */
public interface CredentialsRepository {
	void put(@Validated @NotNull Credentials user);
	Credentials get(@NotBlank String username);
	boolean has(String username);
	Collection<Credentials> all();
}
