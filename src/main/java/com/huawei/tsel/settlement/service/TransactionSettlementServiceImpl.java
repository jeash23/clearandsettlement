package com.huawei.tsel.settlement.service;

import com.huawei.tsel.settlement.entity.*;
import com.huawei.tsel.settlement.entity.InterfaceFile.ProcessingState;
import com.huawei.tsel.settlement.entity.SettlementSummary.SettlementStatus;
import com.huawei.tsel.settlement.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import java.io.File;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

/**
 * <p>Default implementation of {@link TransactionSettlementService}.
 *
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@Service
@Validated
@Transactional(readOnly = true)
public class TransactionSettlementServiceImpl implements TransactionSettlementService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionSettlementServiceImpl.class);

    private InterfaceFileRepository interfaceFiles;

    private InvalidLineRepository invalidLines;

    private TransactionItemRepository transactionItems;

    private SettlementSummaryRepository settlementSummaries;

    private TransactionCounterRepository transactionCounters;
    
    private MerchantMappingRepository merchantMappings;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public InterfaceFile registerInterfaceFile(InterfaceFile interfaceFile) {
        return interfaceFiles.insertOne(interfaceFile);
    }

    @Override
    public boolean hasInterfaceFile(String interfaceFileId) {
        if(!StringUtils.hasText(interfaceFileId)) {
            return false;
        }
        return interfaceFiles.exists(interfaceFileId);
    }

    @Override
    public boolean hasInterfaceFileWithName(String originalFileName) {
        return interfaceFiles.existsByOriginalName(originalFileName);
    }

    @Override
    public InterfaceFile getInterfaceFile(String interfaceFileId) {
        return interfaceFiles.selectOne(interfaceFileId);
    }

    @Override
    public Resource getInterfaceResource(String interfaceFileId) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return null;
        }

        InterfaceFile interfaceFile = interfaceFiles.selectOne(interfaceFileId);
        Resource fileResource = new FileSystemResource(interfaceFile.getStorageDirectory() + File.separator + interfaceFile.getInternalName());
        return fileResource;
    }

    @Override
    public List<InterfaceFile> getInterfaceFiles(ProcessingState processingState) {
        return interfaceFiles.selectAllByStatusOrderByPriorityAsc(ProcessingState.NEW);
    }

    @Override
    public Page<InterfaceFile> getInterfaceFiles(Pageable pageable) {
        return interfaceFiles.selectPage(pageable);
    }

    @Override
    public ProcessingState getProcessingState(String interfaceFileId) {
        return interfaceFiles.selectOneProcessingState(interfaceFileId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setProcessingState(String interfaceFileId, ProcessingState processState) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return;
        }
        interfaceFiles.updateProcessingState(interfaceFileId, processState);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setRejectionReason(String interfaceFileId, RejectionReason rejectionReason) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return;
        }
        interfaceFiles.updateRejectionReason(interfaceFileId, rejectionReason);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setInvalidLines(String interfaceFileId, List<InvalidLine> invalidLines) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return;
        }

        if(invalidLines.isEmpty()) {
            return;
        }

        LOGGER.trace("Set invalid lines of interface file with id {}", interfaceFileId);
        this.invalidLines.insertAll(interfaceFileId, invalidLines);
    }

    @Override
    public boolean hasInvalidLines(String interfaceFileId) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return false;
        }
        return invalidLines.exists(interfaceFileId);
    }

    @Override
    public List<InvalidLine> getInvalidLines(String interfaceFileId) {
        if(!interfaceFiles.exists(interfaceFileId) || !invalidLines.exists(interfaceFileId)) {
            return Collections.emptyList();
        }

        return invalidLines.selectAll(interfaceFileId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setLoadingSummary(String interfaceFileId, InterfaceSummary loadingSummary) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return;
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setProcessedCount(String interfaceFileId, Integer processedCount) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return;
        }
        interfaceFiles.updateProcessedTransactionCount(interfaceFileId, processedCount);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setDeclaredCount(String interfaceFileId, Integer declaredCount) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return;
        }
        interfaceFiles.updateDeclaredTransactionCount(interfaceFileId, declaredCount);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setCalculatedAmount(String interfaceFileId, BigDecimal calculatedAmount) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return;
        }
        interfaceFiles.updateCalculatedTotal(interfaceFileId, calculatedAmount);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setDeclaredAmount(String interfaceFileId, BigDecimal declaredAmount) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            return;
        }
        interfaceFiles.updateDeclaredAmount(interfaceFileId, declaredAmount);
    }

    @Override
    public boolean hasTransactionCounter(String customerUid) {
        return transactionCounters.exists(customerUid);
    }

    @Override
    public TransactionCounter getTransactionCounter(String customerUid) {
        if(!transactionCounters.exists(customerUid)) {
            return null;
        }
        return transactionCounters.selectOne(customerUid);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setTransactionCounter(TransactionCounter transactionCounter) {
        if(transactionCounters.exists(transactionCounter.getCustomerUid())) {
            LOGGER.trace("Update existing customer's (UID : {}) transaction counter {}", transactionCounter.getCustomerUid(), transactionCounter);
            transactionCounters.update(transactionCounter);
        }
        else {
            LOGGER.trace("Create new customer's (UID : {}) transaction counter {}", transactionCounter.getCustomerUid(), transactionCounter);
            transactionCounters.insert(transactionCounter);
        }
    }

    @Override
    public boolean hasInterfaceTransactions(String interfaceFileId) {
        return transactionItems.existsByInterfaceFile(interfaceFileId);
    }

    @Override
    public Page<TransactionItem> getInterfaceTransactions(String interfaceFileId, Pageable pageable) {
        return transactionItems.selectAllByInterfaceFile(interfaceFileId, pageable);
    }

    @Override
    public boolean hasCustomerTransactions(String customerMsisdn) {
        return transactionItems.existsByCustomerMsisdn(customerMsisdn);
    }

    @Override
    public boolean hasCustomerTransactionsWithUid(String customerUid) {
        return transactionItems.existsByCustomerUid(customerUid);
    }

    @Override
    public boolean hasCustomerTransactions(String customerUid, Long purseCounter) {
        return transactionItems.existsByCustomerUidAndPurseCounter(customerUid, purseCounter);
    }

    @Override
    public TransactionItem getCustomerTransaction(String customerUid, Long purseCounter) {
        return transactionItems.selectOneByCustomerUidAndPurseCounter(customerUid, purseCounter);
    }

    @Override
    public Page<TransactionItem> getCustomerTransactions(String customerMsisdn, Pageable pageable) {
        return transactionItems.selectAllByCustomerMsisdn(customerMsisdn, pageable);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void calculateSettlementSummaries(String interfaceFileId) {
        if(!interfaceFiles.exists(interfaceFileId)) {
            LOGGER.error("Trying to calculate settlement summaries of non existence interface file ({})", interfaceFileId);
            return;
        }

        LOGGER.debug("Calculate and load transaction summaries");
        List<SettlementSummary> summaries = settlementSummaries.calculateSummaries(interfaceFileId);
        if(!summaries.isEmpty()) {
            settlementSummaries.insertAll(summaries);
        }
    }

    @Override
    public boolean hasSettlementSummary(String settlementSummaryId) {
        if(!StringUtils.hasText(settlementSummaryId)) {
            return false;
        }
        return settlementSummaries.exists(settlementSummaryId);
    }

    @Override
    public SettlementSummary getSettlementSummary(String settlementSummaryId) {
        if(!StringUtils.hasText(settlementSummaryId) || !settlementSummaries.exists(settlementSummaryId)) {
            return null;
        }
        return settlementSummaries.selectOne(settlementSummaryId);
    }

    @Override
    public SettlementStatus getSettlementSummaryState(String settlementSummaryId) {
        return null;
    }

    @Override
    public List<SettlementSummary> getSettlementSummaries(String interfaceFileId, SettlementStatus settlementStatus) {
        return settlementSummaries.selectAllByInterfaceFileAndState(interfaceFileId, settlementStatus);
    }

    @Override
    public Page<SettlementSummary> getSettlementSummaries(Pageable pageable) {
        return settlementSummaries.selectPage(pageable);
    }

    @Override
    public Page<SettlementSummary> getSettlementSummaries(String interfaceFileId, Pageable pageable) {
        return this.settlementSummaries.selectPageByInterfaceFile(interfaceFileId, pageable);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setSettlementSummaryStatus(String settlementSummaryId, SettlementStatus settlementStatus) {
        this.settlementSummaries.updateSummaryStatus(settlementSummaryId, settlementStatus);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setSettlementSummaryResult(String settlementSummaryId, ResultDescriptor result) {
        this.settlementSummaries.updateSummaryResult(settlementSummaryId, result);
    }

    @Override
	public boolean hasMerchantMapping(String merchantId) {
		return merchantMappings.exists(merchantId);
	}

	@Override
	public MerchantMapping getMerchantMapping(String merchantId) {
		if(!merchantMappings.exists(merchantId)) {
			return null;
		}
		return merchantMappings.selectOne(merchantId);
	}

	@Autowired
    public void setInterfaceFiles(InterfaceFileRepository interfaceFiles) {
        this.interfaceFiles = interfaceFiles;
    }

    @Autowired
    public void setInvalidLines(InvalidLineRepository invalidLines) {
        this.invalidLines = invalidLines;
    }

    @Autowired
    public void setTransactionItems(TransactionItemRepository transactionItems) {
        this.transactionItems = transactionItems;
    }

    @Autowired
    public void setSettlementSummaries(SettlementSummaryRepository settlementSummaries) {
        this.settlementSummaries = settlementSummaries;
    }

    @Autowired
    public void setTransactionCounters(TransactionCounterRepository transactionCounters) {
        this.transactionCounters = transactionCounters;
    }

    @Autowired
	public void setMerchantMappings(MerchantMappingRepository merchantMappings) {
		this.merchantMappings = merchantMappings;
	}
}
