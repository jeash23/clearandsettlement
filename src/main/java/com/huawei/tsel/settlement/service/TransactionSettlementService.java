package com.huawei.tsel.settlement.service;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.huawei.tsel.settlement.entity.*;
import org.hibernate.validator.constraints.NotBlank;

import com.huawei.tsel.settlement.entity.InterfaceFile.ProcessingState;
import com.huawei.tsel.settlement.entity.SettlementSummary.SettlementStatus;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * <p>Contract for service object responsible in handling transaction settlement related tasks.</p>
 *
 * @author zakyalvan
 * @since 31 Jul 2017
 */
public interface TransactionSettlementService {
    /**
     * Register new interface file.
     *
     * @param interfaceFile
     * @return
     */
    InterfaceFile registerInterfaceFile(@Valid @NotNull InterfaceFile interfaceFile);

    boolean hasInterfaceFile(String interfaceFileId);

    boolean hasInterfaceFileWithName(String originalFileName);

    InterfaceFile getInterfaceFile(@NotBlank String interfaceFileId);

    Resource getInterfaceResource(@NotBlank String interfaceFileId);

    List<InterfaceFile> getInterfaceFiles(ProcessingState processingState);

    Page<InterfaceFile> getInterfaceFiles(@NotNull Pageable pageable);

    ProcessingState getProcessingState(@NotBlank String interafaceFileId);

    /**
     * @param interfaceFileId
     * @param processState
     */
    void setProcessingState(@NotBlank String interfaceFileId, @NotNull ProcessingState processState);

    /**
     * <p>Set rejection reason for {@link InterfaceFile}</p>
     *
     * @param interfaceFileId
     * @param rejectionReason
     */
    void setRejectionReason(@NotBlank String interfaceFileId, @NotNull RejectionReason rejectionReason);

    /**
     * <p>Set invalid lines of interface file</p>
     *
     * @param interfaceFileId
     * @param invalidLines
     */
    void setInvalidLines(@NotBlank String interfaceFileId, @NotEmpty List<InvalidLine> invalidLines);

    boolean hasInvalidLines(@NotBlank String interfaceFileId);

    List<InvalidLine> getInvalidLines(@NotBlank String interfaceFileId);

    void setLoadingSummary(@NotBlank String interfaceFileId, @NotNull InterfaceSummary loadingSummary);

    /**
     * @param interfaceFileId
     * @param processedCount
     */
    void setProcessedCount(@NotBlank String interfaceFileId, @NotNull Integer processedCount);

    /**
     * @param interfaceFileId
     * @param declaredCount
     */
    void setDeclaredCount(@NotBlank String interfaceFileId, @NotNull Integer declaredCount);

    /**
     * Set calculated transaction amount for given settlement file id.
     *
     * @param interfaceFileId
     * @param calculatedAmount
     */
    void setCalculatedAmount(@NotBlank String interfaceFileId, @NotNull BigDecimal calculatedAmount);

    /**
     * Set declared transaction amount for given settlement file.
     *
     * @param interfaceFileId
     * @param declaredAmount
     */
    void setDeclaredAmount(@NotBlank String interfaceFileId, @NotNull BigDecimal declaredAmount);

    /**
     * <p>Check whether given customer uid has {@link TransactionCounter} logged in counter database table</p>
     *
     * @param customerUid
     * @return
     */
    boolean hasTransactionCounter(@NotBlank String customerUid);

    /**
     * <p>Retrieve last offline {@link TransactionCounter} of customer determined their UID</p>
     *
     * @param customerUid
     * @return
     */
    TransactionCounter getTransactionCounter(@NotBlank String customerUid);

    /**
     * <p>Set transaction counter</p>
     *
     * @param transactionCounter
     */
    void setTransactionCounter(@Valid @NotNull TransactionCounter transactionCounter);

    boolean hasInterfaceTransactions(String interfaceFileId);

    Page<TransactionItem> getInterfaceTransactions(@NotBlank String interfaceFileId, Pageable pageable);

    /**
     *
     * @param customerMsisdn
     * @return
     */
    boolean hasCustomerTransactions(@NotBlank String customerMsisdn);

    /**
     * Retrieve transaction items, by default sorted by transaction timestamp.
     *
     * @param pageable
     * @return
     */
    Page<TransactionItem> getCustomerTransactions(@NotBlank String customerMsisdn, @NotNull Pageable pageable);

    /**
     * <p>Check whether customer has transaction items loaded with given uid</p>
     *
     * @param customerUid
     * @return
     */
    boolean hasCustomerTransactionsWithUid(@NotBlank String customerUid);

    /**
     * <p>Check whether customer has transaction items loaded with given uid and purse counter</p>
     *
     * @param customerUid
     * @param purseCounter
     * @return
     */
    boolean hasCustomerTransactions(@NotBlank String customerUid, @NotNull Long purseCounter);

    /**
     * <p>Retrieve customer transaction based on their uid and purse counter</p>
     *
     * @param customerUid
     * @param purseCounter
     * @return
     */
    TransactionItem getCustomerTransaction(String customerUid, Long purseCounter);

    /**
     * @param interfaceFileId
     */
    void calculateSettlementSummaries(@NotBlank String interfaceFileId);

    boolean hasSettlementSummary(@NotBlank String settlementSummaryId);

    SettlementSummary getSettlementSummary(@NotBlank String settlementSummaryId);

    SettlementStatus getSettlementSummaryState(@NotBlank String settlementSummaryId);

    List<SettlementSummary> getSettlementSummaries(@NotBlank String interfaceFileId, @NotNull SettlementStatus settlementStatus);

    Page<SettlementSummary> getSettlementSummaries(Pageable pageable);

    Page<SettlementSummary> getSettlementSummaries(@NotBlank String interfaceFileId, Pageable pageable);

    void setSettlementSummaryStatus(@NotBlank String settlementSummaryId, @NotNull SettlementStatus settlementStatus);

    /**
     * <p>Set settlement summary {@link ResultDescriptor descriptor}</p>
     *
     * @param settlementSummaryId
     * @param result
     */
    void setSettlementSummaryResult(@NotBlank String settlementSummaryId, @Valid @NotNull ResultDescriptor result);

    /**
     *
     * @param merchantId
     * @return
     */
    boolean hasMerchantMapping(@NotBlank String merchantId);
    
    MerchantMapping getMerchantMapping(@NotBlank String merchantId);
}
