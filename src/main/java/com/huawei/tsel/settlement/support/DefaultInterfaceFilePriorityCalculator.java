package com.huawei.tsel.settlement.support;

import com.huawei.tsel.settlement.support.PriorityCalculationException.InvalidPriorityDataException;
import com.huawei.tsel.settlement.support.PriorityCalculationException.PriorityDataUnavailableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.io.File;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Default implementation of {@link InterfaceFilePriorityCalculator} which calculate priority
 * based on generated timestamp given in file name.</p>
 *
 * @author zakyalvan
 * @since 14 Sep 2017
 */
public class DefaultInterfaceFilePriorityCalculator implements InterfaceFilePriorityCalculator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultInterfaceFilePriorityCalculator.class);

    private final Pattern namePattern;

    private final DateFormatter timestampFormat;

    private final Integer timestampGroup;


    public DefaultInterfaceFilePriorityCalculator(String namePattern, String timestampFormat, Integer timestampGroup) {
        Assert.hasText(namePattern,"Name pattern parameter must not be null or empty string");
        Assert.hasText(timestampFormat, "Timestamp format parameter must not be null or empty string");
        this.namePattern = Pattern.compile(namePattern);
        this.timestampFormat = new DateFormatter(timestampFormat);
        this.timestampGroup = timestampGroup;
    }

    @Override
    public Long calculatePriority(File file) throws PriorityCalculationException {
        LOGGER.debug("Calculate processing priority of file {}", file);

        String fileName = file.getName();

        Matcher matcher = namePattern.matcher(fileName);
        if(!(matcher.find() && StringUtils.hasText(matcher.group(timestampGroup)))) {
            LOGGER.error("Generated timestamp on file name ({}) as based for priority calculation is unavailable", fileName);
            throw new PriorityDataUnavailableException("Generated timestamp on file name as based for priority calculation is unavailable", file);
        }

        try {
            Date timestamp = timestampFormat.parse(matcher.group(timestampGroup), Locale.getDefault());
            Long priority = Long.MAX_VALUE - timestamp.getTime();

            LOGGER.trace("Processing priority of interface file {} is {}", fileName, priority);
            return  priority;
        }
        catch (ParseException pe) {
            LOGGER.error("Generated timestamp string given on filename ({}) can not be parsed to timestamp object", matcher.group(timestampGroup));
            throw new InvalidPriorityDataException("Generated timestamp string given on filename can not be parsed to timestamp object", file, pe);
        }
    }
}
