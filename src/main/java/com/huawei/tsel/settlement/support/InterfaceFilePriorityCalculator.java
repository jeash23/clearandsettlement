package com.huawei.tsel.settlement.support;

import java.io.File;

/**
 * <p>A contract for calculating processing priority of an interface file</p>
 *
 * @author zakyalvan
 * @since 14 Sep 2017
 */
public interface InterfaceFilePriorityCalculator {
    /**
     * <p>Calculate priority of given {@link File}</p>
     *
     * @param file
     * @return
     */
    Long calculatePriority(File file) throws PriorityCalculationException;
}
