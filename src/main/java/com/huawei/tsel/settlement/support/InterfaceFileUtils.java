package com.huawei.tsel.settlement.support;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author zakyalvan
 * @since 14 Sep 2017
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InterfaceFileUtils {
    public static Date extractTimestampFromFileName(File file, Pattern pattern, int group, DateFormatter format) {
        Assert.notNull(file);
        Assert.notNull(pattern);
        Assert.notNull(format);
        return null;
    }
}
