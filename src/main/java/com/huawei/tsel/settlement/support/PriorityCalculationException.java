package com.huawei.tsel.settlement.support;

import org.springframework.core.NestedRuntimeException;

import java.io.File;
import java.util.Objects;

/**
 * @author zakyalvan
 * @since 14 Sep 2014
 */
public class PriorityCalculationException extends NestedRuntimeException {
    private final File file;

    public PriorityCalculationException(String message, File file) {
        super(message);
        this.file = Objects.requireNonNull(file, "File parameter must not be null");
    }

    public PriorityCalculationException(String msg, File file, Throwable cause) {
        super(msg, cause);
        this.file = Objects.requireNonNull(file, "File parameter must not be null");
    }

    public File getFile() {
        return file;
    }

    /**
     * <p></p>
     */
    public static class PriorityDataUnavailableException extends PriorityCalculationException {
        public PriorityDataUnavailableException(String message, File file) {
            super(message, file);
        }
    }

    /**
     * <p></p>
     */
    public static class InvalidPriorityDataException extends PriorityCalculationException {
        public InvalidPriorityDataException(String msg, File file, Throwable cause) {
            super(msg, file, cause);
        }
    }
}
