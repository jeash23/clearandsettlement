package com.huawei.tsel.settlement.web;

import com.huawei.tsel.settlement.entity.InterfaceFile;
import com.huawei.tsel.settlement.entity.InvalidLine;
import com.huawei.tsel.settlement.entity.SettlementSummary;
import com.huawei.tsel.settlement.entity.SettlementSummary.SettlementStatus;
import com.huawei.tsel.settlement.entity.TransactionItem;
import com.huawei.tsel.settlement.flow.SettlementPublishingGateway;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@Controller
@RequestMapping("/interface-files")
public class InterfaceFileController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InterfaceFileController.class);

    @Autowired
    private TransactionSettlementService settlementService;

    @Autowired
    private SettlementPublishingGateway settlementGateway;

    /**
     * <p>Method for handling list all registered interface files request</p>
     *
     * @param pageable
     * @param model
     * @return
     */
    @GetMapping
    String listInterfaces(@PageableDefault(size = 20) Pageable pageable, Model model) {
        LOGGER.debug("Handle http request for listing interface files");
        Page<InterfaceFile> interfaceFiles = settlementService.getInterfaceFiles(pageable);
        model.addAttribute("interfaceFiles", interfaceFiles);
        return "interface-files/list";
    }

    /**
     * <p>Show details of interface file registered in database</p>
     *
     * @param interfaceFileId
     * @param model
     * @return
     */
    @GetMapping(value = "/{interfaceFileId}")
    String interfaceDetails(@PathVariable("interfaceFileId") String interfaceFileId, Model model) {
        InterfaceFile interfaceFile = settlementService.getInterfaceFile(interfaceFileId);
        model.addAttribute("interfaceFile", interfaceFile);
        return "interface-files/details";
    }

    /**
     * <p>Show content of the file.</p>
     *
     * @param interfaceFileId
     * @param model
     * @return
     */
    @GetMapping(value = "/{interfaceFileId}/file-contents")
    String fileContents(@PathVariable("interfaceFileId") String interfaceFileId, Model model) throws IOException {
        InterfaceFile interfaceFile = settlementService.getInterfaceFile(interfaceFileId);
        model.addAttribute("interfaceFile", interfaceFile);

        Resource interfaceResource = settlementService.getInterfaceResource(interfaceFileId);

        try(BufferedReader contentReader = new BufferedReader(new InputStreamReader(interfaceResource.getInputStream()))) {
            StringBuilder contentBuilder = new StringBuilder();

            String lineSeparator = System.getProperty("line.separator");

            String lineContent;
            while ((lineContent = contentReader.readLine()) != null) {
                contentBuilder.append(lineContent).append(lineSeparator);
            }

            model.addAttribute("interfaceContent", contentBuilder.toString());
        }

        return "interface-files/contents";
    }

    @GetMapping(value = "/{interfaceFileId}/invalid-lines")
    String invalidLines(@PathVariable("interfaceFileId") String interfaceFileId, Model model) {
        InterfaceFile interfaceFile = settlementService.getInterfaceFile(interfaceFileId);
        model.addAttribute("interfaceFile", interfaceFile);

        List<InvalidLine> invalidLines = settlementService.getInvalidLines(interfaceFileId);
        model.addAttribute("invalidLines", invalidLines);

        return "interface-files/invalid";
    }

    @GetMapping(value = "/{interfaceFileId}/transaction-items")
    String loadedTransactions(@PathVariable("interfaceFileId") String interfaceFileId, Model model, @PageableDefault(size = 20) Pageable pageable) {
        InterfaceFile interfaceFile = settlementService.getInterfaceFile(interfaceFileId);
        model.addAttribute("interfaceFile", interfaceFile);

        Page<TransactionItem> transactionItems = settlementService.getInterfaceTransactions(interfaceFileId, pageable);
        model.addAttribute("transactionItems", transactionItems);

        return "interface-files/transactions";
    }

    /**
     *
     * @param interfaceFileId
     * @param pageable
     * @param model
     * @return
     */
    @GetMapping(value = "/{interfaceFileId}/settlement-summaries")
    String transactionSummaries(@PathVariable("interfaceFileId") String interfaceFileId, @PageableDefault(size = 20) Pageable pageable, Model model) {
        InterfaceFile interfaceFile = settlementService.getInterfaceFile(interfaceFileId);
        model.addAttribute("interfaceFile", interfaceFile);

        Page<SettlementSummary> transactionSummaries = settlementService.getSettlementSummaries(interfaceFileId, pageable);
        model.addAttribute("transactionSummaries", transactionSummaries);

        return "interface-files/summaries";
    }

    /**
     * <p>Show single {@link SettlementSummary} details</p>
     *
     * @param interfaceFileId
     * @param settlementSummaryId
     * @param model
     * @return
     */
    @GetMapping(value = "/{interfaceFileId}/settlement-summaries/{settlementSummaryId}")
    String summaryDetails(@PathVariable("interfaceFileId") String interfaceFileId, @PathVariable("settlementSummaryId") String settlementSummaryId, Model model) {
        return "interface-files/summary";
    }

    /**
     * <p>Republish a failed {@link SettlementSummary} to CPS endpoint</p>
     *
     * @param interfaceFileId
     * @param settlementSummaryId
     * @return
     */
    @PostMapping(value = "/{interfaceFileId}/settlement-summaries/{settlementSummaryId}")
    String republishSummary(@PathVariable("interfaceFileId") String interfaceFileId, @PathVariable("settlementSummaryId") String settlementSummaryId) {
        SettlementSummary settlementSummary = settlementService.getSettlementSummary(settlementSummaryId);
        if(settlementSummary.getStatus().equals(SettlementStatus.FAILED)) {
            settlementGateway.publishSettlement(settlementSummary);
        }
        return "redirect:/interface-files/" + interfaceFileId + "/settlement-summaries";
    }

    /**
     * <p>Handle manually launching settlement job request from client.
     *
     * @param interfaceFileId
     * @return
     */
    @PostMapping(value = "/{interfaceFileId}/__launch")
    String launchProcess(@PathVariable("interfaceFileId") String interfaceFileId) {
        return "interface-files/launch";
    }
}
