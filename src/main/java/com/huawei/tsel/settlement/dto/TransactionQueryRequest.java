package com.huawei.tsel.settlement.dto;

import lombok.Builder;
import lombok.Value;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>A dto for carrying information for querying current total coupon number for customer,
 * determined by {@link TransactionQueryRequest#targetMsisdn}.
 *
 * @author zakyalvan
 * @since 7 Mar 2017
 */
@Value
@Builder(builderMethodName = "create", buildMethodName = "get")
@SuppressWarnings("serial")
public class TransactionQueryRequest implements Serializable {
    @NotBlank
    String targetMsisdn;

    LocalDateTime timestamp = LocalDateTime.now();
}
