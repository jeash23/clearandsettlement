package com.huawei.tsel.settlement.dto;

import com.huawei.tsel.settlement.entity.InterfaceLine;

import com.huawei.tsel.settlement.entity.LineType;
import lombok.Data;

/**
 * <p>Type represent header line of an integration (settlement) file</p>
 * 
 * @author zakyalvan
 * @since 3 Aug 2017
 */
@Data
@SuppressWarnings("serial")
public class HeaderLine implements InterfaceLine {
	private String headerSign;

	private Integer lineNumber;

	private String lineContent;

	private final LineType lineType = LineType.HEADER;
}