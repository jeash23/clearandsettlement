package com.huawei.tsel.settlement.dto;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

/**
 * <p>Simple transfer object containing customer uid and msisdn pair</p>
 *
 * @author zakyalvan
 * @since 11 Aug 2017
 * @see com.huawei.tsel.settlement.batch.PopulateCustomerMsisdnStepConfiguration
 */
@Value
@Builder
@SuppressWarnings("serial")
public class UidAndMsisdnPair implements Serializable {
    private String uid;
    private String msisdn;
}
