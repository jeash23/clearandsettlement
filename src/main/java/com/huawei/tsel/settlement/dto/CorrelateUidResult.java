package com.huawei.tsel.settlement.dto;

import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * <p>Type representing result of checking uid from MFS. 
 * 
 * @author zakyalvan
 * @since 30 Jan 2017
 */
@SuppressWarnings("serial")
public class CorrelateUidResult implements Serializable {
	private final String uid;
	private final CheckStatus status;
	private final String msisdn;
	private final String message;
	
	public CorrelateUidResult(String uid, String msisdn) {
		this.uid = uid;
		this.status = CheckStatus.SUCCESS;
		this.msisdn = msisdn;
		this.message = null;
	}
	public CorrelateUidResult(String uid, CheckStatus status, String message) {
		this.uid = uid;
		this.status = status;
		this.msisdn = null;
		this.message = message;
	}

	public String getUid() {
		return uid;
	}
	public CheckStatus getStatus() {
		return status;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public String getMessage() {
		return message;
	}
	public boolean isSuccess() {
		return status.equals(CheckStatus.SUCCESS);
	}

	@Override
	public String toString() {
		return "CorrelateUidResult [uid=" + uid + ", status=" + status + ", msisdn=" + msisdn + ", message=" + message
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((msisdn == null) ? 0 : msisdn.hashCode());
		result = prime * result + ((uid == null) ? 0 : uid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CorrelateUidResult other = (CorrelateUidResult) obj;
		if (status != other.status)
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (msisdn == null) {
			if (other.msisdn != null)
				return false;
		} else if (!msisdn.equals(other.msisdn))
			return false;
		if (uid == null) {
			if (other.uid != null)
				return false;
		} else if (!uid.equals(other.uid))
			return false;
		return true;
	}



	/**
	 * Enumeration of UID checking result code.
	 */
	public enum CheckStatus {
		SUCCESS("00"), //: Success; 
		INVALID_PARAMETER("E1"), //: Invalid parameter;
		INVALID_CREDENTIALS("E2"), //: Invalid user / pwd;
		UID_NOT_PAIRED("E7"), //: UID Not Paired;
		MSISDN_NOT_PAIRED("E12"); //: MSISDN Not Paired.
		
		private final String code;
		
		private CheckStatus(String code) {
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}
		
		public static CheckStatus withCode(String code) {
			Assert.notNull(code, "Code parameter must not be null");
			if("00".equalsIgnoreCase(code))
				return CheckStatus.SUCCESS;
			else if("E1".equalsIgnoreCase(code))
				return CheckStatus.INVALID_PARAMETER;
			else if("E2".equalsIgnoreCase(code))
				return CheckStatus.INVALID_CREDENTIALS;
			else if("E7".equalsIgnoreCase(code))
				return CheckStatus.UID_NOT_PAIRED;
			else if("E12".equalsIgnoreCase(code)) 
				return CheckStatus.MSISDN_NOT_PAIRED;
			else
				return null;
		}
	}
}
