package com.huawei.tsel.settlement.dto;

import lombok.Builder;
import lombok.Value;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author zakyalvan
 * @since 26 Sep 2017
 */
@Value
@SuppressWarnings("serial")
public class LaunchProcessCommand implements Serializable {
    String fileName;
    String fileChecksum;
    String interfaceId;
    String directoryPath;
    Character fileSeparator;
    LocalDateTime launchTimestamp;
    LaunchType launchType;

    @Builder
    LaunchProcessCommand(String fileName, String fileChecksum, String interfaceId, String directoryPath, LaunchType launchType) {
        this.fileName = fileName;
        this.fileChecksum = fileChecksum;
        this.interfaceId = interfaceId;
        this.directoryPath = directoryPath;
        this.fileSeparator = File.separatorChar;
        this.launchTimestamp = LocalDateTime.now();
        this.launchType = launchType;
    }

    public enum LaunchType {
        SCHEDULED, MANUAL
    }
}
