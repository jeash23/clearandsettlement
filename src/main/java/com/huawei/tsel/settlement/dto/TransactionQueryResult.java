package com.huawei.tsel.settlement.dto;

import com.huawei.tsel.settlement.entity.TransactionItem;
import lombok.Builder;
import lombok.Value;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>Result of inquiry coupon process.
 *
 * @author zakyalvan
 * @since 10 Mar 2017
 */
@Value
@Builder
@SuppressWarnings("serial")
public class TransactionQueryResult implements Serializable {
    TransactionQueryRequest inquiryRequest;
    List<TransactionItem> transactions;
    boolean success;
    LocalDateTime resultTimestamp = LocalDateTime.now();
}
