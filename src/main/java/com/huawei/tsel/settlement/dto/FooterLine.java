package com.huawei.tsel.settlement.dto;

import java.math.BigDecimal;

import com.huawei.tsel.settlement.entity.InterfaceLine;

import com.huawei.tsel.settlement.entity.LineType;
import lombok.Data;

/**
 * <p>Type represent footer line of an integration (settlement) file
 *
 * @author zakyalvan
 * @since 3 Aug 2017
 */
@Data
@SuppressWarnings("serial")
public class FooterLine implements InterfaceLine {
    /**
     * Number of transaction in one integration (transaction settlement) file.
     */
    private Integer transactionCount;

    /**
     * Total of transaction amount in one integration (transaction settlement) file.
     */
    private BigDecimal transactionAmount;

    private Integer lineNumber;

    private String lineContent;

    private final LineType lineType = LineType.FOOTER;
}
