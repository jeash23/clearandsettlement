package com.huawei.tsel.settlement.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.core.NestedRuntimeException;
import org.springframework.validation.*;

import java.beans.PropertyEditor;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>Type encapsulate request for inquiry coupon from external system.
 *
 * @author zakyalvan
 * @since 31 Mar 2017
 */
@Data
@SuppressWarnings("serial")
public class TransactionQueryForm implements Serializable {
    /**
     * <p>Password provided by MFS, not encrypted. MFS will check this field
     * with configurable value to determine this is a request through USSD channel.
     *
     * <p>Example : tsel969
     */
    @NotBlank
    private String auth;

    /**
     * <p>Short code number, it’s configurable in the TMenu side.
     *
     * <p>Example : 2828
     */
    @NotBlank
    private String adn;

    /**
     * <p>TMenu get from which system.
     *
     * <p>Here fixed to “ussd”
     */
    private String bearer;

    /**
     * <p>Request content, fields delimited by space.
     *
     * <p>Example : TRX 1234
     */
    @NotBlank
    private String msg;

    /**
     * Request customer’s MSISDN, Format using country code (62).
     */
    @NotBlank
    private String msisdn;

    /**
     * <p>A {@link SmartValidator} implementation for validating {@link TransactionQueryForm}.
     */
    public static class Validator implements SmartValidator {
        private SmartValidator delegate;

        public Validator(SmartValidator delegate) {
            this.delegate = Objects.requireNonNull(delegate);
        }

        @Override
        public boolean supports(Class<?> clazz) {
            return clazz.isAssignableFrom(TransactionQueryForm.class);
        }

        @Override
        public void validate(Object target, Errors errors, Object... validationHints) {
            delegate.validate(target, errors, validationHints);
        }

        @Override
        public void validate(Object target, Errors errors) {
            delegate.validate(target, errors);
        }
    }

    /**
     *
     */
    public static class DataBindingException extends NestedRuntimeException implements BindingResult {
        private final BindingResult errors;

        public DataBindingException(BindingResult errors) {
            super(Objects.requireNonNull(errors).toString());
            this.errors = errors;
        }

        @Override
        public Object getTarget() {
            return errors.getTarget();
        }

        @Override
        public Map<String, Object> getModel() {
            return errors.getModel();
        }

        @Override
        public Object getRawFieldValue(String field) {
            return errors.getRawFieldValue(field);
        }

        @Override
        public PropertyEditor findEditor(String field, Class<?> valueType) {
            return errors.findEditor(field, valueType);
        }

        @Override
        public PropertyEditorRegistry getPropertyEditorRegistry() {
            return errors.getPropertyEditorRegistry();
        }

        @Override
        public void addError(ObjectError error) {
            errors.addError(error);
        }

        @Override
        public String[] resolveMessageCodes(String errorCode) {
            return errors.resolveMessageCodes(errorCode);
        }

        @Override
        public String[] resolveMessageCodes(String errorCode, String field) {
            return errors.resolveMessageCodes(errorCode, field);
        }

        @Override
        public void recordSuppressedField(String field) {
            errors.recordSuppressedField(field);
        }

        @Override
        public String[] getSuppressedFields() {
            return errors.getSuppressedFields();
        }

        @Override
        public String getObjectName() {
            return errors.getObjectName();
        }

        @Override
        public void setNestedPath(String nestedPath) {
            errors.setNestedPath(nestedPath);
        }

        @Override
        public String getNestedPath() {
            return errors.getNestedPath();
        }

        @Override
        public void pushNestedPath(String subPath) {
            errors.pushNestedPath(subPath);
        }

        @Override
        public void popNestedPath() throws IllegalStateException {
            errors.popNestedPath();
        }

        @Override
        public void reject(String errorCode) {
            errors.reject(errorCode);
        }

        @Override
        public void reject(String errorCode, String defaultMessage) {
            errors.reject(errorCode, defaultMessage);
        }

        @Override
        public void reject(String errorCode, Object[] errorArgs, String defaultMessage) {
            errors.reject(errorCode, errorArgs, defaultMessage);
        }

        @Override
        public void rejectValue(String field, String errorCode) {
            errors.rejectValue(field, errorCode);
        }

        @Override
        public void rejectValue(String field, String errorCode, String defaultMessage) {
            errors.rejectValue(field, errorCode, defaultMessage);
        }

        @Override
        public void rejectValue(String field, String errorCode, Object[] errorArgs, String defaultMessage) {
            errors.rejectValue(field, errorCode, errorArgs, defaultMessage);
        }

        @Override
        public void addAllErrors(Errors errors) {
            this.errors.addAllErrors(errors);
        }

        @Override
        public boolean hasErrors() {
            return errors.hasErrors();
        }

        @Override
        public int getErrorCount() {
            return errors.getErrorCount();
        }

        @Override
        public List<ObjectError> getAllErrors() {
            return errors.getAllErrors();
        }

        @Override
        public boolean hasGlobalErrors() {
            return errors.hasGlobalErrors();
        }

        @Override
        public int getGlobalErrorCount() {
            return errors.getGlobalErrorCount();
        }

        @Override
        public List<ObjectError> getGlobalErrors() {
            return errors.getGlobalErrors();
        }

        @Override
        public ObjectError getGlobalError() {
            return errors.getGlobalError();
        }

        @Override
        public boolean hasFieldErrors() {
            return errors.hasFieldErrors();
        }

        @Override
        public int getFieldErrorCount() {
            return errors.getFieldErrorCount();
        }

        @Override
        public List<FieldError> getFieldErrors() {
            return errors.getFieldErrors();
        }

        @Override
        public FieldError getFieldError() {
            return errors.getFieldError();
        }

        @Override
        public boolean hasFieldErrors(String field) {
            return errors.hasFieldErrors(field);
        }

        @Override
        public int getFieldErrorCount(String field) {
            return errors.getFieldErrorCount(field);
        }

        @Override
        public List<FieldError> getFieldErrors(String field) {
            return errors.getFieldErrors(field);
        }

        @Override
        public FieldError getFieldError(String field) {
            return errors.getFieldError(field);
        }

        @Override
        public Object getFieldValue(String field) {
            return errors.getFieldValue(field);
        }

        @Override
        public Class<?> getFieldType(String field) {
            return errors.getFieldType(field);
        }
    }

    /**
     * Exception to be thrown when inquiry request come with invalid credentials.
     */
    public static class InvalidCredentialsException extends NestedRuntimeException {
        public InvalidCredentialsException(String message) {
            super(message);
        }
        public InvalidCredentialsException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
