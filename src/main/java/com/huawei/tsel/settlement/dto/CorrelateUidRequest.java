package com.huawei.tsel.settlement.dto;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

/**
 * <p>Command object to be send for checking uid to mfs.
 *
 * @author zakyalvan
 * @since 30 Jan 2017
 */
@Value
@Builder
@SuppressWarnings("serial")
public class CorrelateUidRequest implements Serializable {
    /**
     * Uid to correlate.
     */
    String uid;
}
