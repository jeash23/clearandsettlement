package com.huawei.tsel.settlement.query;

import java.util.function.Consumer;

/**
 * @author zakyalvan
 * @since 22 Sep 2017
 */
public class StringCriteria extends ColumnCriteria {
    private final String columnName;
    private final StringOperator operator;

    public StringCriteria(String columnName, StringOperator operator) {
        this.columnName = columnName;
        this.operator = operator;
    }

    public String columnName() {
        return columnName;
    }

    public Operator<?> operator() {
        return operator;
    }

    public StringCriteria equalsTo(Consumer<StringOperator> equalsTo) {
        equalsTo.accept(operator);
        return this;
    }

    public StringCriteria contains(Consumer<StringOperator> contains) {
        contains.accept(operator);
        return this;
    }

    public StringCriteria starsWith(Consumer<StringOperator> startWith) {
        startWith.accept(operator);
        return this;
    }

    public StringCriteria endWith(Consumer<StringOperator> endWith) {
        endWith.accept(operator);
        return this;
    }
}
