package com.huawei.tsel.settlement.query;

/**
 * <p></p>
 *
 * @author zakyalvan
 * @since 22 Sep 2017
 */
public class ColumnCriteria {
    private String columnName;

    //private Operator<T> operator;

    /**
     * <p>And</p>
     *
     * @param criteria
     */
    public <C extends ColumnCriteria> C and(C criteria) {
        return criteria;
    }

    /**
     * <p>Or</p>
     *
     * @param criteria
     */
    public <C extends ColumnCriteria> C or(C criteria) {
        return criteria;
    }
}
