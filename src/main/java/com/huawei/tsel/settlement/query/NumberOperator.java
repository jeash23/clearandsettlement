package com.huawei.tsel.settlement.query;

/**
 * @author zakyalvan
 * @since
 */
public class NumberOperator<N extends Number> extends Operator<NumberOperator<N>> {
    private N equalsTo;
    private N lowerThan;
    private boolean exclusiveLowerBound = false;
    private N higherThan;
    private boolean exclusiveHigherBound = false;

    public NumberOperator<N> equalsTo(N value) {
        return this;
    }

    public NumberOperator<N> lowerThan(N maxValue) {
        return this;
    }

    public NumberOperator<N> lowerThanOrEquals(N maxValue) {
        return this;
    }

    public NumberOperator<N> higherThan(N minValue) {
        return this;
    }

    public NumberOperator<N> higherThanOrEquals(N minValue, boolean exclusiveBound) {
        return this;
    }

    public NumberOperator<N> between(N firstBound, N secondBound) {
        return this;
    }
}
