package com.huawei.tsel.settlement.query;

/**
 * @author zakyalvan
 * @since 22 Sep 2017
 */
public class StringOperator extends Operator<StringOperator> {
    private String equalsTo;
    private String startWith;
    private String endWith;
    private String contains;

    public StringOperator equalsTo(String value) {
        this.equalsTo = value;
        return this;
    }

    public StringOperator startWith(String prefix) {
        this.startWith = prefix;
        return this;
    }

    public StringOperator endWith(String suffix) {
        this.endWith = suffix;
        return this;
    }

    public StringOperator contains(String part) {
        this.contains = part;
        return this;
    }
}
