package com.huawei.tsel.settlement.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>Query operator</p>
 *
 * @author zakyalvan
 * @since 22 Sep 2017
 */
public class Operator<T extends Operator> {
    private List<? super Operator> nextAnds = new ArrayList<>();
    private List<? super Operator> nextOrs = new ArrayList<>();

    private boolean inverted;

    public <O extends Operator> T and(O other) {
        nextAnds.add(Objects.requireNonNull(other));
        return (T) this;
    }
    public <O extends Operator> T or(O other) {
        nextOrs.add(Objects.requireNonNull(other));
        return (T) this;
    }
    public void invert() {
        this.inverted = true;
    }

    public boolean isInverted() {
        return inverted;
    }
}
