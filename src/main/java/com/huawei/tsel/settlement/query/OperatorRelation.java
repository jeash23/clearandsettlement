package com.huawei.tsel.settlement.query;

/**
 * @author zakyalvan
 * @since 22 Sep 2017
 */
enum OperatorRelation {
    AND, OR
}
