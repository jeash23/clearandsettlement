package com.huawei.tsel.settlement.query;

import static com.huawei.tsel.settlement.query.TransactionItemCriteria.*;

/**
 * @author zakyalvan
 * @since 21 Aug 2017
 */
public class Sample {
    public static void main(String[] args) {
        TransactionItemCriteria transaction = create();
        transaction.customerUid(uid -> uid.startWith("Zaky").or(uid.endWith("")))
                .and(transaction.purseCounter(counter -> counter.equalsTo(100l))
                        .or(transaction.customerUid(uid -> uid.startWith(""))));
    }
}
