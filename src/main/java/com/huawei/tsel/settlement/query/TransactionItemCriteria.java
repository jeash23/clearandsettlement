package com.huawei.tsel.settlement.query;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author zakyalvan
 * @since 22 Sep 2017
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TransactionItemCriteria extends Operator<TransactionItemCriteria> {
    private List<? extends Operator> operators = new ArrayList<>();

    public static TransactionItemCriteria create() {
        return new TransactionItemCriteria();
    }

    public StringCriteria customerUid(Consumer<StringOperator> customerUid) {
        StringOperator operator = new StringOperator();
        customerUid.accept(operator);
        StringCriteria criteria = new StringCriteria("CUSTOMER_UID", operator);
        return criteria;
    }

    public LongCriteria purseCounter(Consumer<LongOperator> purseCounter) {
        LongOperator operator = new LongOperator();
        purseCounter.accept(operator);
        LongCriteria criteria = new LongCriteria();
        return criteria;
    }
}
