package com.huawei.tsel.settlement.security;

import com.huawei.tsel.security.encrypt.CipherOperations;
import com.huawei.tsel.security.encrypt.InputFunction;
import com.huawei.tsel.security.encrypt.ReturnFunction;
import com.huawei.tsel.security.user.Credentials;
import com.huawei.tsel.security.user.CredentialsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * @author zakyalvan
 * @since 10 Aug 2017
 */
@Component("offline-trx.credentials")
@ConfigurationProperties(prefix = "huawei.offline-transaction.users")
public class TransactionQueryCredentialRepository implements CredentialsRepository, ApplicationContextAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionQueryCredentialRepository.class);

    private static final String DEFAULT_CREDENTIALS_NAME = "2828";
    private static final char[] DEFAULT_CREDENTIALS_PASSWORD = "tsel969".toCharArray();

    @NotNull
    @Size(min = 1)
    private Map<String, char[]> credentials = new HashMap<>();

    private ApplicationContext applicationContext;

    public TransactionQueryCredentialRepository() {
        credentials.put(DEFAULT_CREDENTIALS_NAME, DEFAULT_CREDENTIALS_PASSWORD);
    }

    public Map<String, char[]> getCredentials() {
        return credentials;
    }

    public void setCredentials(Map<String, char[]> credentials) {
        this.credentials = credentials;
    }

    @Override
    public void put(Credentials user) {
        Assert.isTrue(!user.getUsername().equals(DEFAULT_CREDENTIALS_NAME), "' " + DEFAULT_CREDENTIALS_NAME + " ' is reserved default credentials name, choose another one");
        credentials.put(user.getUsername(), user.getPassword());
    }

    @Override
    public Credentials get(String username) {
        if (!credentials.containsKey(username)) {
            LOGGER.trace("User '{}' not found in repository");
            return null;
        }
        return new Credentials(username, credentials.get(username));
    }

    @Override
    public boolean has(String username) {
        return credentials.containsKey(username);
    }

    @Override
    public Collection<Credentials> all() {
        Set<Credentials> users = new HashSet<>();
        for (String username : credentials.keySet()) {
            users.add(new Credentials(username, credentials.get(username)));
        }
        return users;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
