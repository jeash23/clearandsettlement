package com.huawei.tsel.settlement.batch;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>A {@link Configuration} for batch components responsible for processing settlement file.
 * It start with reading settlement file from common/shared storage.
 *
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@Configuration
public class SettlementProcessingJobConfiguration {
    /**
     * <p>A batch {@link Job} responsible for loading and process settlement of offline transactions.
     *
     * @param jobs
     * @return
     */
    @Bean("transactionSettlementJob")
    Job transactionSettlementJob(JobBuilderFactory jobs,
                                 @Qualifier("interfaceFileValidationStep") Step interfaceFileValidationStep,
                                 @Qualifier("transactionLoadingStep") Step transactionLoadingStep,
                                 @Qualifier("transactionSummarizingStep") Step transactionSummarizingStep,
                                 @Qualifier("settlementPublishingStep") Step settlementPublishingStep,
                                 @Qualifier("populateCustomerMsisdnStep") Step populateCustomerMsisdnStep) {
        return jobs.get("TransactionSettlementJob")
                .validator(settlementJobParametersValidator())
                .start(interfaceFileValidationStep).on(ExitStatus.FAILED.toString()).fail()
                .next(transactionLoadingStep)
                .next(transactionSummarizingStep)
                .next(settlementPublishingStep)
                .next(populateCustomerMsisdnStep)
                .end().build();
    }

    @Bean
    SettlementJobParametersValidator settlementJobParametersValidator() {
        return new SettlementJobParametersValidator();
    }
}