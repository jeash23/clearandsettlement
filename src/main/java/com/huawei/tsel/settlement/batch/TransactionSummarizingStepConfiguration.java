package com.huawei.tsel.settlement.batch;

import com.huawei.tsel.settlement.entity.InterfaceFile;
import com.huawei.tsel.settlement.flow.BatchJobLaunchSettings;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>Summarize transaction of each merchants per batch processing. This {@link Tasklet} must be
 * executed after loading of each interface settlement file.</p>
 *
 * <p>Write summary of each merchant's offline transaction into table ...</p>
 *
 * @author zakyalvan
 * @since 7 Aug 2017
 */
@Configuration
public class TransactionSummarizingStepConfiguration implements Tasklet, StepExecutionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionSummarizingStepConfiguration.class);

    @Autowired
    private TransactionSettlementService settlementService;

    /**
     * <p>Transaction summarizing step configuration</p>
     *
     * @param steps
     * @return
     */
    @Bean("transactionSummarizingStep")
    Step transactionSummarizeStep(StepBuilderFactory steps) {
        return steps.get("TransactionSummarizeStep").tasklet(this)
                .allowStartIfComplete(true)
                .listener(this)
                .build();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        LOGGER.trace("Summarize offline transaction from each merchant to be settled");
        String interfaceFileId = (String) chunkContext.getStepContext().getJobParameters().get(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);
        settlementService.calculateSettlementSummaries(interfaceFileId);
        return RepeatStatus.FINISHED;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        String interfaceFileId = stepExecution.getJobParameters().getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);
        settlementService.setProcessingState(interfaceFileId, InterfaceFile.ProcessingState.SUMMARIZING);
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        String interfaceFileId = stepExecution.getJobParameters().getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);
        settlementService.setProcessingState(interfaceFileId, InterfaceFile.ProcessingState.SUMMARIZED);
        return null;
    }
}
