package com.huawei.tsel.settlement.batch;

import com.huawei.tsel.settlement.dto.FooterLine;
import com.huawei.tsel.settlement.dto.HeaderLine;
import com.huawei.tsel.settlement.entity.*;
import com.huawei.tsel.settlement.entity.InterfaceFile.ProcessingState;
import com.huawei.tsel.settlement.flow.BatchJobLaunchSettings;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.listener.StepListenerSupport;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.support.FormatterPropertyEditorAdapter;
import org.springframework.validation.DataBinder;

import java.beans.PropertyEditorSupport;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>A {@link Step} {@link Configuration} for validating settlement interface file.</p>
 *
 * @author zakyalvan
 * @since 13 Aug 2017
 */
@Configuration
public class InterfaceFileValidationStepConfiguration extends StepListenerSupport<InterfaceLine, TransactionItem> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InterfaceFileValidationStepConfiguration.class);

    /**
     * <p>A {@link Map} of customer uid to {@link TransactionCounter} used for
     * validating each customer's transaction item</p>
     */
    private final Map<String, TransactionCounter> transactionCounters = new ConcurrentHashMap<>();

    /**
     * <p>A {@link List} of invalid lines as result of validation</p>
     */
    private final List<InvalidLine> invalidLines = new ArrayList<>();

    @Autowired
    private SettlementProcessingSettings processSettings;

    @Autowired
    private TransactionSettlementService settlementService;

    @Override
    public void beforeStep(StepExecution stepExecution) {
    	LOGGER.trace("Clear transaction counter in-memory cache before starting interface file validation");
        transactionCounters.clear();

        LOGGER.trace("Clear invalid lines cache");
        invalidLines.clear();

        String interfaceId = stepExecution.getJobParameters().getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);
        settlementService.setProcessingState(interfaceId, ProcessingState.VALIDATING);

        transactionValidationProcessor().resetTransactionCounters();
    }

    @Override
	public ExitStatus afterStep(StepExecution stepExecution) {
        String interfaceFileId = stepExecution.getJobParameters().getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);

        if(stepExecution.getProcessSkipCount() > 0) {
    		stepExecution.setStatus(BatchStatus.FAILED);
            settlementService.setProcessingState(interfaceFileId, ProcessingState.INVALID);
            settlementService.setRejectionReason(interfaceFileId, RejectionReason.INVALID_ITEM_ENCOUNTERED);
            settlementService.setInvalidLines(interfaceFileId, invalidLines);
    		return ExitStatus.FAILED;
    	}
    	else if(stepExecution.getReadCount() == 2) {
            stepExecution.setStatus(BatchStatus.FAILED);
            settlementService.setProcessingState(interfaceFileId, ProcessingState.INVALID);
            settlementService.setRejectionReason(interfaceFileId, RejectionReason.ZERO_TRANSACTION_PROVIDED);
            return ExitStatus.FAILED;
        }
        else {
            settlementService.setProcessingState(interfaceFileId, ProcessingState.READY);
            return null;
        }
	}

	@Override
	public void onSkipInProcess(InterfaceLine item, Throwable throwable) {
		LOGGER.trace("Persist invalid line meta {} (#{}) with exception {} ({})", item.getLineContent(), item.getLineNumber(), throwable.getClass().getName(), throwable.getMessage());

        InvalidLine invalidLine = InvalidLine.builder()
                .type(item.getLineType()).number(item.getLineNumber()).content(item.getLineContent()).message(throwable.getMessage())
                .build();

        LOGGER.trace("Invalid line : {}", invalidLine);

        invalidLines.add(invalidLine);

		super.onSkipInProcess(item, throwable);
	}

	@StepScope
    @Bean("interfaceFileValidationReader")
    FlatFileItemReader<InterfaceLine> interfaceFileValidationReader(@Value("file://#{jobParameters['interfaceFile']}") Resource interfaceFile) {
        FlatFileItemReader<InterfaceLine> itemReader = new FlatFileItemReader<>();
        itemReader.setLineMapper(validationSettlementLineMapper());
        itemReader.setResource(interfaceFile);
        itemReader.setEncoding(processSettings.getReading().getTextEncoding());
        itemReader.setSaveState(false);
        itemReader.setStrict(true);
        return itemReader;
    }

    @Bean
    LineMapper<InterfaceLine> validationSettlementLineMapper() {
        PatternMatchingCompositeLineMapper<InterfaceLine> lineMapper = new PatternMatchingCompositeLineMapper<InterfaceLine>() {
            @Override
            public InterfaceLine mapLine(String line, int lineNumber) throws Exception {
                InterfaceLine interfaceLine = super.mapLine(line, lineNumber);
                interfaceLine.setLineNumber(lineNumber);
                interfaceLine.setLineContent(line);
                return interfaceLine;
            }
        };

        Map<String, LineTokenizer> lineTokenizer = new HashMap<>();
        lineTokenizer.put("H|*", validationHeaderLineTokenizer());
        lineTokenizer.put("*", validationTransactionLineTokenizer());
        lineTokenizer.put("F|*", validationFooterLineTokenizer());

        lineMapper.setTokenizers(lineTokenizer);

        Map<String, FieldSetMapper<InterfaceLine>> fieldSetMappers = new HashMap<>();
        fieldSetMappers.put("H|*", validationHeaderFieldSetMapper());
        fieldSetMappers.put("*", validationTransactionFieldSetMapper());
        fieldSetMappers.put("F|*", validationFooterFieldSetMapper());

        lineMapper.setFieldSetMappers(fieldSetMappers);

        return lineMapper;
    }

    @Bean
    LineTokenizer validationHeaderLineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"headerSign"});
        lineTokenizer.setStrict(false);
        lineTokenizer.setDelimiter("|");
        return lineTokenizer;
    }

    @Bean
    FieldSetMapper<InterfaceLine> validationHeaderFieldSetMapper() {
        BeanWrapperFieldSetMapper<InterfaceLine> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(HeaderLine.class);
        fieldSetMapper.setStrict(false);
        return fieldSetMapper;
    }

    @Bean
    LineTokenizer validationTransactionLineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"customerUid", "transactionAmount", "transactionId", "transactionDate", "merchantId", "terminalId", "lastBalance", "purseCounter"});
        lineTokenizer.setStrict(true);
        lineTokenizer.setDelimiter("|");
        return lineTokenizer;
    }

    @Bean
    FieldSetMapper<InterfaceLine> validationTransactionFieldSetMapper() {
        BeanWrapperFieldSetMapper<InterfaceLine> fieldSetMapper = new BeanWrapperFieldSetMapper<InterfaceLine>() {
            @Override
            protected void initBinder(DataBinder binder) {
                DateFormatter transactionDateFormat = new DateFormatter(processSettings.getReading().getTransactionDateFormat());
                binder.registerCustomEditor(Date.class, "transactionDate", new FormatterPropertyEditorAdapter(transactionDateFormat));
                binder.registerCustomEditor(Long.class, "purseCounter", new PropertyEditorSupport() {
                    @Override
                    public void setAsText(String text) throws IllegalArgumentException {
                        try {
                            setValue(Long.parseLong(text, 16));
                        } catch (NumberFormatException nfe) {
                            LOGGER.error(String.format("Can't parse given purse counter (%s) which expected to be in hex number representation", text));
                            throw new IllegalArgumentException("Can't parse given purse counter of transaction line, see stack traces", nfe);
                        }
                    }
                });
                binder.setIgnoreUnknownFields(false);
            }
        };
        fieldSetMapper.setTargetType(TransactionItem.class);
        fieldSetMapper.setStrict(true);
        return fieldSetMapper;
    }

    @Bean
    LineTokenizer validationFooterLineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"footerSign", "transactionCount", "transactionAmount"});
        lineTokenizer.setStrict(true);
        lineTokenizer.setDelimiter("|");
        return lineTokenizer;
    }

    @Bean
    FieldSetMapper<InterfaceLine> validationFooterFieldSetMapper() {
        BeanWrapperFieldSetMapper<InterfaceLine> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(FooterLine.class);
        fieldSetMapper.setStrict(false);
        return fieldSetMapper;
    }

    /**
     * <p>An {@link ItemProcessor} for processing validation of settlement interface file</p>
     *
     * @return
     */
    @Bean("transactionValidationProcessor")
    InterfaceFileValidationItemProcessor transactionValidationProcessor() {
        return new InterfaceFileValidationItemProcessor();
    }


    @Bean("interfaceFileValidationWriter")
    ItemWriter<TransactionItem> interfaceFileValidationWriter() {
        /**
         * FIXME
         * 
         * Currently nothing will be written out by this writer.
         */
    	return items -> {};
    }

    @Bean("interfaceFileValidationStep")
    Step interfaceFileValidationStep(StepBuilderFactory steps,
                                     @Qualifier("interfaceFileValidationReader") ItemReader<InterfaceLine> itemReader,
                                     @Qualifier("transactionValidationProcessor") ItemProcessor<InterfaceLine, TransactionItem> itemProcessor,
                                     @Qualifier("interfaceFileValidationWriter") ItemWriter<TransactionItem> itemWriter) {
        return steps.get("InterfaceFileValidationStep")
                .allowStartIfComplete(true)
                .<InterfaceLine, TransactionItem> chunk(10).reader(itemReader).processor(itemProcessor).writer(itemWriter)
                .faultTolerant().noRollback(InterfaceFileValidationException.class).skipPolicy((throwable, skipCount) -> {
                    if(InterfaceFileValidationException.class.isAssignableFrom(throwable.getClass())) {
                        LOGGER.warn("Skipping for validation error ({} times) {} ({})", skipCount, throwable.getClass().getName(), throwable.getMessage());
                        return true;
                    }
                    return false;
                })
                .listener((StepExecutionListener) this)
                .listener((SkipListener) this)
                .build();
    }
}
