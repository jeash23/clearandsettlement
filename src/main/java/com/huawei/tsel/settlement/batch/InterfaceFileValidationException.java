package com.huawei.tsel.settlement.batch;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import com.huawei.tsel.settlement.entity.InterfaceSummary;
import org.springframework.batch.item.validator.ValidationException;
import org.springframework.core.NestedRuntimeException;

import com.huawei.tsel.settlement.entity.TransactionItem;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;


/**
 * <p>A {@link NestedRuntimeException} extension intended to be thrown when validation of
 * settlement interface file and the contents failed.</p>
 *
 * @author zakyalvan
 * @see InterfaceFileValidationStepConfiguration
 * @since 14 Aug 2017
 */
@SuppressWarnings("serial")
public class InterfaceFileValidationException extends NestedRuntimeException {
    public InterfaceFileValidationException(String msg) {
        super(msg);
    }

    public InterfaceFileValidationException(String msg, Throwable cause) {
        super(msg, cause);
    }

    /**
     * <p>Base {@link Exception} type representing errors of {@link TransactionItem} validation.</p>
     */
    public static class TransactionItemValidationException extends InterfaceFileValidationException {
        private final TransactionItem transaction;
        public TransactionItemValidationException(String msg, TransactionItem transaction) {
            super(msg);
            this.transaction = Objects.requireNonNull(transaction);
        }
        public TransactionItem getTransaction() {
            return transaction;
        }
    }

    /**
     * <p>Exception type to be thrown when no merchant mapping found for given merchant id</p>
     */
    public static class MerchantNotMappedException extends TransactionItemValidationException {
        private final String merchantId;

        public MerchantNotMappedException(String merchantId, TransactionItem transaction) {
            super(String.format("No merchant mapping for merchant with id '%s'", merchantId), transaction);
            this.merchantId = Objects.requireNonNull(merchantId);
        }

        public String getMerchantId() {
            return merchantId;
        }
    }

    /**
     * <p>Exception type to be thrown when already exists {@link TransactionItem#purseCounter} found</p>
     */
    public static class DuplicatePurseCounterException extends TransactionItemValidationException {
        private static final String MESSAGE_TEMPLATE = "Transaction with purse counter {} (0x{}) and customer uid {} already loaded";

        private final Long purseCounter;

        private final TransactionItem duplicateTransaction;

        public DuplicatePurseCounterException(TransactionItem transaction, TransactionItem duplicateTransaction, Long purseCounter) {
            super(String.format(MESSAGE_TEMPLATE, purseCounter, Long.toHexString(purseCounter), transaction.getCustomerUid()), transaction);
            this.purseCounter = purseCounter;

            this.duplicateTransaction = Objects.requireNonNull(duplicateTransaction);
        }

        public Long getPurseCounter() {
            return purseCounter;
        }

        public TransactionItem getDuplicateTransaction() {
            return duplicateTransaction;
        }
    }

    /**
     *
     */
    public static class PurseCounterStartException extends TransactionItemValidationException {
        private static final String MESSAGE_TEMPLATE = "Expected purse counter start value is %s %d (0x%s) but given value is %d (%s) for customer with uid %s";

        private final Long expectedValue;
        private final Long givenValue;
        private final boolean strictStart;

        public PurseCounterStartException(TransactionItem transaction, Long expectedValue, Long givenValue, boolean strictStart) {
            super(String.format(MESSAGE_TEMPLATE, strictStart ? "equal to" : "lowerThan", expectedValue, Long.toHexString(expectedValue), givenValue, Long.toHexString(givenValue), transaction.getCustomerUid()), transaction);
            this.expectedValue = expectedValue;
            this.givenValue = givenValue;
            this.strictStart = strictStart;
        }

        public Long getExpectedValue() {
            return expectedValue;
        }

        public Long getGivenValue() {
            return givenValue;
        }

        public boolean isStrictStart() {
            return strictStart;
        }
    }

    /**
     * <p></p>
     */
    public static class PurseCounterDecrementException extends TransactionItemValidationException {
        private static final String MESSAGE_TEMPLATE = "Expected current purse counter value is %s %d (0x%s) but given value is %d (0x%s) for customer with uid %s";

        private final Long currentValue;
        private final Long givenValue;
        private final Integer decrementValue;
        private final boolean constantDecrement;

        public PurseCounterDecrementException(TransactionItem transaction, Long currentValue, Long givenValue, Integer decrementValue) {
            super(String.format(MESSAGE_TEMPLATE, "equals to", currentValue - decrementValue, Long.toHexString(currentValue - decrementValue), givenValue, Long.toHexString(givenValue), transaction.getCustomerUid()), transaction);
            this.currentValue = currentValue;
            this.givenValue = givenValue;
            this.decrementValue = decrementValue;
            this.constantDecrement = true;
        }
        public PurseCounterDecrementException(TransactionItem transaction, Long currentValue, Long givenValue) {
            super(String.format(MESSAGE_TEMPLATE, "lower than", currentValue, Long.toHexString(currentValue), givenValue, Long.toHexString(givenValue), transaction.getCustomerUid()), transaction);
            this.currentValue = currentValue;
            this.givenValue = givenValue;
            this.decrementValue = 0;
            this.constantDecrement = false;
        }

        public Long getCurrentValue() {
            return currentValue;
        }

        public Long getGivenValue() {
            return givenValue;
        }

        public Integer getDecrementValue() {
            return decrementValue;
        }

        public boolean isConstantDecrement() {
            return constantDecrement;
        }
    }

    /**
     * <p>Exception type to be thrown when last balance of transaction line is invalid</p>
     */
    public static class InvalidLastBalanceException extends TransactionItemValidationException {
        private final BigDecimal expectedValue;
        private final BigDecimal givenValue;

        public InvalidLastBalanceException(BigDecimal expectedValue, BigDecimal givenValue, TransactionItem transaction) {
            super("Expected value is " + expectedValue + " but given value is " + givenValue + " for transaction item : " + transaction.toString(), transaction);
            this.expectedValue = Objects.requireNonNull(expectedValue);
            this.givenValue = Objects.requireNonNull(expectedValue);
        }

        public BigDecimal getExpectedValue() {
            return expectedValue;
        }

        public BigDecimal getGivenValue() {
            return givenValue;
        }
    }

    /**
     * <p>Exception type to be thrown when bound transaction data constraints violated</p>
     */
    public static class ConstraintsViolatedException extends TransactionItemValidationException implements Errors {
        private final Errors errors;

        public ConstraintsViolatedException(Errors errors, TransactionItem transaction) {
            super("Data constraints violated", transaction);
            this.errors = Objects.requireNonNull(errors);
        }

        public String getObjectName() {
            return errors.getObjectName();
        }

        public void setNestedPath(String nestedPath) {
            errors.setNestedPath(nestedPath);
        }

        public String getNestedPath() {
            return errors.getNestedPath();
        }

        public void pushNestedPath(String subPath) {
            errors.pushNestedPath(subPath);
        }

        public void popNestedPath() throws IllegalStateException {
            errors.popNestedPath();
        }

        public void reject(String errorCode) {
            errors.reject(errorCode);
        }

        public void reject(String errorCode, String defaultMessage) {
            errors.reject(errorCode, defaultMessage);
        }

        public void reject(String errorCode, Object[] errorArgs, String defaultMessage) {
            errors.reject(errorCode, errorArgs, defaultMessage);
        }

        public void rejectValue(String field, String errorCode) {
            errors.rejectValue(field, errorCode);
        }

        public void rejectValue(String field, String errorCode, String defaultMessage) {
            errors.rejectValue(field, errorCode, defaultMessage);
        }

        public void rejectValue(String field, String errorCode, Object[] errorArgs, String defaultMessage) {
            errors.rejectValue(field, errorCode, errorArgs, defaultMessage);
        }

        public void addAllErrors(Errors errors) {
            this.errors.addAllErrors(errors);
        }

        public boolean hasErrors() {
            return errors.hasErrors();
        }

        public int getErrorCount() {
            return errors.getErrorCount();
        }

        public List<ObjectError> getAllErrors() {
            return errors.getAllErrors();
        }

        public boolean hasGlobalErrors() {
            return errors.hasGlobalErrors();
        }

        public int getGlobalErrorCount() {
            return errors.getGlobalErrorCount();
        }

        public List<ObjectError> getGlobalErrors() {
            return errors.getGlobalErrors();
        }

        public ObjectError getGlobalError() {
            return errors.getGlobalError();
        }

        public boolean hasFieldErrors() {
            return errors.hasFieldErrors();
        }

        public int getFieldErrorCount() {
            return errors.getFieldErrorCount();
        }

        public List<FieldError> getFieldErrors() {
            return errors.getFieldErrors();
        }

        public FieldError getFieldError() {
            return errors.getFieldError();
        }

        public boolean hasFieldErrors(String field) {
            return errors.hasFieldErrors(field);
        }

        public int getFieldErrorCount(String field) {
            return errors.getFieldErrorCount(field);
        }

        public List<FieldError> getFieldErrors(String field) {
            return errors.getFieldErrors(field);
        }

        public FieldError getFieldError(String field) {
            return errors.getFieldError(field);
        }

        public Object getFieldValue(String field) {
            return errors.getFieldValue(field);
        }

        public Class<?> getFieldType(String field) {
            return errors.getFieldType(field);
        }
    }
}
