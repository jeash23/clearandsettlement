package com.huawei.tsel.settlement.batch;

import com.huawei.tsel.settlement.dto.UidAndMsisdnPair;
import com.huawei.tsel.settlement.dto.CorrelateUidRequest;
import com.huawei.tsel.settlement.dto.CorrelateUidResult;
import com.huawei.tsel.settlement.flow.CorrelateUidChannels;
import com.huawei.tsel.settlement.flow.CorrelateUidException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.resource.ListPreparedStatementSetter;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.Collections;

/**
 * <p>A Configuration of batch job for processing customer</p>
 *
 * @author zakyalvan
 * @since 11 Aug 2017
 */
@Configuration
public class PopulateCustomerMsisdnStepConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(PopulateCustomerMsisdnStepConfiguration.class);

    @Autowired
    private CorrelateUidChannels correlateUidChannels;

    /**
     * <p>An item reader for reading customer uid from transaction item database table
     * which msisdn still null</p>
     *
     * @return
     */
    @Bean
    JdbcCursorItemReader<String> customerUidReader(DataSource dataSource) {
        JdbcCursorItemReader<String> itemReader = new JdbcCursorItemReader<>();
        itemReader.setSaveState(false);
        itemReader.setFetchSize(10);
        itemReader.setDataSource(dataSource);
        itemReader.setSql("SELECT DISTINCT T.CUSTOMER_UID customerUid FROM SETTLE_OFFLINE_TRX T WHERE T.CUSTOMER_MSISDN IS NULL");

        ListPreparedStatementSetter preparedStatementSetter = new ListPreparedStatementSetter();
        preparedStatementSetter.setParameters(Collections.emptyList());
        itemReader.setPreparedStatementSetter(preparedStatementSetter);
        itemReader.setRowMapper((resultSet, rowNumber) -> resultSet.getString("customerUid"));
        return itemReader;
    }

    /**
     * <p>An {@link ItemProcessor} which will launching correlate customer uid integration flow</p>
     *
     * @return
     */
    @Bean("correlateUidProcessor")
    ItemProcessor<String, UidAndMsisdnPair> correlateUidProcessor() {
        return item -> {
            CorrelateUidRequest correlateRequest = CorrelateUidRequest.builder().uid(item).build();
            CorrelateUidResult correlateResult = correlateUidMessagingTemplate().convertSendAndReceive(correlateRequest, CorrelateUidResult.class);

            LOGGER.trace("Correlate uid result : {}", correlateResult);

            /**
             * TODO
             *
             * Currently filtering item, consider to throw an exception.
             */
            if(!correlateResult.getStatus().equals(CorrelateUidResult.CheckStatus.SUCCESS)) {
                LOGGER.error("Correlate uid failed, currently just ignore processed uid item.");
                return null;
            }
            return UidAndMsisdnPair.builder().uid(item).msisdn(correlateResult.getMsisdn()).build();
        };
    }

    /**
     * <p>Messaging template which responsible for sending correlate message request</p>
     *
     * @return
     */
    @Bean
    MessagingTemplate correlateUidMessagingTemplate() {
        return new MessagingTemplate(correlateUidChannels.correlateParameters());
    }

    @Bean("customerMsisdnWriter")
    JdbcBatchItemWriter<UidAndMsisdnPair> customerMsisdnWriter(DataSource dataSource) {
        JdbcBatchItemWriter<UidAndMsisdnPair> itemWriter = new JdbcBatchItemWriter<>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql("UPDATE SETTLE_OFFLINE_TRX SET CUSTOMER_MSISDN=:customerMsisdn WHERE CUSTOMER_UID=:customerUid AND CUSTOMER_MSISDN IS NULL");
        itemWriter.setItemSqlParameterSourceProvider(item -> new MapSqlParameterSource()
                .addValue("customerMsisdn", item.getMsisdn(), Types.VARCHAR)
                .addValue("customerUid", item.getUid(), Types.VARCHAR));
        return itemWriter;
    }

    @Bean("populateCustomerMsisdnStep")
    Step populateCustomerMsisdnStep(StepBuilderFactory steps,
                                @Qualifier("customerUidReader") ItemReader<String> itemReader,
                                @Qualifier("correlateUidProcessor") ItemProcessor<String, UidAndMsisdnPair> itemProcessor,
                                @Qualifier("customerMsisdnWriter") ItemWriter<UidAndMsisdnPair> itemWriter) {
        return steps.get("PopulateCustomerMsisdnStep")
                .<String, UidAndMsisdnPair> chunk(100).reader(itemReader).processor(itemProcessor).writer(itemWriter)
                .faultTolerant().noRollback(CorrelateUidException.class).processorNonTransactional()
                .build();
    }
}
