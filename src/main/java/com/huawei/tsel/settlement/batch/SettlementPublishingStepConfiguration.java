package com.huawei.tsel.settlement.batch;

import com.huawei.tsel.settlement.entity.InterfaceFile.ProcessingState;
import com.huawei.tsel.settlement.entity.SettlementSummary;
import com.huawei.tsel.settlement.entity.SettlementSummary.SettlementStatus;
import com.huawei.tsel.settlement.flow.BatchJobLaunchSettings;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 *
 *
 * @author zakyalvan
 * @since 9 Aug 2017
 * @see com.huawei.tsel.settlement.flow.SettlementPublishingFlowConfiguration
 */
@Configuration
public class SettlementPublishingStepConfiguration implements StepExecutionListener {
    @Autowired
    private TransactionSettlementService settlementService;

    @Bean("settlementPublishingStep")
    Step settlementPublishingStep(StepBuilderFactory steps, @Qualifier("settlementPublishingTasklet") Tasklet settlementPublishingTasklet) {
        return steps.get("SettlementPublishingStep").tasklet(settlementPublishingTasklet)
                .allowStartIfComplete(true)
                .listener(this)
                .build();
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        String interfaceFileId = stepExecution.getJobParameters().getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);
        settlementService.setProcessingState(interfaceFileId, ProcessingState.SETTLEMENT);
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        String interfaceFileId = stepExecution.getJobParameters().getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);
        settlementService.setProcessingState(interfaceFileId, ProcessingState.SETTLEMENT);
        return null;
    }

    /**
     * <p>A {@link Tasklet} implementation which responsible for starting publish settlement to cps system.</p>
     */
    @Component("settlementPublishingTasklet")
    public static class SettlementPublishingTasklet implements Tasklet {
        private static final Logger LOGGER = LoggerFactory.getLogger(SettlementPublishingTasklet.class);

        private TransactionSettlementService settlementService;

        private MessagingTemplate messagingTemplate;

        @Autowired
        public SettlementPublishingTasklet(@Qualifier("pushSettlementInput") MessageChannel settlementInputChannel, TransactionSettlementService settlementService) {
            this.messagingTemplate = new MessagingTemplate(Objects.requireNonNull(settlementInputChannel));
            this.settlementService = Objects.requireNonNull(settlementService);
        }

        @Override
        public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
            String interfaceFileId = chunkContext.getStepContext().getStepExecution().getJobParameters()
                    .getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);

            List<SettlementSummary> summaries = settlementService.getSettlementSummaries(interfaceFileId, SettlementStatus.NEW);
            for (SettlementSummary summary : summaries) {
                LOGGER.trace("Publish settlement summary ({}) to remote cps server", summary);
                Message<SettlementSummary> settlementMessage = MessageBuilder.withPayload(summary).build();
                messagingTemplate.send(settlementMessage);
            }
            return RepeatStatus.FINISHED;
        }
    }
}
