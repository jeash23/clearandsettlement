package com.huawei.tsel.settlement.batch;

import com.huawei.tsel.common.integration.EndpointAddress;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>Settings for settlement processing.
 *
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@Data
@Component
@ConfigurationProperties(prefix = "huawei.settlement.batch-process")
public class SettlementProcessingSettings implements InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(SettlementProcessingSettings.class);

    @Valid
    @NestedConfigurationProperty
    private final FileReadingSettings reading = new FileReadingSettings();

    @Valid
    @NestedConfigurationProperty
    private final TransactionValidationSettings validation = new TransactionValidationSettings();

    /**
     * @use com.huawei.tsel.settlement.flow.SettlementPublishingSettings
     */
    @Valid
    @Deprecated
    @NestedConfigurationProperty
    private final SettlementEndpointSettings endpoint = new SettlementEndpointSettings();

    /**
     * <p>Reading delimited line interface file settings</p>
     */
    @Data
    @SuppressWarnings("serial")
    public static class FileReadingSettings implements Serializable {
        /**
         * Used encoding scheme.
         */
        private String textEncoding = "UTF-8";

        private String headerLinePrefix = "H|";

        private String footerLinePrefix = "F|";

        private String fieldSeparator = "|";

        private String transactionDateFormat = "yyyyMMddHHmmss";
    }

    /**
     * <p>Push settlement to cps api request settings</p>
     */
    @Data
    @SuppressWarnings("serial")
    public static class SettlementEndpointSettings implements Serializable {
        @Valid
        @NotNull
        @NestedConfigurationProperty
        private EndpointAddress address = new EndpointAddress("http", "10.49.0.10",30001, "payment/services/SYNCAPIRequestMgrService");

        @NotNull
        private Integer callerType = 2;

        @NotBlank
        private String callerIdentifier = "POS_Broker";

        @NotBlank
        private String callerPassword = "gMMqGGrKxsE=";

        @NotBlank
        private String initiatorType = "14";

        @NotBlank
        private String initiatorIdentifier = "dwitest";

        @NotBlank
        private String initiatorCredential = "gMMqGGrKxsE=";

        @NotBlank
        private String receiverType = "4";

        private String transactionCurrency = "IDR";
    }

    /**
     * <p>Settings for transaction item validation</p>
     *
     * @see InterfaceFileValidationStepConfiguration
     * @see InterfaceFileValidationItemProcessor
     */
    @Data
    @SuppressWarnings("serial")
    public static class TransactionValidationSettings implements Serializable {
        /**
         * <p>Flag whether to validate constraint defined in transaction bean properties</p>
         */
        private boolean validateBeanConstraints = false;

        /**
         * <p>Flag whether to validate transaction's purse counter</p>
         */
        private boolean validatePurseCounter = false;

        /**
         * <p>Flag determine whether to validate last balance state of each transaction item</p>
         */
        private boolean validateLastBalance = false;

        /**
         * <p>Flag whether to use strict counter start value defined in {@link #counterStartValue}</p>
         */
        private boolean strictCounterStart = false;

        /**
         * <p>Default customer purse counter start value</p>
         */
        @NotNull
        @Max(0x7fffffff)
        private Long counterStartValue = Long.valueOf(0x7fffffff);

        /**
         * <p>Flag whether to check purse counter decrement against loaded counter from another settlement interface file,
         * if set to false, validation only check purse counter in current processed file</p>
         */
        private boolean regardsPersistentCounter = false;

        /**
         * <p>Flog whether purse counter for each customer uid in settlement file always decrement by constant value</p>
         */
        private boolean constantCounterDecrement = false;

        /**
         * <p>Counter decrement value, by default this value is set to 1. Please note, this settings
         * only applicable when {@link #constantCounterDecrement} set to true.</p>
         */
        @NotNull
        @Min(0x1)
        private Integer counterDecrementValue = 1;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.trace(">>>>>>>>>>>>>>>>> Validate purse counter : {}", getValidation().isValidatePurseCounter());
    }
}
