package com.huawei.tsel.settlement.batch;

import com.huawei.tsel.settlement.dto.FooterLine;
import com.huawei.tsel.settlement.entity.InterfaceFile.ProcessingState;
import com.huawei.tsel.settlement.entity.InterfaceLine;
import com.huawei.tsel.settlement.entity.InterfaceSummary;
import com.huawei.tsel.settlement.entity.TransactionCounter;
import com.huawei.tsel.settlement.entity.TransactionItem;
import com.huawei.tsel.settlement.flow.BatchJobLaunchSettings;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * @author zakyalvan
 * @since 8 Aug 2017
 */
@Component
public class TransactionLoadingListener extends StepListenerSupport<InterfaceLine, TransactionItem> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionLoadingListener.class);

    @Autowired
    private TransactionSettlementService settlementService;

    /**
     * Current {@link JobExecution}.
     */
    private JobExecution jobExecution;

    /**
     * <p>Current chunk written items</p>
     */
    private List<TransactionItem> chunkItems = new ArrayList<>();

    private ThreadLocal<InterfaceSummary> intefaceSummary = new ThreadLocal<InterfaceSummary>() {
        @Override
        protected InterfaceSummary initialValue() {
            return InterfaceSummary.builder().calculatedAmount(BigDecimal.ZERO).calculatedCount(0l)
                    .declaredAmount(BigDecimal.ZERO).declaredCount(0l)
                    .build();
        }
    };

    @Override
    public void beforeStep(StepExecution stepExecution) {
        jobExecution = stepExecution.getJobExecution();

        String interfaceFileId = stepExecution.getJobParameters().getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);
        settlementService.setProcessingState(interfaceFileId, ProcessingState.LOADING);
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        String interfaceFileId = stepExecution.getJobParameters().getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);
        settlementService.setProcessingState(interfaceFileId, ProcessingState.LOADED);
        return null;
    }

    @Override
    public void beforeChunk(ChunkContext context) {
        chunkItems.clear();
    }

    @Override
    public void afterChunk(ChunkContext context) {
        LOGGER.trace("Create transaction counter for tracking purpose");
        for(TransactionItem item : chunkItems) {
            TransactionCounter counter = TransactionCounter.builder()
                    .customerUid(item.getCustomerUid()).lastPurseCounter(item.getPurseCounter()).lastBalance(item.getLastBalance())
                    .build();

            settlementService.setTransactionCounter(counter);
        }
    }

    @Override
    public void afterChunkError(ChunkContext context) {
        LOGGER.trace("After chunk processing error, clear current chunk processed items");
        chunkItems.clear();
    }


    @Override
    public void afterRead(InterfaceLine item) {
        if (item instanceof TransactionItem) {
            String settlementFileId = jobExecution.getJobParameters().getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER);
            LOGGER.trace("Assign settlement interface file id {} to transaction item {}", settlementFileId, ((TransactionItem) item).getRecordId());
            ((TransactionItem) item).setInterfaceFileId(settlementFileId);
        }
    }


    @Override
    public void beforeProcess(InterfaceLine item) {
        if (item instanceof FooterLine) {

        }
    }


    @Override
    public void afterWrite(List<? extends TransactionItem> items) {
        LOGGER.trace("After items written, add all item into current processing chunk items");
        chunkItems.addAll(items);
    }
}
