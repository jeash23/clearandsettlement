package com.huawei.tsel.settlement.batch;

import java.io.IOException;

import com.huawei.tsel.settlement.flow.BatchJobLaunchSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.JobParametersValidator;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

/**
 * <p>A {@link JobParametersValidator} responsible for validating {@link JobParameters} supplied for
 * {@link SettlementProcessingJobConfiguration}</p>
 *
 * @author zakyalvan
 * @since 7 Aug 2017
 * @see SettlementProcessingJobConfiguration
 */
public class SettlementJobParametersValidator implements JobParametersValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(SettlementJobParametersValidator.class);

    @Override
    public void validate(JobParameters parameters) throws JobParametersInvalidException {
        LOGGER.debug("Validate job parameters.");

        if (!StringUtils.hasText(parameters.getString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER))) {
            LOGGER.error("No settlement interface file id parameter given (null or empty) string provided");
            throw new JobParametersInvalidException("No settlement interface file id parameter given (null or empty) string provided");
        }

        if (!StringUtils.hasText(parameters.getString(BatchJobLaunchSettings.FILE_NAME_PARAMETER))) {
            LOGGER.error("No settlement file name parameter given or null or empty string provided");
            throw new JobParametersInvalidException("No settlement file name parameter given or null or empty string provided");
        }

        Resource interfaceFile = new FileSystemResource(parameters.getString(BatchJobLaunchSettings.FILE_NAME_PARAMETER));
        if (!interfaceFile.exists()) {
            LOGGER.error("Given settlement file is not exists");
            throw new JobParametersInvalidException("Given settlement file is not exists");
        }

        if (StringUtils.hasText(parameters.getString(BatchJobLaunchSettings.FILE_CHECKSUM_PARAMETER))) {
            try {
                String calculatedHash = DigestUtils.md5DigestAsHex(interfaceFile.getInputStream());
                if (!calculatedHash.equals(parameters.getString(BatchJobLaunchSettings.FILE_CHECKSUM_PARAMETER))) {
                    LOGGER.error("Given file checksum is invalid, it means file changed after fetched and before processed");
                    throw new JobParametersInvalidException("Given file checksum is invalid, it means file changed after fetched and before processed");
                }
            }
            catch (IOException ioe) {
                LOGGER.error("An ignorable exception thrown on file checksum calculation, see stack trace for details", ioe);
            }
        }
    }
}
