package com.huawei.tsel.settlement.batch;

import com.huawei.tsel.settlement.batch.InterfaceFileValidationException.*;

import com.huawei.tsel.settlement.entity.InterfaceLine;
import com.huawei.tsel.settlement.entity.TransactionCounter;
import com.huawei.tsel.settlement.entity.TransactionItem;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>An {@link ItemProcessor} implementation responsible for validating an {@link TransactionItem}
 * including validation against {@link TransactionCounter}. This item processor will be used in validating item line given
 * in settlement interface file, before loading them into database and doing the settlement to mfs</p>
 *
 * @author zakyalvan
 * @since 11 Sep 2017
 * @see  InterfaceFileValidationStepConfiguration
 */
public class InterfaceFileValidationItemProcessor implements ItemProcessor<InterfaceLine, TransactionItem> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InterfaceFileValidationItemProcessor.class);

    /**
     * <p>In memory Cache of transaction counters</p>
     */
    private final Map<String, TransactionCounter> transactionCounters = new ConcurrentHashMap<>();

    private TransactionSettlementService settlementService;

    private SettlementProcessingSettings processSettings;

    private SmartValidator beanValidator;

    @Autowired
    public void setSettlementService(TransactionSettlementService settlementService) {
        this.settlementService = settlementService;
    }

    @Autowired
    public void setProcessSettings(SettlementProcessingSettings processSettings) {
        this.processSettings = processSettings;
    }

    @Autowired
    public void setBeanValidator(SmartValidator beanValidator) {
        this.beanValidator = beanValidator;
    }

    /**
     * <p>Clear internal in memory transaction counter cache, this is useful on processing new file</p>
     */
    public void resetTransactionCounters() {
        LOGGER.trace("Reset (clearing) internal in memory transaction counter cache");
        transactionCounters.clear();
    }

    @Override
    public TransactionItem process(InterfaceLine line) throws Exception {
        if(!TransactionItem.class.isAssignableFrom(line.getClass())) {
            LOGGER.trace("Filter settlement-file-line ({}) which are not a transaction-item type", line);
            return null;
        }
        TransactionItem transaction = (TransactionItem) line;

        if(processSettings.getValidation().isValidateBeanConstraints()) {
            validateBeanConstraints(transaction);
        }

        validateTransactionMerchant(transaction);

        validateTransactionUniqueness(transaction);

        if(processSettings.getValidation().isValidatePurseCounter()) {
            boolean regardsPersistence = processSettings.getValidation().isRegardsPersistentCounter();
            TransactionCounter counter = resolveTransactionCounter(transaction, regardsPersistence);

            if(counter == null) {
                if(!regardsPersistence) {
                    boolean previousTransactionExists = settlementService.hasCustomerTransactionsWithUid(transaction.getCustomerUid());
                    if(!previousTransactionExists) {
                        validatePurseCounter(transaction);
                    }
                }
                else {
                    validatePurseCounter(transaction);
                }
            }
            else {
                validatePurseCounter(transaction, counter);
            }
        }
        updateTransactionCounter(transaction);
        return transaction;
    }

    /**
     * <p>Validate bean constraints of {@link TransactionItem}</p>
     *
     * @param transaction
     */
    private void validateBeanConstraints(TransactionItem transaction) {
        Errors errors = new BeanPropertyBindingResult(transaction, "item");
        beanValidator.validate(transaction, errors);
        if(errors.hasErrors()) {
            LOGGER.trace("Error found on data binding of transaction-item property constraints of {}", transaction);
            throw new ConstraintsViolatedException(errors, transaction);
        }
    }

    /**
     * <p>Validate transaction merchant, only check whether there is mapping between Verifone merchant uid to mfs short code</p>
     *
     * @param transaction
     */
    private void validateTransactionMerchant(TransactionItem transaction) {
        if(!settlementService.hasMerchantMapping(transaction.getMerchantId())) {
            LOGGER.error("No merchant mapping to mfs's short-code found for Youtap merchant-id '{}'", transaction.getMerchantId());
            throw new MerchantNotMappedException(transaction.getMerchantId(), transaction);
        }
    }

    /**
     * <p>Validate transaction to make sure no duplicates found based on customer uid and purse counter.</p>
     *
     * @param transaction
     */
    private void validateTransactionUniqueness(TransactionItem transaction) {
        if(settlementService.hasCustomerTransactions(transaction.getCustomerUid(), transaction.getPurseCounter())) {
            TransactionItem duplicate = settlementService.getCustomerTransaction(transaction.getCustomerUid(), transaction.getPurseCounter());
            throw new DuplicatePurseCounterException(transaction, duplicate, transaction.getPurseCounter());
        }
    }

    /**
     * <p>Resolve {@link TransactionCounter} for given {@link TransactionItem} from in memory cache,
     * or fallback to persistence store if not found, or return null in case not resolvable from both source.</p>
     *
     * @param transaction
     * @param fallbackPersistence
     * @return
     */
    private TransactionCounter resolveTransactionCounter(TransactionItem transaction, boolean fallbackPersistence) {
        LOGGER.trace("Resolve in-memory cache if transaction counter for customer with uid {}", transaction.getCustomerUid());
        TransactionCounter transactionCounter = transactionCounters.get(transaction.getCustomerUid());

        if(transactionCounter == null && fallbackPersistence && settlementService.hasTransactionCounter(transaction.getCustomerUid())) {
            LOGGER.trace("In-memory cache transaction-counter for customer with uid {} not found, load persisted transaction-counter from database", transaction.getCustomerUid());
            transactionCounter = settlementService.getTransactionCounter(transaction.getCustomerUid());
        }
        return transactionCounter;
    }

    /**
     * <p>Validate purse counter of given new {@link TransactionItem}. This means, processed item is first
     * transaction for customer determined by uid</p>
     *
     * @param transaction
     */
    private void validatePurseCounter(TransactionItem transaction) {
        Long startCounter = processSettings.getValidation().getCounterStartValue();
        Long givenCounter = transaction.getPurseCounter();
        if(processSettings.getValidation().isStrictCounterStart()) {
            LOGGER.trace("Purse counter must be started with strict start value {} (0x{})", startCounter, Long.toHexString(startCounter));

            if(!startCounter.equals(givenCounter)) {
                LOGGER.error("Invalid purse counter start given {} (0x{}), expected value is equals to {} (0x{}).", givenCounter, Long.toHexString(givenCounter), startCounter, Long.toHexString(startCounter));
                throw new PurseCounterStartException(transaction, startCounter, givenCounter, true);
            }
        }
        else {
            LOGGER.trace("Purse counter must be started with value lower than or equals {} (0x{})", startCounter, Long.toHexString(startCounter));
            if(givenCounter > startCounter) {
                LOGGER.error("Invalid purse counter start given {} (0x{}), expected value is equals to {} (0x{}).", givenCounter, Long.toHexString(givenCounter), startCounter, Long.toHexString(startCounter));
                throw new PurseCounterStartException(transaction, startCounter, givenCounter, false);
            }
        }
    }

    /**
     * <p>Validate purse counter of given {@link TransactionItem} against {@link TransactionCounter}. This means,
     * customer qualified by uid already have transaction loaded before.</p>
     *
     * @param transaction
     * @param counter
     */
    private void validatePurseCounter(TransactionItem transaction, TransactionCounter counter) {
        Long lastCounter = counter.getLastPurseCounter();
        Long givenCounter = transaction.getPurseCounter();
        if(processSettings.getValidation().isConstantCounterDecrement()) {
            Integer counterDecrement = processSettings.getValidation().getCounterDecrementValue();
            LOGGER.trace("Purse counter must be decremented by constant value {}", counterDecrement);

            Long expectedCounter = lastCounter - counterDecrement;
            if(expectedCounter != givenCounter) {
                LOGGER.error("Invalid purse counter given {} (0x{}), expected value is equals to {} (0x{}).", givenCounter, Long.toHexString(givenCounter), expectedCounter, Long.toHexString(expectedCounter));
                throw new PurseCounterDecrementException(transaction, lastCounter, givenCounter, counterDecrement);
            }
        }
        else {
            LOGGER.trace("Purse counter decrement not by constant value");
            if(givenCounter >= lastCounter) {
                LOGGER.error("Invalid purse counter given {} (0x{}), expected value is lower than {} (0x{})", givenCounter, Long.toHexString(givenCounter), lastCounter, Long.toHexString(lastCounter));
                throw new PurseCounterDecrementException(transaction, lastCounter, givenCounter);
            }
        }
    }

    /**
     * <p>Update transaction counter in memory cache</p>
     *
     * @param transaction
     */
    private void updateTransactionCounter(TransactionItem transaction) {
        String customerUid = transaction.getCustomerUid();
        Long purseCounter = transaction.getPurseCounter();
        BigDecimal lastBalance = transaction.getLastBalance();
        LocalDateTime lastUpdate = LocalDateTime.now();
        TransactionCounter transactionCounter = TransactionCounter.builder()
                .customerUid(customerUid).lastPurseCounter(purseCounter).lastBalance(lastBalance).lastUpdate(lastUpdate)
                .build();

        LOGGER.trace("Update in-memory transaction counter cache for uid {} with {}", customerUid, transactionCounter);
        transactionCounters.put(customerUid, transactionCounter);
    }
}
