package com.huawei.tsel.settlement.batch;

import com.huawei.tsel.settlement.dto.FooterLine;
import com.huawei.tsel.settlement.dto.HeaderLine;
import com.huawei.tsel.settlement.entity.InterfaceFile;
import com.huawei.tsel.settlement.entity.InterfaceLine;
import com.huawei.tsel.settlement.entity.TransactionItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.support.FormatterPropertyEditorAdapter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.validation.DataBinder;

import javax.sql.DataSource;
import java.beans.PropertyEditorSupport;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p></p>
 *
 * @author zakyalvan
 * @since 13 Aug 2017
 */
@Configuration
public class TransactionLoadingStepConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionLoadingStepConfiguration.class);

    @Autowired
    private SettlementProcessingSettings processSettings;

    /**
     * <p>Read {@link TransactionItem} from processed {@link InterfaceFile}.
     * Please note we must read header and footer of file also.
     *
     * @return
     */
    @StepScope
    @Bean("settlementFileReader")
    FlatFileItemReader<InterfaceLine> settlementFileReader(@Value("file://#{jobParameters['interfaceFile']}") Resource interfaceFile) {
        FlatFileItemReader<InterfaceLine> itemReader = new FlatFileItemReader<>();
        itemReader.setStrict(true);
        itemReader.setLineMapper(settlementLineMapper());
        itemReader.setResource(interfaceFile);
        itemReader.setEncoding(processSettings.getReading().getTextEncoding());
        itemReader.setSaveState(true);
        return itemReader;
    }

    /**
     * <p>Interface file lines mapper.
     *
     * @return
     */
    @Bean
    LineMapper<InterfaceLine> settlementLineMapper() {
        PatternMatchingCompositeLineMapper<InterfaceLine> lineMapper = new PatternMatchingCompositeLineMapper<InterfaceLine>() {
            @Override
            public InterfaceLine mapLine(String lineContent, int lineNumber) throws Exception {
                InterfaceLine interfaceLine = super.mapLine(lineContent, lineNumber);
                interfaceLine.setLineContent(lineContent);
                interfaceLine.setLineNumber(lineNumber);
                return interfaceLine;
            }
        };

        Map<String, LineTokenizer> lineTokenizer = new HashMap<>();
        lineTokenizer.put("H|*", headerLineTokenizer());
        lineTokenizer.put("F|*", footerLineTokenizer());
        lineTokenizer.put("*", transactionLineTokenizer());

        lineMapper.setTokenizers(lineTokenizer);

        Map<String, FieldSetMapper<InterfaceLine>> fieldSetMappers = new HashMap<>();
        fieldSetMappers.put("H|*", headerFieldSetMapper());
        fieldSetMappers.put("F|*", footerFieldSetMapper());
        fieldSetMappers.put("*", transactionFieldSetMapper());

        lineMapper.setFieldSetMappers(fieldSetMappers);

        return lineMapper;
    }

    @Bean
    LineTokenizer headerLineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"headerSign"});
        lineTokenizer.setDelimiter("|");
        lineTokenizer.setStrict(false);
        return lineTokenizer;
    }

    @Bean
    FieldSetMapper<InterfaceLine> headerFieldSetMapper() {
        BeanWrapperFieldSetMapper<InterfaceLine> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(HeaderLine.class);
        fieldSetMapper.setStrict(false);
        return fieldSetMapper;
    }

    @Bean
    LineTokenizer transactionLineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"customerUid", "transactionAmount", "transactionId", "transactionDate", "merchantId", "terminalId", "lastBalance", "purseCounter"});
        lineTokenizer.setDelimiter("|");
        lineTokenizer.setStrict(true);
        return lineTokenizer;
    }

    @Bean
    FieldSetMapper<InterfaceLine> transactionFieldSetMapper() {
        BeanWrapperFieldSetMapper<InterfaceLine> fieldSetMapper = new BeanWrapperFieldSetMapper<InterfaceLine>() {
            @Override
            protected void initBinder(DataBinder binder) {
                binder.setIgnoreUnknownFields(false);
                /**
                 * Custom editor for transaction date field so that can parse date in "yyyyMMddhh24miss" format.
                 */
                DateFormatter transactionDateFormat = new DateFormatter(processSettings.getReading().getTransactionDateFormat());
                binder.registerCustomEditor(Date.class, "transactionDate", new FormatterPropertyEditorAdapter(transactionDateFormat));

                /**
                 * Register editor for purse counter.
                 */
                binder.registerCustomEditor(Long.class, "purseCounter", new PropertyEditorSupport() {
                    @Override
                    public void setAsText(String text) throws IllegalArgumentException {
                        try {
                            setValue(Long.parseLong(text, 16));
                        } catch (NumberFormatException nfe) {
                            LOGGER.error("Can not parse purse counter which expected to be hex number representation");
                            throw new IllegalArgumentException("Can not parsing purse counter of transaction line, see stack traces", nfe);
                        }
                    }
                });
            }
        };
        fieldSetMapper.setTargetType(TransactionItem.class);
        fieldSetMapper.setStrict(true);
        return fieldSetMapper;
    }

    @Bean
    LineTokenizer footerLineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"footerSign", "transactionCount", "transactionAmount"});
        lineTokenizer.setDelimiter("|");
        lineTokenizer.setStrict(true);
        return lineTokenizer;
    }

    @Bean
    FieldSetMapper<InterfaceLine> footerFieldSetMapper() {
        BeanWrapperFieldSetMapper<InterfaceLine> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(FooterLine.class);
        fieldSetMapper.setStrict(false);
        return fieldSetMapper;
    }

    /**
     * <p>An batch {@link ItemProcessor} responsible for filtering {@link InterfaceLine}
     * which will pass {@link TransactionItem} type only.
     * <p>
     * TODO Record info read from footer.
     *
     * @return
     */
    @Bean("settlementLineProcessor")
    ItemProcessor<InterfaceLine, TransactionItem> settlementLineProcessor() {
        return item -> {
            if (!TransactionItem.class.isAssignableFrom(item.getClass())) {
                /**
                 * Returning null means filtering item, or no need to processing items further.
                 */
                return null;
            }

            return (TransactionItem) item;
        };
    }

    /**
     * <p>An {@link ItemWriter} responsible for writing {@link TransactionItem} into database,
     * so that can be queried later, e.g. for reposting purpose.
     *
     * @return
     */
    @Bean("transactionItemWriter")
    JdbcBatchItemWriter<TransactionItem> transactionItemWriter(DataSource dataSource) {
        String sqlString = "INSERT INTO SETTLE_OFFLINE_TRX "
                + "(RECORD_ID, CUSTOMER_UID, MERCHANT_ID, TERMINAL_ID, TRANSACTION_ID, TRANSACTION_DATE, TRANSACTION_AMOUNT, LAST_BALANCE, PURSE_COUNTER, LOADED_DATE, SETTLEMENT_FILE_ID) "
                + "VALUES (:recordId, :customerUid, :merchantId, :terminalId, :transactionId, :transactionDate, :transactionAmount, :lastBalance, :purseCounter, :loadedTimestamp, :interfaceFileId)";

        JdbcBatchItemWriter<TransactionItem> itemWriter = new JdbcBatchItemWriter<>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql(sqlString);
        itemWriter.setItemSqlParameterSourceProvider(transaction -> new MapSqlParameterSource()
                .addValue("recordId", transaction.getRecordId(), Types.VARCHAR)
                .addValue("interfaceFileId", transaction.getInterfaceFileId(), Types.VARCHAR)
                .addValue("merchantId", transaction.getMerchantId(), Types.VARCHAR)
                .addValue("terminalId", transaction.getTerminalId(), Types.VARCHAR)
                .addValue("customerUid", transaction.getCustomerUid(), Types.VARCHAR)
                .addValue("transactionId", transaction.getTransactionId(), Types.VARCHAR)
                .addValue("transactionDate", transaction.getTransactionDate(), Types.TIMESTAMP)
                .addValue("transactionAmount", transaction.getTransactionAmount(), Types.DECIMAL)
                .addValue("lastBalance", transaction.getLastBalance(), Types.DECIMAL)
                .addValue("purseCounter", transaction.getPurseCounter(), Types.BIGINT)
                .addValue("loadedTimestamp", new Date(), Types.TIMESTAMP));
        return itemWriter;
    }

    /**
     * <p>A batch {@link Step} for loading settlement file into database.
     *
     * @param steps
     * @param itemReader
     * @param itemProcessor
     * @param itemWriter
     * @return
     */
    @Bean("transactionLoadingStep")
    Step transactionLoadingStep(StepBuilderFactory steps,
                                @Qualifier("settlementFileReader") ItemReader<InterfaceLine> itemReader,
                                @Qualifier("settlementLineProcessor") ItemProcessor<InterfaceLine, TransactionItem> itemProcessor,
                                @Qualifier("transactionItemWriter") ItemWriter<TransactionItem> itemWriter) {
        return steps.get("TransactionLoadingStep")
                .<InterfaceLine, TransactionItem> chunk(20).reader(itemReader).processor(itemProcessor).writer(itemWriter)
                .listener((StepExecutionListener) transactionLoadingListener())
                .listener((ChunkListener) transactionLoadingListener())
                .listener((ItemReadListener<InterfaceLine>) transactionLoadingListener())
                .listener((ItemWriteListener<TransactionItem>) transactionLoadingListener())
                .build();
    }

    @Bean("transactionLoadingListener")
    TransactionLoadingListener transactionLoadingListener() {
        return new TransactionLoadingListener();
    }
}
