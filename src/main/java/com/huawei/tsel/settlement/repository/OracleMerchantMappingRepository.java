package com.huawei.tsel.settlement.repository;

import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.huawei.tsel.settlement.entity.MerchantMapping;

/**
 * 
 * @author zakyalvan
 * @since 15 Aug 2017
 */
@Validated
@Repository
@Transactional(readOnly=true)
public class OracleMerchantMappingRepository extends NamedParameterJdbcDaoSupport implements MerchantMappingRepository {
	private RowMapper<MerchantMapping> defaultRowMapper = (resultSet, rowNumber) -> MerchantMapping.builder()
			.merchantId(resultSet.getString("merchantId"))
			.shortCode(resultSet.getString("shortCode"))
			.merchantName(resultSet.getString("merchantName"))
			.build();
	
	@Autowired
	public OracleMerchantMappingRepository(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void insertOne(MerchantMapping merchantMapping) {
		String sqlString = "INSERT INTO SETTLE_MERCHANT_SHORT_CODE (MERCHANT_ID, SHORT_CODE, MERCHANT_NAME) VALUES (:merchantId, :shortCode, :merchantName)";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource()
				.addValue("merchantId", merchantMapping.getMerchantId(), Types.VARCHAR)
				.addValue("shortCode", merchantMapping.getShortCode(), Types.VARCHAR)
				.addValue("merchantName", merchantMapping.getMerchantName(), Types.VARCHAR);
		getNamedParameterJdbcTemplate().update(sqlString, parameterSource);
	}

	@Override
	public boolean exists(String merchantId) {
		String sqlString = "SELECT CASE WHEN EXISTS(SELECT SC.MERCHANT_ID FROM SETTLE_MERCHANT_SHORT_CODE SC WHERE SC.MERCHANT_ID=:merchantId) THEN 1 ELSE 0 END FROM DUAL";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource()
				.addValue("merchantId", merchantId, Types.VARCHAR);
		return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameterSource, Integer.class) == 1;
	}

	@Override
	public MerchantMapping selectOne(String merchantId) {
		String sqlString = "SELECT SC.MERCHANT_ID merchantId, SC.SHORT_CODE shortCode, SC.MERCHANT_NAME merchantName FROM SETTLE_MERCHANT_SHORT_CODE SC WHERE SC.MERCHANT_ID=:merchantId";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource()
				.addValue("merchantId", merchantId, Types.VARCHAR);
		return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameterSource, defaultRowMapper);
	}	
}
