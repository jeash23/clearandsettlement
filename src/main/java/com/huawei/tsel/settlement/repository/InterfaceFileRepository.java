package com.huawei.tsel.settlement.repository;

import com.huawei.tsel.settlement.entity.InterfaceFile;
import com.huawei.tsel.settlement.entity.InterfaceFile.ProcessingState;
import com.huawei.tsel.settlement.entity.RejectionReason;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author zakyalvan
 * @since 31 Jul 2017
 */
public interface InterfaceFileRepository {
    InterfaceFile insertOne(@Valid @NotNull InterfaceFile interfaceFile);

    boolean exists(String interfaceFileId);

    boolean existsByOriginalName(String originalFileName);

    Long countAll();

    InterfaceFile selectOne(@NotBlank String interfaceFileId);

    List<InterfaceFile> selectAllByStatusOrderByPriorityAsc(ProcessingState status);

    Page<InterfaceFile> selectPage(@NotNull Pageable pageable);

    ProcessingState selectOneProcessingState(@NotBlank String interfaceFileId);

    void updateProcessingState(@NotBlank String interfaceFileId, ProcessingState processingState);

    void updateRejectionReason(@NotBlank String interfaceFileId, RejectionReason rejectionReason);

    void updateProcessedTransactionCount(@NotBlank String interfaceFileId, Integer transactionCount);

    void updateDeclaredTransactionCount(@NotBlank String interfaceFileId, Integer transactionCount);

    void updateDeclaredAmount(@NotBlank String interfaceFileId, BigDecimal declaredAmount);

    void updateCalculatedTotal(@NotBlank String interfaceFileId, BigDecimal calculatedTotal);
}
