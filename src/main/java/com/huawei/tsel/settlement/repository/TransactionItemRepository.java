package com.huawei.tsel.settlement.repository;

import com.huawei.tsel.settlement.entity.TransactionItem;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;

/**
 * @author zakyalvan
 * @since 11 Aug 2017
 */
public interface TransactionItemRepository {
    /**
     * Check whether customer's {@link TransactionItem} exists, determined by their msisdn.
     *
     * @param customerMsisdn
     * @return
     */
    boolean existsByCustomerMsisdn(@NotBlank String customerMsisdn);

    /**
     * <p>Check whether transaction item exists interface file</p>
     *
     * @param interfaceFileIs
     * @return
     */
    boolean existsByInterfaceFile(@NotBlank String interfaceFileIs);

    /**
     * <p>Check whether {@link TransactionItem} with given customer uid exists</p>
     *
     * @param customerUid
     * @return
     */
    boolean existsByCustomerUid(@NotBlank String customerUid);

    /**
     * <p>Check whether {@link TransactionItem} with given customer id and purse counter already exists</p>
     *
     * @param customerUid
     * @param purseCounter
     * @return
     */
    boolean existsByCustomerUidAndPurseCounter(@NotBlank String customerUid, @NotNull Long purseCounter);

    /**
     *
     * @param customerMsisdn
     * @return
     */
    long countAllByCustomerMsisdn(@NotBlank String customerMsisdn);

    long countAllByInterfaceFile(@NotBlank String interfaceFileId);

    /**
     * <p>Select one {@link TransactionItem} based on customer uid and purse counter value</p>
     *
     * @param customerUid
     * @param purseCounter
     * @return
     */
    TransactionItem selectOneByCustomerUidAndPurseCounter(String customerUid, Long purseCounter);

    /**
     * <p>Select all customer </p>
     *
     * @param customerMsisdn
     * @param pageable
     * @return
     */
    Page<TransactionItem> selectAllByCustomerMsisdn(@NotBlank String customerMsisdn, @NotNull Pageable pageable);

    Page<TransactionItem> selectAllByInterfaceFile(@NotBlank String interfaceFileId, @NotNull Pageable pageable);
}
