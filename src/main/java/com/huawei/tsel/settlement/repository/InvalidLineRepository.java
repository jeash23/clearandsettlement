package com.huawei.tsel.settlement.repository;

import com.huawei.tsel.settlement.entity.InvalidLine;

import java.util.List;

/**
 * @author zakyalvan
 * @since 18 Sep 2017
 */
public interface InvalidLineRepository {
    void insertAll(String interfaceFileId, List<InvalidLine> invalidLines);
    List<InvalidLine> selectAll(String interfaceFileId);
    boolean exists(String interfaceFileId);
    void deleteAll(String interfaceFileId);
}
