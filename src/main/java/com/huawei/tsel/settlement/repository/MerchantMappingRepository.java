package com.huawei.tsel.settlement.repository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.huawei.tsel.settlement.entity.MerchantMapping;

/**
 * 
 * @author zakyalvan
 * @since 15 Aug 2017
 */
public interface MerchantMappingRepository {
	void insertOne(@Valid @NotNull MerchantMapping merchantMapping);
	boolean exists(@NotBlank String merchantId);
	MerchantMapping selectOne(@NotBlank String merchantId);
}
