package com.huawei.tsel.settlement.repository;

import org.hibernate.validator.constraints.NotBlank;
import com.huawei.tsel.settlement.entity.TransactionCounter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author zakyalvan
 */
public interface TransactionCounterRepository {
    void insert(@Valid @NotNull TransactionCounter transactionCounter);

    boolean exists(String customerUid);

    TransactionCounter selectOne(@NotBlank String customerUid);

    void update(@Valid @NotNull TransactionCounter transactionCounter);

    void delete(@NotBlank String customerUid);
}
