package com.huawei.tsel.settlement.repository;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.huawei.tsel.settlement.entity.ResultDescriptor;
import org.hibernate.validator.constraints.NotBlank;

import com.huawei.tsel.settlement.entity.SettlementSummary;
import com.huawei.tsel.settlement.entity.SettlementSummary.SettlementStatus;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author zakyalvan
 * @since 7 Aug 2017
 */
public interface SettlementSummaryRepository {
    /**
     * Calculate merchant transaction summary from an settlement interface file.
     *
     * @return
     */
    List<SettlementSummary> calculateSummaries(String interfaceFileId);

    /**
     * Insert one settlement summary item into database.
     *
     * @param settlementSummary
     * @return
     */
    void insertOne(@Valid @NotNull SettlementSummary settlementSummary);

    void insertAll(@NotEmpty List<SettlementSummary> settlementSummaries);

    boolean exists(String settlementSummaryId);

    boolean existsByInterfaceFile(@NotBlank String interfaceFileId);

    long countAll();

    long countAllByInterfaceFile(@NotBlank String interfaceFileId);

    /**
     *
     *
     * @param settlementSummaryId
     * @return
     */
    SettlementSummary selectOne(@NotBlank String settlementSummaryId);

    /**
     * @param interfaceFileId
     * @return
     */
    SettlementSummary selectOneByInterfaceFile(@NotBlank String interfaceFileId);

    /**
     * @param settlementFileId
     * @return
     */
    @Deprecated
    List<SettlementSummary> selectAllNewByInterfaceFile(@NotBlank String settlementFileId);

    List<SettlementSummary> selectAllByInterfaceFileAndState(@NotBlank String interfaceFileId, @NotNull SettlementStatus settlementStatus);

    Page<SettlementSummary> selectPage(@NotNull Pageable pageable);

    Page<SettlementSummary> selectPageByInterfaceFile(@NotBlank String interfaceFileFileId, Pageable pageable);

    /**
     * @param settlementSummaryId
     * @param settlementStatus
     */
    void updateSummaryStatus(String settlementSummaryId, SettlementStatus settlementStatus);

    /**
     *
     * @param settlementSummaryId
     * @param result
     */
    void updateSummaryResult(@NotBlank  String settlementSummaryId, @NotNull ResultDescriptor result);

    /**
     * @param interfaceFileId
     */
    void deleteOneBySettlementFile(@NotBlank String interfaceFileId);
}
