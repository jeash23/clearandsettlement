package com.huawei.tsel.settlement.repository;

import com.huawei.tsel.settlement.entity.ResultDescriptor;
import com.huawei.tsel.settlement.entity.SettlementSummary;
import com.huawei.tsel.settlement.entity.SettlementSummary.SettlementStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * @author zakyalvan
 * @since 7 Aug 2017
 */
@Repository
@Validated
@Transactional(readOnly = true)
public class OracleSettlementSummaryRepository extends NamedParameterJdbcDaoSupport implements SettlementSummaryRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(OracleSettlementSummaryRepository.class);

    private InterfaceFileRepository settlementFiles;

    private RowMapper<SettlementSummary> summaryRowMapper = (resultSet, rowNumber) -> SettlementSummary.builder()
            .id(StringUtils.hasText(resultSet.getString("recordId")) ? resultSet.getString("recordId") : UUID.randomUUID().toString())
            .interfaceFile(settlementFiles.selectOne(resultSet.getString("interfaceFileId")))
            .merchantId(resultSet.getString("merchantId"))
            .merchantShortCode(resultSet.getString("merchantShortCode"))
            .merchantName(resultSet.getString("merchantName"))
            .transactionAmount(resultSet.getBigDecimal("transactionAmount"))
            .transactionCount(resultSet.getLong("transactionCount"))
            //.summarizedTimestamp(resultSet.getDate("summarizedTimestamp") != null ? LocalDateTime.from(resultSet.getDate("summarizedTimestamp").toInstant()) : null)
            .status(StringUtils.hasText(resultSet.getString("settlementStatus")) ? SettlementStatus.valueOf(resultSet.getString("settlementStatus")) : null)
            //.settledTimestamp(resultSet.getDate("settledTimestamp") != null ? LocalDateTime.from(resultSet.getDate("settledTimestamp").toInstant()) : null)
            .result(ResultDescriptor.builder()
                    .type(resultSet.getInt("settlementResultType"))
                    .code(resultSet.getString("settlementResultCode"))
                    .description(resultSet.getString("settlementResultDesc"))
                    .transactionId(resultSet.getString("settlementResultTransactionId"))
                    .build())
            .build();

    public OracleSettlementSummaryRepository(InterfaceFileRepository settlementFiles, DataSource dataSource) {
        this.settlementFiles = Objects.requireNonNull(settlementFiles);
        setDataSource(Objects.requireNonNull(dataSource));
    }

    @Override
    public List<SettlementSummary> calculateSummaries(String interfaceFileId) {
        LOGGER.debug("Calculate settlement summaries for settlement interface file with id {}", interfaceFileId);
        Assert.isTrue(settlementFiles.exists(interfaceFileId), "Given settlement interface file id is invalid, not exists");
        String sqlString = "SELECT :recordId recordId, T.SETTLEMENT_FILE_ID interfaceFileId, T.MERCHANT_ID merchantId, M.SHORT_CODE merchantShortCode, M.MERCHANT_NAME merchantName, COUNT(T.RECORD_ID) transactionCount, SUM(T.TRANSACTION_AMOUNT) transactionAmount, :summarizedTimestamp summarizedTimestamp, :settlementStatus settlementStatus FROM SETTLE_OFFLINE_TRX T LEFT JOIN SETTLE_MERCHANT_SHORT_CODE M ON T.MERCHANT_ID=M.MERCHANT_ID GROUP BY T.SETTLEMENT_FILE_ID, T.MERCHANT_ID, M.SHORT_CODE, M.MERCHANT_NAME HAVING T.SETTLEMENT_FILE_ID=:interfaceFileId";

        RowMapper<SettlementSummary> calculatedSummaryMapper = (resultSet, rowNumber) -> SettlementSummary.builder()
                .id(StringUtils.hasText(resultSet.getString("recordId")) ? resultSet.getString("recordId") : UUID.randomUUID().toString())
                .interfaceFile(settlementFiles.selectOne(resultSet.getString("interfaceFileId")))
                .merchantId(resultSet.getString("merchantId"))
                .merchantShortCode(resultSet.getString("merchantShortCode"))
                .merchantName(resultSet.getString("merchantName"))
                .transactionAmount(resultSet.getBigDecimal("transactionAmount"))
                .transactionCount(resultSet.getLong("transactionCount"))
                //.summarizedTimestamp(resultSet.getDate("summarizedTimestamp") != null ? LocalDateTime.from(resultSet.getDate("summarizedTimestamp").toInstant()) : null)
                .status(StringUtils.hasText(resultSet.getString("settlementStatus")) ? SettlementStatus.valueOf(resultSet.getString("settlementStatus")) : null)
                //.settledTimestamp(resultSet.getDate("settledTimestamp") != null ? LocalDateTime.from(resultSet.getDate("settledTimestamp").toInstant()) : null)
                .build();

        MapSqlParameterSource parameters = new MapSqlParameterSource()
                // Hacked!
                .addValue("recordId", "", Types.VARCHAR)
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR)
                .addValue("settlementStatus", SettlementStatus.RAW.toString(), Types.VARCHAR)
                .addValue("summarizedTimestamp", Calendar.getInstance().getTime(), Types.TIMESTAMP);
        return getNamedParameterJdbcTemplate().query(sqlString, parameters, calculatedSummaryMapper);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void insertOne(SettlementSummary settlementSummary) {
        LOGGER.debug("Insert new settlement summary {}", settlementSummary);

        Assert.isTrue(settlementSummary.getStatus().equals(SettlementStatus.RAW), "Only unpersisted settlement summary can be inserted into database");

        String sqlString = "INSERT INTO SETTLE_SETTLEMENT_ITEM_SUMMARY "
                + "(RECORD_ID, SETTLEMENT_FILE_ID, MERCHANT_ID, MERCHANT_SHORT_CODE, MERCHANT_NAME, TRANSACTION_COUNT, TRANSACTION_AMOUNT, SUMMARIZED_TIMESTAMP, SETTLEMENT_STATUS) VALUES "
                + "(:recordId, :interfaceFileId, :merchantId, :merchantShortCode, :merchantName, :transactionCount, :transactionAmount, :summarizedTimestamp, :settlementStatus)";

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("recordId", settlementSummary.getId(), Types.VARCHAR)
                .addValue("interfaceFileId", settlementSummary.getInterfaceFile().getRecordId(), Types.VARCHAR)
                .addValue("merchantId", settlementSummary.getMerchantId(), Types.VARCHAR)
                .addValue("merchantShortCode", settlementSummary.getMerchantShortCode(), Types.VARCHAR)
                .addValue("merchantName", settlementSummary.getMerchantName(), Types.VARCHAR)
                .addValue("transactionCount", settlementSummary.getTransactionCount(), Types.BIGINT)
                .addValue("transactionAmount", settlementSummary.getTransactionAmount(), Types.DECIMAL)
                /**
                 * FIXME
                 *
                 * This is using hard coded value.
                 */
                .addValue("summarizedTimestamp", new Date(), Types.TIMESTAMP)
                .addValue("settlementStatus", SettlementStatus.NEW.toString(), Types.VARCHAR);

        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void insertAll(List<SettlementSummary> settlementSummaries) {
        LOGGER.debug("Insert new settlement summaries {}");

        Integer rawSize = settlementSummaries.stream().map(summary -> summary.getStatus() == SettlementStatus.RAW ? summary.getStatus() : null).filter(status -> status != null).collect(Collectors.toList()).size();

        String sqlString = "INSERT INTO SETTLE_SETTLEMENT_ITEM_SUMMARY "
                + "(RECORD_ID, SETTLEMENT_FILE_ID, MERCHANT_ID, MERCHANT_SHORT_CODE, MERCHANT_NAME, TRANSACTION_COUNT, TRANSACTION_AMOUNT, SUMMARIZED_TIMESTAMP, SETTLEMENT_STATUS) VALUES "
                + "(:recordId, :interfaceFileId, :merchantId, :merchantShortCode, :merchantName, :transactionCount, :transactionAmount, :summarizedTimestamp, :settlementStatus)";

        List<SqlParameterSource> parameterSources = new ArrayList<>();
        for(SettlementSummary settlementSummary : settlementSummaries) {
            SqlParameterSource parameterSource = new MapSqlParameterSource()
                    .addValue("recordId", settlementSummary.getId(), Types.VARCHAR)
                    .addValue("interfaceFileId", settlementSummary.getInterfaceFile().getRecordId(), Types.VARCHAR)
                    .addValue("merchantId", settlementSummary.getMerchantId(), Types.VARCHAR)
                    .addValue("merchantShortCode", settlementSummary.getMerchantShortCode(), Types.VARCHAR)
                    .addValue("merchantName", settlementSummary.getMerchantName(), Types.VARCHAR)
                    .addValue("transactionCount", settlementSummary.getTransactionCount(), Types.BIGINT)
                    .addValue("transactionAmount", settlementSummary.getTransactionAmount(), Types.DECIMAL)
                    /**
                     * FIXME
                     *
                     * This is using hard coded value.
                     */
                    .addValue("summarizedTimestamp", new Date(), Types.TIMESTAMP)
                    .addValue("settlementStatus", SettlementStatus.NEW.toString(), Types.VARCHAR);
            parameterSources.add(parameterSource);
        }

        getNamedParameterJdbcTemplate().batchUpdate(sqlString, parameterSources.toArray(new SqlParameterSource[parameterSources.size()]));
    }

    @Override
    public boolean exists(String settlementSummaryId) {
        String sqlString = "SELECT CASE WHEN EXISTS(SELECT RECORD_ID FROM SETTLE_SETTLEMENT_ITEM_SUMMARY WHERE RECORD_ID=:settlementSummaryId) THEN 1 ELSE 0 END FROM DUAL";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("settlementSummaryId", settlementSummaryId, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Integer.class) == 1;
    }

    @Override
    public boolean existsByInterfaceFile(String interfaceFileId) {
        String sqlString = "SELECT CASE WHEN EXISTS(SELECT RECORD_ID FROM SETTLE_SETTLEMENT_ITEM_SUMMARY WHERE SETTLEMENT_FILE_ID=:interfaceFileId) THEN 1 ELSE 0 END FROM DUAL";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Integer.class) == 1;
    }

    @Override
    public long countAll() {
        String sqlString = "SELECT COUNT(RECORD_ID) FROM SETTLE_SETTLEMENT_ITEM_SUMMARY WHERE SETTLEMENT_FILE_ID=:interfaceFileId";
        SqlParameterSource parameters = new EmptySqlParameterSource();
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Long.class);
    }

    @Override
    public long countAllByInterfaceFile(String interfaceFileId) {
        String sqlString = "SELECT COUNT(RECORD_ID) FROM SETTLE_SETTLEMENT_ITEM_SUMMARY WHERE SETTLEMENT_FILE_ID=:interfaceFileId";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Long.class);
    }

    @Override
    public SettlementSummary selectOne(String settlementSummaryId) {
        String sqlString = "SELECT RECORD_ID recordId, SETTLEMENT_FILE_ID interfaceFileId, MERCHANT_ID merchantId, MERCHANT_SHORT_CODE merchantShortCode, MERCHANT_NAME merchantName, TRANSACTION_COUNT transactionCount, TRANSACTION_AMOUNT transactionAmount, SUMMARIZED_TIMESTAMP summarizedTimestamp, SETTLEMENT_STATUS settlementStatus, SETTLEMENT_TIMESTAMP settlementTimestamp, SETTLEMENT_RESULT_TYPE settlementResultType, SETTLEMENT_RESULT_CODE settlementResultCode, SETTLEMENT_RESULT_DESC settlementResultDesc, SETTLEMENT_RESULT_TRX_ID settlementResultTransactionId FROM SETTLE_SETTLEMENT_ITEM_SUMMARY WHERE RECORD_ID=:settlementSummaryId";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("settlementSummaryId", settlementSummaryId, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, summaryRowMapper);
    }

    @Override
    public SettlementSummary selectOneByInterfaceFile(String interfaceFileId) {
        String sqlString = "SELECT RECORD_ID recordId, SETTLEMENT_FILE_ID interfaceFileId, MERCHANT_ID merchantId, MERCHANT_SHORT_CODE merchantShortCode, MERCHANT_NAME merchantName, TRANSACTION_COUNT transactionCount, TRANSACTION_AMOUNT transactionAmount, SUMMARIZED_TIMESTAMP summarizedTimestamp, SETTLEMENT_STATUS settlementStatus, SETTLEMENT_TIMESTAMP settlementTimestamp, SETTLEMENT_RESULT_TYPE settlementResultType, SETTLEMENT_RESULT_CODE settlementResultCode, SETTLEMENT_RESULT_DESC settlementResultDesc, SETTLEMENT_RESULT_TRX_ID settlementResultTransactionId FROM SETTLE_SETTLEMENT_ITEM_SUMMARY WHERE SETTLEMENT_FILE_ID=:interfaceFileId";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, summaryRowMapper);
    }

    /**
     * TODO
     *
     * Refactor this, so parameter received also settlement status.
     *
     * @param interfaceFileId
     * @return
     */
    @Override
    @Deprecated
    public List<SettlementSummary> selectAllNewByInterfaceFile(String interfaceFileId) {
        if(!existsByInterfaceFile(interfaceFileId)) {
            return Collections.emptyList();
        }

        String sqlString = "SELECT RECORD_ID recordId, SETTLEMENT_FILE_ID interfaceFileId, MERCHANT_ID merchantId, MERCHANT_SHORT_CODE merchantShortCode, MERCHANT_NAME merchantName, TRANSACTION_COUNT transactionCount, TRANSACTION_AMOUNT transactionAmount, SUMMARIZED_TIMESTAMP summarizedTimestamp, SETTLEMENT_STATUS settlementStatus, SETTLEMENT_TIMESTAMP settlementTimestamp , SETTLEMENT_RESULT_TYPE settlementResultType, SETTLEMENT_RESULT_CODE settlementResultCode, SETTLEMENT_RESULT_DESC settlementResultDesc, SETTLEMENT_RESULT_TRX_ID settlementResultTransactionId FROM SETTLE_SETTLEMENT_ITEM_SUMMARY WHERE SETTLEMENT_FILE_ID=:interfaceFileId AND SETTLEMENT_STATUS=:settlementStatus";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR)
                .addValue("settlementStatus", SettlementStatus.NEW.toString(), Types.VARCHAR);
        return getNamedParameterJdbcTemplate().query(sqlString, parameters, summaryRowMapper);
    }

    @Override
    public List<SettlementSummary> selectAllByInterfaceFileAndState(String interfaceFileId, SettlementStatus settlementStatus) {
        if(!existsByInterfaceFile(interfaceFileId)) {
            return Collections.emptyList();
        }

        String sqlString = "SELECT RECORD_ID recordId, SETTLEMENT_FILE_ID interfaceFileId, MERCHANT_ID merchantId, MERCHANT_SHORT_CODE merchantShortCode, MERCHANT_NAME merchantName, TRANSACTION_COUNT transactionCount, TRANSACTION_AMOUNT transactionAmount, SUMMARIZED_TIMESTAMP summarizedTimestamp, SETTLEMENT_STATUS settlementStatus, SETTLEMENT_TIMESTAMP settlementTimestamp , SETTLEMENT_RESULT_TYPE settlementResultType, SETTLEMENT_RESULT_CODE settlementResultCode, SETTLEMENT_RESULT_DESC settlementResultDesc, SETTLEMENT_RESULT_TRX_ID settlementResultTransactionId FROM SETTLE_SETTLEMENT_ITEM_SUMMARY WHERE SETTLEMENT_FILE_ID=:interfaceFileId AND SETTLEMENT_STATUS=:settlementStatus";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR)
                .addValue("settlementStatus", settlementStatus.toString(), Types.VARCHAR);
        return getNamedParameterJdbcTemplate().query(sqlString, parameters, summaryRowMapper);
    }

    @Override
    public Page<SettlementSummary> selectPage(Pageable pageable) {
        long recordCount = countAll();

        if(recordCount == 0) {
            return new PageImpl<>(Collections.emptyList(), pageable, recordCount);
        }

        String sqlString = "SELECT SQ.RECORD_ID recordId, SQ.SETTLEMENT_FILE_ID interfaceFileId, SQ.MERCHANT_ID merchantId, SQ.MERCHANT_SHORT_CODE merchantShortCode, SQ.MERCHANT_NAME merchantName, SQ.TRANSACTION_COUNT transactionCount, SQ.TRANSACTION_AMOUNT transactionAmount, SQ.SUMMARIZED_TIMESTAMP summarizedTimestamp, SQ.SETTLEMENT_STATUS settlementStatus, SQ.SETTLEMENT_TIMESTAMP settlementTimestamp , SQ.SETTLEMENT_RESULT_TYPE settlementResultType, SQ.SETTLEMENT_RESULT_CODE settlementResultCode, SQ.SETTLEMENT_RESULT_DESC settlementResultDesc, SQ.SETTLEMENT_RESULT_TRX_ID settlementResultTransactionId " +
                "FROM (SELECT ROW_NUMBER() OVER (ORDER BY TS.SUMMARIZED_TIMESTAMP DESC) ROW_INDEX, TS.RECORD_ID, TS.SETTLEMENT_FILE_ID, TS.MERCHANT_ID, TS.MERCHANT_SHORT_CODE, TS.MERCHANT_NAME, TS.TRANSACTION_COUNT, TS.TRANSACTION_AMOUNT, TS.SUMMARIZED_TIMESTAMP, TS.SETTLEMENT_STATUS, TS.SETTLEMENT_TIMESTAMP, TS.SETTLEMENT_RESULT_TYPE, TS.SETTLEMENT_RESULT_CODE, TS.SETTLEMENT_RESULT_DESC, TS.SETTLEMENT_RESULT_TRX_ID FROM SETTLE_SETTLEMENT_ITEM_SUMMARY TS) SQ " +
                "WHERE SQ.ROW_INDEX BETWEEN :startRowNumber AND :endRowNumber " +
                "ORDER BY SQ.SUMMARIZED_TIMESTAMP DESC";

        Integer startRowNumber = pageable.getOffset() + 1;
        Integer endRowNumber = pageable.getOffset() + pageable.getPageSize();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("startRowNumber", startRowNumber, Types.INTEGER)
                .addValue("endRowNumber", endRowNumber, Types.INTEGER);

        List<SettlementSummary> summaries = getNamedParameterJdbcTemplate().query(sqlString, parameterSource, summaryRowMapper);
        return new PageImpl<>(summaries, pageable, recordCount);
    }

    @Override
    public Page<SettlementSummary> selectPageByInterfaceFile(String interfaceFileId, Pageable pageable) {
        if(!existsByInterfaceFile(interfaceFileId)) {
            return new PageImpl<>(Collections.emptyList(), pageable, 0l);
        }

        long recordCount = countAllByInterfaceFile(interfaceFileId);
        if(recordCount == 0) {
            return new PageImpl<>(Collections.emptyList(), pageable, recordCount);
        }

        String sqlString = "SELECT SQ.RECORD_ID recordId, SQ.SETTLEMENT_FILE_ID interfaceFileId, SQ.MERCHANT_ID merchantId, SQ.MERCHANT_SHORT_CODE merchantShortCode, SQ.MERCHANT_NAME merchantName, SQ.TRANSACTION_COUNT transactionCount, SQ.TRANSACTION_AMOUNT transactionAmount, SQ.SUMMARIZED_TIMESTAMP summarizedTimestamp, SQ.SETTLEMENT_STATUS settlementStatus, SQ.SETTLEMENT_TIMESTAMP settlementTimestamp , SQ.SETTLEMENT_RESULT_TYPE settlementResultType, SQ.SETTLEMENT_RESULT_CODE settlementResultCode, SQ.SETTLEMENT_RESULT_DESC settlementResultDesc, SQ.SETTLEMENT_RESULT_TRX_ID settlementResultTransactionId " +
                "FROM (SELECT ROW_NUMBER() OVER (ORDER BY TS.SUMMARIZED_TIMESTAMP DESC) ROW_INDEX, TS.RECORD_ID, TS.SETTLEMENT_FILE_ID, TS.MERCHANT_ID, TS.MERCHANT_SHORT_CODE, TS.MERCHANT_NAME, TS.TRANSACTION_COUNT, TS.TRANSACTION_AMOUNT, TS.SUMMARIZED_TIMESTAMP, TS.SETTLEMENT_STATUS, TS.SETTLEMENT_TIMESTAMP, TS.SETTLEMENT_RESULT_TYPE, TS.SETTLEMENT_RESULT_CODE, TS.SETTLEMENT_RESULT_DESC, TS.SETTLEMENT_RESULT_TRX_ID FROM SETTLE_SETTLEMENT_ITEM_SUMMARY TS WHERE TS.SETTLEMENT_FILE_ID=:interfaceFileId) SQ " +
                "WHERE SQ.ROW_INDEX BETWEEN :startRowNumber AND :endRowNumber " +
                "ORDER BY SQ.SUMMARIZED_TIMESTAMP DESC";

        Integer startRowNumber = pageable.getOffset() + 1;
        Integer endRowNumber = pageable.getOffset() + pageable.getPageSize();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR)
                .addValue("startRowNumber", startRowNumber, Types.INTEGER)
                .addValue("endRowNumber", endRowNumber, Types.INTEGER);

        List<SettlementSummary> summaries = getNamedParameterJdbcTemplate().query(sqlString, parameterSource, summaryRowMapper);

        return new PageImpl<>(summaries, pageable, recordCount);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateSummaryStatus(String settlementSummaryId, SettlementStatus settlementStatus) {
        String sqlString = "UPDATE SETTLE_SETTLEMENT_ITEM_SUMMARY SET SETTLEMENT_STATUS=:settlementStatus WHERE RECORD_ID=:settlementSummaryId";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("settlementSummaryId", settlementSummaryId, Types.VARCHAR)
                .addValue("settlementStatus", settlementStatus.toString(), Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }

    @Override
    public void updateSummaryResult(String settlementSummaryId, ResultDescriptor result) {
        String sqlString = "UPDATE SETTLE_SETTLEMENT_ITEM_SUMMARY SET SETTLEMENT_RESULT_TYPE=:resultType, SETTLEMENT_RESULT_CODE=:resultCode, SETTLEMENT_RESULT_DESC=:resultDescription, SETTLEMENT_RESULT_TRX_ID=:resultTransactionId WHERE RECORD_ID=:settlementSummaryId";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("settlementSummaryId", settlementSummaryId, Types.VARCHAR)
                .addValue("resultType", result.getType(), Types.INTEGER)
                .addValue("resultCode", result.getCode(), Types.VARCHAR)
                .addValue("resultDescription", result.getDescription(), Types.VARCHAR)
                .addValue("resultTransactionId", result.getTransactionId(), Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteOneBySettlementFile(String interfaceFileId) {
        String sqlString = "DELETE FROM SETTLE_OFFLINE_TRX WHERE SETTLEMENT_FILE_ID=:interfaceFileId";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }
}
