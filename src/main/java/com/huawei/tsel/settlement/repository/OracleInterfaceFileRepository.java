package com.huawei.tsel.settlement.repository;

import com.huawei.tsel.settlement.entity.InterfaceFile;
import com.huawei.tsel.settlement.entity.InterfaceFile.ProcessingState;
import com.huawei.tsel.settlement.entity.RejectionReason;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Types;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

/**
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@Repository
@Validated
@Transactional(readOnly = true)
public class OracleInterfaceFileRepository extends NamedParameterJdbcDaoSupport implements InterfaceFileRepository {
    /**
     * <p>Default {@link RowMapper} for {@link InterfaceFile}</p>
     */
    private RowMapper<InterfaceFile> defaultRowMapper = (resultSet, rowNumber) -> InterfaceFile.builder()
            .recordId(resultSet.getString("recordId"))
            .originalName(resultSet.getString("originalFileName"))
            .storageDirectory(resultSet.getString("storageDirectory"))
            .internalName(resultSet.getString("storageFileName"))
            .checksumValue(resultSet.getString("fileChecksum"))
            //.acquiredTimestamp(LocalDateTime.from(resultSet.getDate("acquiredTimestamp").toInstant()))
            .processedTransactionCount(resultSet.getLong("processedTrxCount"))
            .declaredTransactionCount(resultSet.getLong("declaredTrxCount"))
            .processedTransactionAmount(resultSet.getBigDecimal("processedTrxAmount"))
            .declaredTransactionAmount(resultSet.getBigDecimal("declaredTrxAmount"))
            .processingPriority(resultSet.getLong("processingPriority"))
            .status(ProcessingState.valueOf(resultSet.getString("processingState")))
            .build();

    public OracleInterfaceFileRepository(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public InterfaceFile insertOne(InterfaceFile interfaceFile) {
        String sqlString = "INSERT INTO SETTLE_INTERFACE_FILE(RECORD_ID, ORIGINAL_FILENAME, STORAGE_DIRECTORY, STORAGE_FILENAME, FILE_CHECKSUM, ACQUIRED_TIMESTAMP, CALCULATED_TX_AMOUNT, DECLARED_TX_AMOUNT, PROCESSING_PRIORITY, PROCESSING_STATE) VALUES "
                + "(:recordId, :originalFileName, :storageDirectory, :storageFileName, :fileChecksum, :acquiredTimestamp, :processedTxAmount, :declaredTxAmount, :processingPriority, :processingState)";

        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("recordId", interfaceFile.getRecordId(), Types.VARCHAR)
                .addValue("originalFileName", interfaceFile.getOriginalName(), Types.VARCHAR)
                .addValue("storageDirectory", interfaceFile.getStorageDirectory(), Types.VARCHAR)
                .addValue("storageFileName", interfaceFile.getInternalName(), Types.VARCHAR)
                .addValue("fileChecksum", interfaceFile.getChecksumValue(), Types.VARCHAR)
                .addValue("acquiredTimestamp", Date.from(interfaceFile.getAcquiredTimestamp().atZone(ZoneId.systemDefault()).toInstant()), Types.TIMESTAMP)
                .addValue("processedTxAmount", interfaceFile.getProcessedTransactionAmount(), Types.DECIMAL)
                .addValue("declaredTxAmount", interfaceFile.getDeclaredTransactionAmount(), Types.DECIMAL)
                .addValue("processingPriority", interfaceFile.getProcessingPriority(), Types.BIGINT)
                .addValue("processingState", interfaceFile.getStatus().toString(), Types.VARCHAR);

        getNamedParameterJdbcTemplate().update(sqlString, parameterSource);
        return interfaceFile;
    }

    @Override
    public boolean exists(String settlementFileId) {
        if (!StringUtils.hasText(settlementFileId)) {
            return false;
        }

        String sqlString = "SELECT CASE WHEN EXISTS(SELECT F.RECORD_ID FROM SETTLE_INTERFACE_FILE F WHERE F.RECORD_ID=:interfaceFileId) THEN 1 ELSE 0 END FROM DUAL";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("interfaceFileId", settlementFileId.trim(), Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Integer.class) > 0;
    }

    @Override
    public boolean existsByOriginalName(String originalFileName) {
        if (!StringUtils.hasText(originalFileName)) {
            return false;
        }

        String sqlString = "SELECT CASE WHEN EXISTS(SELECT F.RECORD_ID FROM SETTLE_INTERFACE_FILE F WHERE UPPER(F.ORIGINAL_FILENAME)=UPPER(:originalFileName)) THEN 1 ELSE 0 END FROM DUAL";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("originalFileName", originalFileName.trim(), Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Integer.class) > 0;
    }

    @Override
    public Long countAll() {
        String sqlString = "SELECT COUNT(F.RECORD_ID) FROM SETTLE_INTERFACE_FILE F";
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, new EmptySqlParameterSource(), Long.class);
    }

    @Override
    public InterfaceFile selectOne(String interfaceFileId) {
        if (!exists(interfaceFileId)) {
            return null;
        }

        String sqlString = "SELECT F.RECORD_ID recordId, F.ORIGINAL_FILENAME originalFileName, F.STORAGE_DIRECTORY storageDirectory, F.STORAGE_FILENAME storageFileName, F.FILE_CHECKSUM fileChecksum, F.ACQUIRED_TIMESTAMP acquiredTimestamp, F.PROCESSED_TX_COUNT processedTrxCount, F.DECLARED_TX_COUNT declaredTrxCount, F.CALCULATED_TX_AMOUNT processedTrxAmount, F.DECLARED_TX_AMOUNT declaredTrxAmount, F.PROCESSING_PRIORITY processingPriority, F.PROCESSING_STATE processingState FROM SETTLE_INTERFACE_FILE F WHERE F.RECORD_ID=:interfaceFileId";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId.trim(), Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, defaultRowMapper);
    }

    @Override
    public List<InterfaceFile> selectAllByStatusOrderByPriorityAsc(ProcessingState status) {
        Assert.notNull(status, "Processing state parameter must not be null");

        String sqlString = "SELECT F.RECORD_ID recordId, F.ORIGINAL_FILENAME originalFileName, F.STORAGE_DIRECTORY storageDirectory, F.STORAGE_FILENAME storageFileName, F.FILE_CHECKSUM fileChecksum, F.ACQUIRED_TIMESTAMP acquiredTimestamp, F.PROCESSED_TX_COUNT processedTrxCount, F.DECLARED_TX_COUNT declaredTrxCount, F.CALCULATED_TX_AMOUNT processedTrxAmount, F.DECLARED_TX_AMOUNT declaredTrxAmount, F.PROCESSING_PRIORITY processingPriority, F.PROCESSING_STATE processingState FROM SETTLE_INTERFACE_FILE F WHERE F.PROCESSING_STATE=:processingState ORDER BY F.PROCESSING_PRIORITY DESC";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("processingState", status.name(), Types.VARCHAR);
        return getNamedParameterJdbcTemplate().query(sqlString, parameterSource, defaultRowMapper);
    }

    /**
     *
     * @param pageable
     * @return
     */
    @Override
    public Page<InterfaceFile> selectPage(Pageable pageable) {
        String sqlString = "SELECT SQ.RECORD_ID recordId, SQ.ORIGINAL_FILENAME originalFileName, SQ.STORAGE_DIRECTORY storageDirectory, SQ.STORAGE_FILENAME storageFileName, SQ.FILE_CHECKSUM fileChecksum, SQ.ACQUIRED_TIMESTAMP acquiredTimestamp, SQ.PROCESSED_TX_COUNT processedTrxCount, SQ.DECLARED_TX_COUNT declaredTrxCount, SQ.CALCULATED_TX_AMOUNT processedTrxAmount, SQ.DECLARED_TX_AMOUNT declaredTrxAmount, SQ.PROCESSING_PRIORITY processingPriority, SQ.PROCESSING_STATE processingState " +
                "FROM (SELECT ROW_NUMBER() OVER (ORDER BY F.PROCESSING_PRIORITY ASC) ROW_INDEX, F.RECORD_ID, F.ORIGINAL_FILENAME, F.STORAGE_DIRECTORY, F.STORAGE_FILENAME, F.FILE_CHECKSUM, F.ACQUIRED_TIMESTAMP, F.PROCESSED_TX_COUNT, F.DECLARED_TX_COUNT, F.CALCULATED_TX_AMOUNT, F.DECLARED_TX_AMOUNT, F.PROCESSING_PRIORITY, F.PROCESSING_STATE FROM SETTLE_INTERFACE_FILE F) SQ " +
                "WHERE SQ.ROW_INDEX BETWEEN :startRowNumber AND :endRowNumber AND 1=1 " +
                "ORDER BY SQ.PROCESSING_PRIORITY ASC";

        Integer startRowNumber = pageable.getOffset() + 1;
        Integer endRowNumber = pageable.getOffset() + pageable.getPageSize();

        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("startRowNumber", startRowNumber, Types.INTEGER)
                .addValue("endRowNumber", endRowNumber, Types.INTEGER);

        List<InterfaceFile> content = getNamedParameterJdbcTemplate().query(sqlString, parameterSource, defaultRowMapper);
        return new PageImpl<>(content, pageable, countAll());
    }

    @Override
    public ProcessingState selectOneProcessingState(String interfaceFileId) {
        String sqlString = "SELECT file.PROCESSING_STATE state FROM SETTLE_INTERFACE_FILE WHERE file.RECORD_ID=:recordId";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("recordId", interfaceFileId.trim(), Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, ProcessingState.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateProcessingState(String interfaceFileId, ProcessingState processingState) {
        String sqlString = "UPDATE SETTLE_INTERFACE_FILE SET PROCESSING_STATE=:processingState WHERE RECORD_ID=:interfaceFileId";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("processingState", processingState.name(), Types.VARCHAR)
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateRejectionReason(String interfaceFileId, RejectionReason rejectionReason) {
        String sqlString = "UPDATE SETTLE_INTERFACE_FILE SET REJECTION_REASON=:rejectionReason WHERE RECORD_ID=:interfaceFileId";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("rejectionReason", rejectionReason.name(), Types.VARCHAR)
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateProcessedTransactionCount(String interfaceFileId, Integer transactionCount) {
        String sqlString = "UPDATE SETTLE_INTERFACE_FILE SET PROCESSED_TX_COUNT=:processedTransactionCount WHERE RECORD_ID=:interfaceFileId";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("processedTransactionCount", transactionCount, Types.BIGINT)
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateDeclaredTransactionCount(String interfaceFileId, Integer transactionCount) {
        String sqlString = "UPDATE SETTLE_INTERFACE_FILE SET DECLARED_TX_COUNT=:declaredTransactionCount WHERE RECORD_ID=:interfaceFileId";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("declaredTransactionCount", transactionCount, Types.BIGINT)
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateDeclaredAmount(String interfaceFileId, BigDecimal declaredAmount) {
        String sqlString = "UPDATE SETTLE_INTERFACE_FILE SET DECLARED_TX_AMOUNT=:declaredTxAmount WHERE RECORD_ID=:interfaceFileId";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("declaredTxAmount", declaredAmount, Types.DECIMAL)
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCalculatedTotal(String interfaceFileId, BigDecimal calculatedTotal) {
        String sqlString = "UPDATE SETTLE_INTERFACE_FILE SET CALCULATED_TX_AMOUNT=:calculatedTxAmount";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("calculatedTxAmount", calculatedTotal, Types.DECIMAL)
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }
}
