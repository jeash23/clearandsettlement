package com.huawei.tsel.settlement.repository;

import javax.sql.DataSource;

import com.huawei.tsel.settlement.entity.TransactionItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.sql.Types;
import java.util.Collections;
import java.util.List;

/**
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@Repository
@Validated
@Transactional(readOnly = true)
public class OracleTransactionItemRepository extends NamedParameterJdbcDaoSupport implements TransactionItemRepository {
    private RowMapper<TransactionItem> defaultRowMapper = (resultSet, rowNumber) -> TransactionItem.builder()
            .recordId(resultSet.getString("recordId"))
            .customerUid(resultSet.getString("customerUid"))
            .customerMsisdn(resultSet.getString("customerMsisdn"))
            .merchantId(resultSet.getString("merchantId"))
            .merchantShortCode(resultSet.getString("merchantShortCode"))
            .terminalId(resultSet.getString("terminalId"))
            .transactionId(resultSet.getString("transactionId"))
            .transactionAmount(resultSet.getBigDecimal("transactionAmount"))
            .transactionDate(resultSet.getDate("transactionDate"))
            .lastBalance(resultSet.getBigDecimal("lastBalance"))
            .purseCounter(resultSet.getLong("purseCounter"))
            .loadDate(resultSet.getDate("loadedDate"))
            .interfaceFileId(resultSet.getString("interfaceFileId"))
            .settled(resultSet.getInt("isSettled") > 0 ? true : false)
            .settleDate(resultSet.getDate("settlementDate"))
            .build();

    public OracleTransactionItemRepository(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @Override
    public boolean existsByCustomerMsisdn(String customerMsisdn) {
        String sqlString = "SELECT CASE WHEN EXISTS(SELECT T.RECORD_ID FROM SETTLE_OFFLINE_TRX T WHERE T.CUSTOMER_MSISDN=:customerMsisdn) THEN 1 ELSE 0 END FROM DUAL";
        MapSqlParameterSource parameters = new MapSqlParameterSource().addValue("customerMsisdn", customerMsisdn, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Long.class) == 1;
    }
    @Override
    public boolean existsByInterfaceFile(String interfaceFileId) {
        String sqlString = "SELECT CASE WHEN EXISTS(SELECT T.RECORD_ID FROM SETTLE_OFFLINE_TRX T WHERE T.SETTLEMENT_FILE_ID=:interfaceFileId) THEN 1 ELSE 0 END FROM DUAL";
        MapSqlParameterSource parameters = new MapSqlParameterSource().addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Long.class) == 1;
    }

    @Override
    public boolean existsByCustomerUid(String customerUid) {
        String sqlString = "SELECT CASE WHEN EXISTS(SELECT T.RECORD_ID FROM SETTLE_OFFLINE_TRX T WHERE T.CUSTOMER_UID=:customerUid) THEN 1 ELSE 0 END FROM DUAL";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("customerUid", customerUid, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Long.class) == 1;
    }

    @Override
    public boolean existsByCustomerUidAndPurseCounter(String customerUid, Long purseCounter) {
        String sqlString = "SELECT CASE WHEN EXISTS(SELECT T.RECORD_ID FROM SETTLE_OFFLINE_TRX T WHERE T.CUSTOMER_UID=:customerUid AND T.PURSE_COUNTER=:purseCounter) THEN 1 ELSE 0 END FROM DUAL";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("customerUid", customerUid, Types.VARCHAR)
                .addValue("purseCounter", purseCounter, Types.BIGINT);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Long.class) == 1;
    }

    @Override
    public long countAllByCustomerMsisdn(String customerMsisdn) {
        String sqlString = "SELECT COUNT(T.RECORD_ID) FROM SETTLE_OFFLINE_TRX T WHERE T.CUSTOMER_MSISDN=:customerMsisdn";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("customerMsisdn", customerMsisdn, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Long.class);
    }
    @Override
    public long countAllByInterfaceFile(String interfaceFileId) {
        String sqlString = "SELECT COUNT(T.RECORD_ID) FROM SETTLE_OFFLINE_TRX T WHERE T.SETTLEMENT_FILE_ID=:interfaceFileId";
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameters, Long.class);
    }

    @Override
    public TransactionItem selectOneByCustomerUidAndPurseCounter(String customerUid, Long purseCounter) {
        String sqlString = "SELECT T.RECORD_ID recordId, T.CUSTOMER_UID customerUid, T.CUSTOMER_MSISDN customerMsisdn, T.MERCHANT_ID merchantId, T.MERCHANT_SHORTCODE merchantShortCode, T.TERMINAL_ID terminalId, T.TRANSACTION_ID transactionId, T.TRANSACTION_AMOUNT transactionAmount, T.TRANSACTION_DATE transactionDate, T.LAST_BALANCE lastBalance, T.PURSE_COUNTER purseCounter, T.LOADED_DATE loadedDate, T.SETTLEMENT_FILE_ID interfaceFileId, T.IS_SETTLED isSettled, T.SETTLEMENT_DATE settlementDate "
                + "FROM SETTLE_OFFLINE_TRX T "
                + "WHERE T.CUSTOMER_UID=:customerUid AND T.PURSE_COUNTER=:purseCounter";

        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("customerUid", customerUid, Types.VARCHAR)
                .addValue("purseCounter", purseCounter, Types.BIGINT);

        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameterSource, defaultRowMapper);
    }

    @Override
    public Page<TransactionItem> selectAllByCustomerMsisdn(String customerMsisdn, Pageable pageable) {
        if(!existsByCustomerMsisdn(customerMsisdn)) {
            return new PageImpl<>(Collections.emptyList(), pageable, 0l);
        }

        String sqlString = "SELECT SUBQ.ROW_INDEX, SUBQ.RECORD_ID recordId, SUBQ.CUSTOMER_UID customerUid, SUBQ.CUSTOMER_MSISDN customerMsisdn, SUBQ.MERCHANT_ID merchantId, SUBQ.MERCHANT_SHORTCODE merchantShortCode, SUBQ.TERMINAL_ID terminalId, SUBQ.TRANSACTION_ID transactionId, SUBQ.TRANSACTION_AMOUNT transactionAmount, SUBQ.TRANSACTION_DATE transactionDate, SUBQ.LAST_BALANCE lastBalance, SUBQ.PURSE_COUNTER purseCounter, SUBQ.LOADED_DATE loadedDate, SUBQ.SETTLEMENT_FILE_ID interfaceFileId, SUBQ.IS_SETTLED isSettled, SUBQ.SETTLEMENT_DATE settlementDate "
                + "FROM (SELECT T.RECORD_ID, T.CUSTOMER_UID, T.CUSTOMER_MSISDN, T.MERCHANT_ID, T.MERCHANT_SHORTCODE, T.TERMINAL_ID, T.TRANSACTION_ID, T.TRANSACTION_AMOUNT, T.TRANSACTION_DATE, T.LAST_BALANCE, T.PURSE_COUNTER, T.LOADED_DATE, T.SETTLEMENT_FILE_ID, T.IS_SETTLED, T.SETTLEMENT_DATE, ROW_NUMBER() OVER (ORDER BY TRANSACTION_DATE DESC) ROW_INDEX FROM SETTLE_OFFLINE_TRX T WHERE T.CUSTOMER_MSISDN=:customerMsisdn) SUBQ "
                + "WHERE SUBQ.ROW_INDEX BETWEEN :startRowNumber AND :endRowNumber AND 1=1";

        Integer startRowNumber = pageable.getOffset() + 1;
        Integer endRowNumber = pageable.getOffset() + pageable.getPageSize();

        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("customerMsisdn", customerMsisdn, Types.VARCHAR)
                .addValue("startRowNumber", startRowNumber, Types.INTEGER)
                .addValue("endRowNumber", endRowNumber, Types.INTEGER);

        List<TransactionItem> content = getNamedParameterJdbcTemplate().query(sqlString, parameters, defaultRowMapper);
        return new PageImpl<>(content, pageable, countAllByCustomerMsisdn(customerMsisdn));
    }

    @Override
    public Page<TransactionItem> selectAllByInterfaceFile(String interfaceFileId, Pageable pageable) {
        if(!existsByInterfaceFile(interfaceFileId)) {
            return new PageImpl<>(Collections.emptyList(), pageable, 0l);
        }

        String sqlString = "SELECT SUBQ.ROW_INDEX, SUBQ.RECORD_ID recordId, SUBQ.CUSTOMER_UID customerUid, SUBQ.CUSTOMER_MSISDN customerMsisdn, SUBQ.MERCHANT_ID merchantId, SUBQ.MERCHANT_SHORTCODE merchantShortCode, SUBQ.TERMINAL_ID terminalId, SUBQ.TRANSACTION_ID transactionId, SUBQ.TRANSACTION_AMOUNT transactionAmount, SUBQ.TRANSACTION_DATE transactionDate, SUBQ.LAST_BALANCE lastBalance, SUBQ.PURSE_COUNTER purseCounter, SUBQ.LOADED_DATE loadedDate, SUBQ.SETTLEMENT_FILE_ID interfaceFileId, SUBQ.IS_SETTLED isSettled, SUBQ.SETTLEMENT_DATE settlementDate "
                + "FROM (SELECT T.RECORD_ID, T.CUSTOMER_UID, T.CUSTOMER_MSISDN, T.MERCHANT_ID, T.MERCHANT_SHORTCODE, T.TERMINAL_ID, T.TRANSACTION_ID, T.TRANSACTION_AMOUNT, T.TRANSACTION_DATE, T.LAST_BALANCE, T.PURSE_COUNTER, T.LOADED_DATE, T.SETTLEMENT_FILE_ID, T.IS_SETTLED, T.SETTLEMENT_DATE, ROW_NUMBER() OVER (ORDER BY TRANSACTION_DATE DESC) ROW_INDEX FROM SETTLE_OFFLINE_TRX T WHERE T.SETTLEMENT_FILE_ID=:interfaceFileId) SUBQ "
                + "WHERE SUBQ.ROW_INDEX BETWEEN :startRowNumber AND :endRowNumber AND 1=1";

        Integer startRowNumber = pageable.getOffset() + 1;
        Integer endRowNumber = pageable.getOffset() + pageable.getPageSize();

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR)
                .addValue("startRowNumber", startRowNumber, Types.INTEGER)
                .addValue("endRowNumber", endRowNumber, Types.INTEGER);

        List<TransactionItem> content = getNamedParameterJdbcTemplate().query(sqlString, parameters, defaultRowMapper);
        return new PageImpl<>(content, pageable, countAllByInterfaceFile(interfaceFileId));
    }
}
