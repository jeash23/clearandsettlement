package com.huawei.tsel.settlement.repository;

import com.huawei.tsel.settlement.entity.InvalidLine;
import com.huawei.tsel.settlement.entity.LineType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author zakyalvan
 * @since 18 Sep 2017
 */
@Repository
@Transactional(readOnly = true)
public class OracleInvalidLineRepository extends NamedParameterJdbcDaoSupport implements InvalidLineRepository {
    private RowMapper<InvalidLine> defaultRowMapper = (resultSet, rowNumber) -> {
        InvalidLine invalidLine = InvalidLine.builder()
                .content(resultSet.getString("lineContent"))
                .number(resultSet.getInt("lineNumber"))
                .type(LineType.valueOf(resultSet.getString("lineType")))
                .message(resultSet.getString("errorMessage"))
                .build();
        return invalidLine;
    };

    @Autowired
    public OracleInvalidLineRepository(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @Override
    public void insertAll(String interfaceFileId, List<InvalidLine> invalidLines) {
        String sqlString = "INSERT INTO SETTLE_INVALID_LINE(RECORD_ID, INTERFACE_FILE_ID, LINE_NUMBER, LINE_CONTENT, LINE_TYPE, ERROR_MESSAGE) VALUES (:recordId, :interfaceFileId, :lineNumber, :lineContent, :lineType, :errorMessage)";
        List<SqlParameterSource> parameterSources = new ArrayList<>();
        for(InvalidLine invalidLine : invalidLines) {
            MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                    .addValue("recordId", UUID.randomUUID().toString(), Types.VARCHAR)
                    .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR)
                    .addValue("lineNumber", invalidLine.getNumber(), Types.INTEGER)
                    .addValue("lineContent", invalidLine.getContent(), Types.VARCHAR)
                    .addValue("lineType", invalidLine.getType().name(), Types.VARCHAR)
                    .addValue("errorMessage", invalidLine.getMessage(), Types.VARCHAR);
            parameterSources.add(parameterSource);
        }
        getNamedParameterJdbcTemplate().batchUpdate(sqlString, parameterSources.toArray(new SqlParameterSource[parameterSources.size()]));
    }

    @Override
    public boolean exists(String interfaceFileId) {
        String sqlString = "SELECT CASE WHEN EXISTS(SELECT RECORD_ID FROM SETTLE_INVALID_LINE WHERE INTERFACE_FILE_ID=:interfaceFileId) THEN 1 ELSE 0 END FROM DUAL";
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameterSource, Integer.class) == 1;
    }

    @Override
    public List<InvalidLine> selectAll(String interfaceFileId) {
        String sqlString = "SELECT RECORD_ID recordId, INTERFACE_FILE_ID interfaceFileId, LINE_NUMBER lineNumber, LINE_CONTENT lineContent, LINE_TYPE lineType, ERROR_MESSAGE errorMessage FROM SETTLE_INVALID_LINE WHERE INTERFACE_FILE_ID=:interfaceFileId ORDER BY LINE_NUMBER ASC";
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);

        List<InvalidLine> invalidLines = getNamedParameterJdbcTemplate().query(sqlString, parameterSource, defaultRowMapper);
        if(invalidLines == null) {
            invalidLines = Collections.emptyList();
        }
        return  invalidLines;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteAll(String interfaceFileId) {
        if(exists(interfaceFileId)) {
            return;
        }

        String sqlString = "DELETE FROM SETTLE_INVALID_LINE WHERE INTERFACE_FILE_ID=:interfaceFileId";
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("interfaceFileId", interfaceFileId, Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameterSource);
    }
}
