package com.huawei.tsel.settlement.repository;

import java.sql.Types;
import java.util.Date;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import com.huawei.tsel.settlement.entity.TransactionCounter;

/**
 * @author zakyalvan
 * @since 8 Aug 2017
 */
@Validated
@Repository
@Transactional(readOnly = true)
public class OracleTransactionCounterRepository extends NamedParameterJdbcDaoSupport implements TransactionCounterRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(OracleTransactionCounterRepository.class);

    private RowMapper<TransactionCounter> defaultRowMapper = (resultSet, rowNumber) -> TransactionCounter.builder()
            .customerUid(resultSet.getString("customerUid"))
            .lastPurseCounter(resultSet.getLong("lastPurseCounter"))
            .lastBalance(resultSet.getBigDecimal("lastBalance"))
            //.lastUpdate()
            .build();

    @Autowired
    public OracleTransactionCounterRepository(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void insert(TransactionCounter transactionCounter) {
        Assert.isTrue(!exists(transactionCounter.getCustomerUid()), String.format("Transaction counter for given customer uid (%s) already registered", transactionCounter.getCustomerUid()));

        String sqlString = "INSERT INTO SETTLE_PURSE_COUNTER (CUSTUMER_UID, CURRENT_COUNTER, LAST_BALANCE, LAST_UPDATED) VALUES (:customerUid, :lastPurseCounter, :lastBalance, :lastUpdated)";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("customerUid", transactionCounter.getCustomerUid(), Types.VARCHAR)
                .addValue("lastPurseCounter", transactionCounter.getLastPurseCounter(), Types.BIGINT)
                .addValue("lastUpdated", new Date(), Types.TIMESTAMP)
                .addValue("lastBalance", transactionCounter.getLastBalance(), Types.DECIMAL);
        getNamedParameterJdbcTemplate().update(sqlString, parameters);
    }

    @Override
    public boolean exists(String customerUid) {
        String sqlString = "SELECT CASE WHEN EXISTS(SELECT C.CUSTUMER_UID FROM SETTLE_PURSE_COUNTER C WHERE C.CUSTUMER_UID=:customerUid) THEN 1 ELSE 0 END FROM DUAL";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("customerUid", customerUid, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameterSource, Integer.class) > 0;
    }

    @Override
    public TransactionCounter selectOne(String customerUid) {
        String sqlString = "SELECT TC.CUSTUMER_UID customerUid, TC.CURRENT_COUNTER lastPurseCounter, TC.LAST_BALANCE lastBalance, TC.LAST_UPDATED lastUpdated FROM SETTLE_PURSE_COUNTER TC WHERE TC.CUSTUMER_UID=:customerUid";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("customerUid", customerUid, Types.VARCHAR);
        return getNamedParameterJdbcTemplate().queryForObject(sqlString, parameterSource, defaultRowMapper);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(TransactionCounter transactionCounter) {
        Assert.isTrue(exists(transactionCounter.getCustomerUid()), String.format("Trying to update non existent transaction counter for customer with uid %s", transactionCounter.getCustomerUid()));

        String sqlString = "UPDATE SETTLE_PURSE_COUNTER SET CURRENT_COUNTER=:lastPurseCounter, LAST_BALANCE=:lastBalance, LAST_UPDATED=:lastUpdated WHERE CUSTUMER_UID=:customerUid";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("customerUid", transactionCounter.getCustomerUid(), Types.VARCHAR)
                .addValue("lastPurseCounter", transactionCounter.getLastPurseCounter(), Types.BIGINT)
                .addValue("lastBalance", transactionCounter.getLastBalance(), Types.DECIMAL)
                .addValue("lastUpdated", new Date(), Types.TIMESTAMP);
        getNamedParameterJdbcTemplate().update(sqlString, parameterSource);
    }

    @Override
    public void delete(String customerUid) {
        Assert.isTrue(exists(customerUid), String.format("Trying to remove non existent transaction counter for customer with uid %s", customerUid));

        String sqlString = "DELETE FROM SETTLE_PURSE_COUNTER WHERE CUSTUMER_UID=:customerUid";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("customerUid", "customerUid", Types.VARCHAR);
        getNamedParameterJdbcTemplate().update(sqlString, parameterSource);
    }
}
 