package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.common.integration.RequestRetrySettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.handler.advice.RequestHandlerRetryAdvice;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.BackOffPolicy;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.backoff.NoBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>A {@link Configuration} for doing retry on correlate tcash sticker's uid to msisdn service invocation,
 * in case error happen on invocation.
 *
 * @author zakyalvan
 * @since 18 May 2017
 */
@Configuration
public class CorrelateUidRequestRetryConfiguration {
    @Autowired
    private CorrelateUidSettings correlateSettings;

    /**
     * <p>A {@link RequestHandlerRetryAdvice} type to be attached on http outbound gateway for
     * checking btpn-tcash customer linkage.
     *
     * @return
     */
    @Bean("correlateUidRetryAdvice")
    RequestHandlerRetryAdvice correlateUidRetryAdvice() {
        RequestHandlerRetryAdvice retryAdvice = new RequestHandlerRetryAdvice();
        retryAdvice.setRetryTemplate(correlateUidRetryTemplate());
        return retryAdvice;
    }

    @Bean
    RetryTemplate correlateUidRetryTemplate() {
        RetryTemplate retryTemplate = new RetryTemplate();
        retryTemplate.setRetryPolicy(correlateUidRetryPolicy());
        retryTemplate.setBackOffPolicy(correlateUidBackoffPolicy());

        /**
         * Throw original exception instead of default {@link org.springframework.retry.ExhaustedRetryException}
         * exception type.
         */
        retryTemplate.setThrowLastExceptionOnExhausted(true);
        return retryTemplate;
    }

    @Bean
    RetryPolicy correlateUidRetryPolicy() {
        Map<Class<? extends Throwable>, Boolean> retryableExceptions = new HashMap<>();
        for(Class<? extends Throwable> retryException : correlateSettings.getRetry().getRetryableExceptions()) {
            retryableExceptions.put(retryException, true);
        }
        for(Class<? extends Throwable> unretryException : correlateSettings.getRetry().getUnretryableExceptions()) {
            retryableExceptions.put(unretryException, false);
        }

        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(correlateSettings.getRetry().getMaxAttempts(), retryableExceptions, correlateSettings.getRetry().isTraverseExceptionCause());
        return retryPolicy;
    }

    /**
     * <p>Backoff policy of retry process.
     *
     * @return
     */
    @Bean
    BackOffPolicy correlateUidBackoffPolicy() {
        if(correlateSettings.getRetry().getBackOffType() == RequestRetrySettings.RetryBackoff.EXPONENTIAL) {
            ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
            backOffPolicy.setInitialInterval(correlateSettings.getRetry().getPeriodMillis());
            backOffPolicy.setMultiplier(correlateSettings.getRetry().getBackOffMultiplier());
            return backOffPolicy;
        }
        else if(correlateSettings.getRetry().getBackOffType() == RequestRetrySettings.RetryBackoff.FIXED) {
            FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
            backOffPolicy.setBackOffPeriod(correlateSettings.getRetry().getPeriodMillis());
            return backOffPolicy;
        }
        else {
            NoBackOffPolicy backOffPolicy = new NoBackOffPolicy();
            return backOffPolicy;
        }
    }
}
