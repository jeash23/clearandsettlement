package com.huawei.tsel.settlement.flow;

import com.jcraft.jsch.ChannelSftp.LsEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>A {@link org.springframework.integration.file.filters.FileListFilter} which reponsible for filtering
 * sftp file which not contains valid timestamp in filename</p>
 *
 * @author zakyalvan
 * @since 14 Sep 2017
 */
class SftpFileNameContainsTimestampFilter extends AbstractFileNameContainsTimestampFilter<LsEntry> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpFileNameContainsTimestampFilter.class);

    SftpFileNameContainsTimestampFilter(String namePattern, String timestampFormat, int timestampGroup) {
        super(namePattern, timestampFormat, timestampGroup);
    }

    @Override
    protected String extractName(LsEntry file) {
        String fileName = file.getFilename();
        LOGGER.trace("Extracted file name '{}' from sftp file object", fileName);
        return fileName;
    }
}