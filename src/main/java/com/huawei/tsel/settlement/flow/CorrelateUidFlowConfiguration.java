package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.dto.CorrelateUidRequest;
import com.huawei.tsel.settlement.dto.CorrelateUidResult;
import com.huawei.tsel.settlement.dto.CorrelateUidResult.CheckStatus;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.handler.advice.RequestHandlerRetryAdvice;
import org.springframework.integration.http.outbound.HttpRequestExecutingMessageHandler;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;

/**
 * <p>An {@link IntegrationFlow} {@link org.springframework.context.annotation.Configuration}.
 *
 * @author zakyalvan
 * @since 18 Mar 2017
 */
@Configuration
public class CorrelateUidFlowConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(CorrelateUidFlowConfiguration.class);

    private static final String REQUEST_CACHE_HEADER = "OriginalCheckUIDRequest";

    @Autowired
    private CorrelateUidSettings correlateSettings;

    @Autowired
    private CorrelateUidChannels correlateChannels;

    @Autowired
    @Qualifier("correlateUidRetryAdvice")
    private RequestHandlerRetryAdvice correlateRetryAdvice;

    @Bean
    IntegrationFlow correlateFlow() {
        return flow -> flow.channel(correlateChannels.correlateParameters())
                .gateway(correlateMainFlow(), specs -> specs.replyChannel(correlateChannels.correlateResults())
                        .errorChannel(correlateChannels.correlateErrors()));
    }

    /**
     * <p>Integration flow for checking TCASH UID against
     *
     * @return
     */
    @Bean
    IntegrationFlow correlateMainFlow() {
        return flow -> flow.enrich(specs -> specs.header(MessageHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE))
                .enrich(specs -> specs.headerFunction(REQUEST_CACHE_HEADER, message -> message.getPayload()))
                .handle(correlateOutboundGateway())
                .<String> handle((payload, headers) -> {
                    LOGGER.trace("Handle response from check tcash sticker uid with payload {}", payload);
                    String[] parts = StringUtils.delimitedListToStringArray(payload, ":");
                    CorrelateUidRequest parameters = (CorrelateUidRequest) headers.get(REQUEST_CACHE_HEADER);
                    CheckStatus status = CheckStatus.withCode(parts[0]);
                    CorrelateUidResult result = new CorrelateUidResult(parameters.getUid(), parts[1]);
                    return MessageBuilder.withPayload(result)
                            .copyHeaders(headers).removeHeader(REQUEST_CACHE_HEADER)
                            .build();
                });
    }

    /**
     * A {@link RestTemplate} for sending correlate uid request to related mfs address.
     *
     * @return
     */
    @Bean("correlateUidRestTemplate")
    RestTemplate correlateUidRestTemplate() {
        RestTemplate restTemplate = new RestTemplate() {
            @Override
            public void setRequestFactory(ClientHttpRequestFactory requestFactory) {
                if(requestFactory instanceof BufferingClientHttpRequestFactory)
                    super.setRequestFactory(requestFactory);
                else
                    super.setRequestFactory(new BufferingClientHttpRequestFactory(requestFactory));
            }
        };
        restTemplate.setRequestFactory(correlateUidHttpRequestFactory());
        restTemplate.setErrorHandler(new CorrelateUidResponseErrorHandler());
        return restTemplate;
    }

    /**
     * <p>A {@link ClientHttpRequestFactory} responsible for creating http request
     * to correlate tcash sticker uid to msisdn.
     *
     * @return
     */
    @Bean
    OkHttp3ClientHttpRequestFactory correlateUidHttpRequestFactory() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .retryOnConnectionFailure(false)
                .hostnameVerifier((hostname, session) -> {
                    LOGGER.warn("FIXME : Https host name verification on correlating uid is bypassed...");
                    return true;
                });

        OkHttp3ClientHttpRequestFactory requestFactory = new OkHttp3ClientHttpRequestFactory(builder.build());
        return requestFactory;
    }

    /**
     * Outbound http gateway which responsible to make request to MFS for correlate or checking
     * customer msisdn by uid using http get request.
     *
     * @return
     */
    @Bean
    HttpRequestExecutingMessageHandler correlateOutboundGateway() {
        String correlateUidUri = correlateSettings.getEndpoint().getAddress().toUriComponentsBuilder()
                .queryParam("user", correlateSettings.getEndpoint().getUsername())
                .queryParam("pwd", correlateSettings.getEndpoint().getPassword())
                .queryParam("uid", "{uid}")
                .build().toUriString();
        LOGGER.trace("Correlate uid uri template : {}", correlateUidUri);
        HttpRequestExecutingMessageHandler outboundGateway = Http.outboundGateway(correlateUidUri, correlateUidRestTemplate())
                .encodeUri(true)
                .httpMethod(HttpMethod.GET)
                .expectedResponseType(String.class)
                .<CorrelateUidRequest> uriVariablesFunction(message -> Collections.singletonMap("uid", message.getPayload().getUid()))
                .get();

        if(correlateSettings.getRetry().isEnabled()) {
            LOGGER.info("Retry of correlate uid api invocation is enabled, set retry advice for correlate uid http outbound gateway");
            outboundGateway.setAdviceChain(Arrays.asList(correlateRetryAdvice));
        }

        return outboundGateway;
    }

    /**
     * <p>An {@link IntegrationFlow} for handling error emitted from main flow.
     *
     * @return
     */
    @Bean
    IntegrationFlow correlateErrorFlow() {
        return flow -> flow.channel(correlateChannels.correlateErrors())
                .wireTap(tapFlow -> tapFlow.handle(message -> LOGGER.trace("Handle correlate uid errors")))
                .<MessagingException> handle((error, headers) -> {
                    CorrelateUidRequest parameters = (CorrelateUidRequest) error.getFailedMessage().getHeaders().get(REQUEST_CACHE_HEADER);

                    if(error.getCause() instanceof CorrelateUidException.NoResponseBodyException) {
                        return new CorrelateUidResult(parameters.getUid(), CheckStatus.UID_NOT_PAIRED, "No response body");
                    }
                    else if(error.getCause() instanceof CorrelateUidException.CantParseResponseException) {
                        return new CorrelateUidResult(parameters.getUid(), CheckStatus.UID_NOT_PAIRED, "Can not parse response");
                    }
                    else if(error.getCause() instanceof CorrelateUidException.UnknownCorrelateCodeException) {
                        return new CorrelateUidResult(parameters.getUid(), CheckStatus.UID_NOT_PAIRED, "Unknown correlation code");
                    }
                    else if(error.getCause() instanceof CorrelateUidException.FailedCorrelateCodeException) {
                        return new CorrelateUidResult(parameters.getUid(), ((CorrelateUidException.FailedCorrelateCodeException) error.getCause()).getCheckStatus(), "Unknown error actually");
                    }
                    else {
                        LOGGER.error("Handle unknown exception on correlating tcash sticker uid, see stack traces.", error);
                        return new CorrelateUidResult(parameters.getUid(), CheckStatus.UID_NOT_PAIRED, "Unknown error actually");
                    }
                });
    }
}
