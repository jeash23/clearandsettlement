package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.dto.LaunchProcessCommand;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Payload;

import java.util.concurrent.Future;

/**
 * <p>Launching batch processing job gateway, to enable manual launching of processing instead of default scheduled mechanism</p>
 *
 * @author zakyalvan
 * @since 26 Sep 2017
 */
@MessagingGateway
public interface BatchJobLaunchGateway {
    /**
     * <p>Launch processing</p>
     *
     * @param launchCommand
     * @return
     */
    @Gateway(requestChannel = BatchJobLaunchSettings.LAUNCH_REQUEST_CHANNEL)
    Future<Void> launchProcessing(@Payload LaunchProcessCommand launchCommand);
}
