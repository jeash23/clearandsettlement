package com.huawei.tsel.settlement.flow;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.dsl.channel.MessageChannels;

/**
 * @author zakyalvan
 * @since 10 Aug 2017
 */
@Configuration
public class TransactionQueryChannels {
    @Bean
    public DirectChannel queryRequests() {
        return MessageChannels.direct().get();
    }

    @Bean
    public DirectChannel queryResults() {
        return MessageChannels.direct().get();
    }

    @Bean
    public PublishSubscribeChannel queryErrors() {
        return MessageChannels.publishSubscribe().get();
    }
}
