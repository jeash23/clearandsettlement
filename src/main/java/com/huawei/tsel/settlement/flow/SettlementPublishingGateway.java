package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.entity.SettlementSummary;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Payload;

import java.util.concurrent.Future;

/**
 * <p>Contract for publishing settlement gateway</p>
 *
 * @author zakyalvan
 * @since 26 Sep 2017
 */
@MessagingGateway
public interface SettlementPublishingGateway {
    /**
     * <p>Publish settlement to cps endpoint</p>
     *
     * FIXME
     *
     * Return the settlement summary object, instead of void type.
     *
     * @param settlementSummary
     */
    @Gateway(requestChannel = SettlementPublishingFlowConfiguration.SETTLEMENT_REQUEST_CHANNEL)
    Future<Void> publishSettlement(@Payload SettlementSummary settlementSummary);
}
