package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.entity.InterfaceFile;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import com.huawei.tsel.settlement.support.DefaultInterfaceFilePriorityCalculator;
import com.huawei.tsel.settlement.support.InterfaceFilePriorityCalculator;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.integration.aop.AbstractMessageSourceAdvice;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.dsl.file.Files;
import org.springframework.integration.dsl.sftp.Sftp;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.file.filters.*;
import org.springframework.integration.file.remote.gateway.AbstractRemoteFileOutboundGateway;
import org.springframework.integration.file.support.FileExistsMode;
import org.springframework.integration.sftp.filters.SftpRegexPatternFileListFilter;
import org.springframework.integration.sftp.gateway.SftpOutboundGateway;
import org.springframework.integration.sftp.inbound.SftpInboundFileSynchronizingMessageSource;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.integration.util.CompoundTrigger;
import org.springframework.messaging.Message;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.function.Function;

/**
 * <p>A {@link Configuration} components responsible for copying settlement file from sharing ftp/sftp server.
 * Please note, duplicate file must be ignore for subsequent copy.</p>
 *
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@Configuration
public class InterfaceFileCopyFlowConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(InterfaceFileCopyFlowConfiguration.class);

    @Autowired
    private InterfaceFileCopySettings copySettings;

    @Autowired
    private TransactionSettlementService settlementService;

    /**
     * <p>An {@link IntegrationFlow} for copying file from shared ftp/sftp directory into backup directory and
     * registering settlement interface meta data of {@link InterfaceFile} copied.</p>
     *
     * @return
     * @throws Exception
     */
    @Bean
    IntegrationFlow copyFileFlow() throws Exception {
        return flow -> flow.transform(interfaceFileTransformer())
                .<InterfaceFile> handle((payload, headers) -> settlementService.registerInterfaceFile(payload));
    }

    @Bean
    IntegrationFlow remoteFileCopyFlow() throws Exception {
        return IntegrationFlows.from(remoteInterfaceFileMessageSource(), specs -> specs.poller(Pollers.trigger(copySettings.getSchedule().get()).maxMessagesPerPoll(100).taskExecutor(remoteInterfaceFileCopyExecutor()).advice(remoteFileCopyEnabledAdvice())))
                .wireTap(flow -> flow.handle(message -> LOGGER.info("Start process copying settlement interface file ({}) from remote file source", ((File) message.getPayload()).getName() )))
                .gateway(copyFileFlow())
                .handle(message -> LOGGER.info("Copying settlement interface file ({}) from remote file finished", ((InterfaceFile) message.getPayload()).getOriginalName() ))
                .get();
    }

    /**
     * <p>Advice to be applied to remote file {@link MessageSource} so that aware for several condition, for example : </p>
     * <ul>
     *     <li>Check whether copy scheduled is enabled</li>
     *     <li></li>
     * </ul>
     *
     * @return
     */
    @Bean
    AbstractMessageSourceAdvice remoteFileCopyEnabledAdvice() {
        return new AbstractMessageSourceAdvice() {
            @Override
            public boolean beforeReceive(MessageSource<?> source) {
                if(!copySettings.getSchedule().isEnabled()) {
                    LOGGER.trace("Scheduled polling of remote file remote file source is disabled, abort polling");
                    return false;
                }
                return true;
            }

            @Override
            public Message<?> afterReceive(Message<?> result, MessageSource<?> source) {
                return result;
            }
        };
    }

    /**
     * <p>Trigger for remote file copy. Using {@link CompoundTrigger} so that can be overriden on runtime,
     * for example to enable user to do manual poll file from remote file repository</p>
     *
     * <p>Consider to to use {@link org.springframework.integration.aop.CompoundTriggerAdvice} advice on pollers</p>
     *
     * @return
     */
    @Bean
    CompoundTrigger remoteFileCopyTrigger() {
        CompoundTrigger trigger = new CompoundTrigger(copySettings.getSchedule().get());
        return trigger;
    }

    @Bean
    TaskExecutor remoteInterfaceFileCopyExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(20);
        taskExecutor.setMaxPoolSize(100);
        taskExecutor.setQueueCapacity(1000);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setAllowCoreThreadTimeOut(false);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setThreadNamePrefix("remote-copy-");
        return taskExecutor;
    }

    /**
     * <p>Message source for settlement interface file</p>
     *
     * @return
     * @throws IOException
     */
    @Bean
    SftpInboundFileSynchronizingMessageSource remoteInterfaceFileMessageSource() throws Exception {

        SftpInboundFileSynchronizingMessageSource messageSource = Sftp.inboundAdapter(sftpSessionFactory(), remoteInterfaceFileReceptionComparator())
                .remoteDirectory(copySettings.getRemoteFile().getSourceDirectory())
                .remoteFileSeparator(copySettings.getRemoteFile().getFileSeparator())
                .filter(remoteInterfaceFileListFilter())
                .deleteRemoteFiles(copySettings.getRemoteFile().isDeleteSourceFiles())
                .preserveTimestamp(copySettings.getBackupFile().isPreserveTimestamp())
                .localDirectory(new File(copySettings.getBackupFile().getTargetDirectory().getFile(), "Remote"))
                .autoCreateLocalDirectory(copySettings.getBackupFile().isAutoCreateDirectory())
                .localFilter(backupInterfaceFileListFilter())
                .temporaryFileSuffix(copySettings.getBackupFile().getTemporaryFileSuffix())
                .get();
        return messageSource;
    }

    /**
     * <p></p>
     *
     * @return
     * @throws IOException
     */
    //@Bean
    SftpOutboundGateway remoteInterfaceFileOutboundGateway() throws IOException {
        SftpOutboundGateway gateway = new SftpOutboundGateway(sftpSessionFactory(), AbstractRemoteFileOutboundGateway.Command.LS.getCommand(), "");
        gateway.setAutoCreateLocalDirectory(true);
        return gateway;
    }

    @Bean
    Comparator<File> remoteInterfaceFileReceptionComparator() {
        String timestampPattern = copySettings.getFileName().getTimestampPattern();
        Integer timestampGroup = copySettings.getFileName().getTimestampGroup();
        String namePattern = copySettings.getFileName().getFileNamePattern();

        InterfaceFilePriorityCalculator priorityCalculator = new DefaultInterfaceFilePriorityCalculator(namePattern, timestampPattern, timestampGroup);
        return new InterfaceFileReceptionComparator(priorityCalculator);
    }

    /**
     * <p>Sftp session factory</p>
     *
     * @return
     */
    @Bean
    DefaultSftpSessionFactory sftpSessionFactory() {
        DefaultSftpSessionFactory sessionFactory = new DefaultSftpSessionFactory();
        sessionFactory.setHost(copySettings.getSftpConnection().getHost());
        sessionFactory.setPort(copySettings.getSftpConnection().getPort());

        sessionFactory.setUser(copySettings.getSftpConnection().getUsername());
        sessionFactory.setAllowUnknownKeys(true);
        if (copySettings.getSftpConnection().isUsingPassword()) {
            sessionFactory.setPassword(copySettings.getSftpConnection().getPassword());
        } else {
            sessionFactory.setPrivateKey(copySettings.getSftpConnection().getPrivateKey());
            sessionFactory.setPrivateKeyPassphrase(copySettings.getSftpConnection().getPrivateKeyPassPhrase());
        }
        return sessionFactory;
    }

    @Bean
    FileListFilter<LsEntry> remoteInterfaceFileListFilter() {
        ChainFileListFilter<LsEntry> filters = new ChainFileListFilter();

        String fileNamePattern = copySettings.getFileName().getFileNamePattern();
        SftpRegexPatternFileListFilter regexPatternFilter = new SftpRegexPatternFileListFilter(fileNamePattern);
        filters.addFilter(regexPatternFilter);

        String timestampFormat = copySettings.getFileName().getTimestampFormat();
        Integer timestampGroup = copySettings.getFileName().getTimestampGroup();
        SftpFileNameContainsTimestampFilter containsTimestampFilter = new SftpFileNameContainsTimestampFilter(fileNamePattern, timestampFormat, timestampGroup);
        filters.addFilter(containsTimestampFilter);

        AbstractFileListFilter<LsEntry> acceptOnceFilter = new AbstractFileListFilter<LsEntry>() {
            @Override
            protected boolean accept(LsEntry file) {
                String fileName = file.getFilename();
                if(settlementService.hasInterfaceFileWithName(fileName)) {
                    LOGGER.trace("Interface file with original name '{}' already registered in database, reject file", fileName);
                    return false;
                }
                return true;
            }
        };
        filters.addFilter(acceptOnceFilter);

        return filters;
    }

    @Bean
    IntegrationFlow localFileCopyFlow() throws Exception {
        return IntegrationFlows.from(localInterfaceFileMessageSource(), specs -> specs.poller(Pollers.trigger(copySettings.getSchedule().get()).maxMessagesPerPoll(100).taskExecutor(localInterfaceFileCopyExecutor())))
                .wireTap(flow -> flow.handle(message -> LOGGER.info("Start process copying settlement interface file ({}) from local file source", ((File) message.getPayload()).getName() )))
                .handle(backupLocalInterfaceFileHandler())
                .wireTap(flow -> flow.handle(message -> LOGGER.info("File {}, backed up in backup directory", message.getPayload())))
                .gateway(copyFileFlow())
                .handle(message -> LOGGER.info("Copying settlement interface file ({}) from remote file finished", ((InterfaceFile) message.getPayload()).getOriginalName() ))
                .get();
    }

    @Bean
    TaskExecutor localInterfaceFileCopyExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(20);
        taskExecutor.setMaxPoolSize(100);
        taskExecutor.setQueueCapacity(1000);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setAllowCoreThreadTimeOut(false);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setThreadNamePrefix("local-copy-");
        return taskExecutor;
    }

    /**
     * <p>Local interface file source</p>
     *
     * @return
     */
    @Bean
    FileReadingMessageSource localInterfaceFileMessageSource() throws IOException {
        return Files.inboundAdapter(copySettings.getLocalFile().getSourceDirectory().getFile())
                .filter(localInterfaceFileListFilter())
                .autoCreateDirectory(copySettings.getLocalFile().isAutoCreateDirectory())
                .nioLocker()
                .get();
    }

    @Bean
    FileListFilter<File> localInterfaceFileListFilter() {
        ChainFileListFilter<File> filters = new ChainFileListFilter<>();

        String fileNamePattern = copySettings.getFileName().getFileNamePattern();
        RegexPatternFileListFilter regexPatternFilter = new RegexPatternFileListFilter(fileNamePattern);
        filters.addFilter(regexPatternFilter);

        String timestampFormat = copySettings.getFileName().getTimestampFormat();
        Integer timestampGroup = copySettings.getFileName().getTimestampGroup();
        LocalFileNameContainsTimestampFilter containsTimestampFilter = new LocalFileNameContainsTimestampFilter(fileNamePattern, timestampFormat, timestampGroup);
        filters.addFilter(containsTimestampFilter);

        IgnoreHiddenFileListFilter ignoreHiddenFilter = new IgnoreHiddenFileListFilter();
        filters.addFilter(ignoreHiddenFilter);

        AbstractFileListFilter<File> acceptOnceFilter = new AbstractFileListFilter<File>() {
            @Override
            protected boolean accept(File file) {
                String fileName = file.getName();
                if(settlementService.hasInterfaceFileWithName(fileName)) {
                    LOGGER.trace("Interface file with original name '{}' already registered in database, reject file", fileName);
                    return false;
                }
                return true;
            }
        };
        filters.addFilter(acceptOnceFilter);

        return filters;
    }

    @Bean
    FileWritingMessageHandler backupLocalInterfaceFileHandler() throws IOException {
        return Files.outboundGateway(new File(copySettings.getBackupFile().getTargetDirectory().getFile(),"Local"))
                .autoCreateDirectory(copySettings.getBackupFile().isAutoCreateDirectory())
                .deleteSourceFiles(copySettings.getLocalFile().isRemoveAfterBackup())
                .temporaryFileSuffix(copySettings.getBackupFile().getTemporaryFileSuffix())
                .fileExistsMode(FileExistsMode.IGNORE)
                .charset("UTF-8")
                .get();
    }

    @Bean
    FileListFilter<File> backupInterfaceFileListFilter() {
        ChainFileListFilter<File> filters = new ChainFileListFilter<>();

        IgnoreHiddenFileListFilter ignoreHiddenFilter = new IgnoreHiddenFileListFilter();
        filters.addFilter(ignoreHiddenFilter);

        AbstractFileListFilter<File> acceptOnceFilter = new AbstractFileListFilter<File>() {
            @Override
            protected boolean accept(File file) {
                return !settlementService.hasInterfaceFileWithName(file.getName());
            }
        };
        filters.addFilter(acceptOnceFilter);

        return filters;
    }

    @Bean
    GenericTransformer<File, InterfaceFile> interfaceFileTransformer() {
        InterfaceFileMetaTransformer transformer = new InterfaceFileMetaTransformer();
        return transformer;
    }
}