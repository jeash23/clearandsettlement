package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.entity.InterfaceFile;
import com.huawei.tsel.settlement.entity.InterfaceFile.ProcessingState;
import com.huawei.tsel.settlement.support.DefaultInterfaceFilePriorityCalculator;
import com.huawei.tsel.settlement.support.InterfaceFilePriorityCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.util.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p>A {@link GenericTransformer} responsible for transforming a settlement {@link File} object into
 * {@link InterfaceFile} meta data to be persisted into database, so that can be used later.
 *
 * @author zakyalvan
 * @since 7 Aug 2017
 */
public class InterfaceFileMetaTransformer implements GenericTransformer<File, InterfaceFile>, InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(InterfaceFileMetaTransformer.class);

    @Autowired
    private InterfaceFileCopySettings copySettings;

    /**
     * @see #afterPropertiesSet()
     */
    private InterfaceFilePriorityCalculator priorityCalculator;

    @Override
    public InterfaceFile transform(File source) {
        LOGGER.debug("Transform a file object into settlement file meta to be persisted and used later.");

        InterfaceFile interfaceFile = InterfaceFile.builder()
                .recordId(UUID.randomUUID().toString())
                .storageDirectory(source.getParentFile().getAbsolutePath())
                .fileResource(new FileSystemResource(source))
                .internalName(source.getName())
                .originalName(source.getName())
                .processingPriority(priorityCalculator.calculatePriority(source))
                .acquiredTimestamp(LocalDateTime.now())
                .status(ProcessingState.NEW)
                .build();

        try {
            String fileChecksum = DigestUtils.md5DigestAsHex(new FileInputStream(source));
            interfaceFile.setChecksumValue(fileChecksum);
        } catch (IOException ioe) {
            LOGGER.error("I/O exception thrown on settlement file checksum (digest) calculation, just ignore this error", ioe);
        }

        LOGGER.trace("Interface file transform result {}", interfaceFile);

        return interfaceFile;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String namePattern = copySettings.getFileName().getFileNamePattern();
        String timestampFormat = copySettings.getFileName().getTimestampFormat();
        Integer timestampGroup = copySettings.getFileName().getTimestampGroup();
        this.priorityCalculator = new DefaultInterfaceFilePriorityCalculator(namePattern, timestampFormat, timestampGroup);
    }
}
