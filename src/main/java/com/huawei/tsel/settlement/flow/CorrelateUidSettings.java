package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.common.integration.EndpointAddress;
import com.huawei.tsel.common.integration.RequestRetrySettings;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author zakyalvan
 * @since 5 Feb 2017
 */
@Data
@Component
@Validated
@ConfigurationProperties(prefix = "huawei.settlement.check-uid")
public class CorrelateUidSettings {
    @Valid
    @NotNull
    @NestedConfigurationProperty
    private CorrelateEndpointSettings endpoint = new CorrelateEndpointSettings();

    @Valid
    @NotNull
    @NestedConfigurationProperty
    private RequestRetrySettings retry = new RequestRetrySettings();

    /**
     * Correlate uid endpoint settings.
     */
    @Data
    public static class CorrelateEndpointSettings implements Serializable {
        @Valid
        @NotNull
        @NestedConfigurationProperty
        private EndpointAddress address = new EndpointAddress("https", "10.49.0.10", 30021, "payment/HttpService/form/TelkomselNFCQueryTagLink");

        @NotBlank
        private String username = "wcooperator";

        @NotBlank
        private String password = "vv93KH03";
    }
}
