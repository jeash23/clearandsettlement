package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.support.InterfaceFilePriorityCalculator;
import com.huawei.tsel.settlement.support.PriorityCalculationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Comparator;
import java.util.Objects;

/**
 * @author zakyalvan
 * @since 18 Sep 2017
 */
public class InterfaceFileReceptionComparator implements Comparator<File> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InterfaceFileReceptionComparator.class);

    private final InterfaceFilePriorityCalculator priorityCalculator;

    public InterfaceFileReceptionComparator(InterfaceFilePriorityCalculator priorityCalculator) {
        this.priorityCalculator = Objects.requireNonNull(priorityCalculator);
    }

    @Override
    public int compare(File first, File second) {
        if(first.equals(second)) {
            return 0;
        }

        if(first != null && second != null) {
            try {
                Integer compared = priorityCalculator.calculatePriority(first).compareTo(priorityCalculator.calculatePriority(second));
                return compared;
            }
            catch (PriorityCalculationException e) {
                LOGGER.error("Can not compare interface file, see stack traces", e);
                return 0;
            }
        }
        else if(first != null && second == null) {
            return 1;
        }
        else if(first == null && second != null) {
            return -1;
        }
        else {
            return 0;
        }
    }
}
