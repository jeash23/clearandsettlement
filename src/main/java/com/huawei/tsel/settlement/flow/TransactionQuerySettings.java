package com.huawei.tsel.settlement.flow;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * @author zakyalvan
 * @since 10 Aug 2017
 */
@Data
@Component
@ConfigurationProperties(prefix = "huawei.settlement.transaction-inquiry")
public class TransactionQuerySettings {
    @Valid
    @NestedConfigurationProperty
    private final NotificationSettings notification = new NotificationSettings();

    @Valid
    @NestedConfigurationProperty
    private final LoggingSettings logging = new LoggingSettings();

    /**
     * <p>Sms notification settings</p>
     */
    @Data
    @SuppressWarnings("serial")
    public static class NotificationSettings implements Serializable {
        /**
         * Sender masking for offline transaction query response.
         */
        @NotBlank
        private String sender = "TCASH";

        /**
         * Flag whether to enable sending no transaction.
         */
        private boolean noTransactionEnabled = true;

        /**
         *
         */
        @NotBlank
        private String noTransactionTemplate = "Transaksi offline Anda tidak ditemukan";
    }

    /**
     * <p>Process logging settings</p>
     */
    @Data
    @SuppressWarnings("serial")
    public static class LoggingSettings implements Serializable {
        /**
         * Flag whether to print error or exception verbosely.
         */
        private boolean verboseError = true;
    }
}
