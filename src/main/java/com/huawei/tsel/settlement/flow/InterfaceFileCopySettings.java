package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.common.schedule.DefaultSchedulingSettings;
import com.huawei.tsel.common.schedule.SchedulingProperties.TriggerType;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>Settings for settlement file copy process from remote sftp file repository</p>
 *
 * @author zakyalvan
 * @see InterfaceFileCopyFlowConfiguration
 * @since 31 Jul 2017
 */
@Data
@Component
@ConfigurationProperties(prefix = "huawei.settlement.file-copy")
public class InterfaceFileCopySettings {
    @Valid
    @NestedConfigurationProperty
    private final CopyScheduleSettings schedule = CopyScheduleSettings.builder()
            .enabled(true).triggerType(TriggerType.CRON).cronExpression("0 0/1 * * * ?")
            .build();

    @Valid
    @NestedConfigurationProperty
    private final SftpConnectionSettings sftpConnection = new SftpConnectionSettings();

    @Valid
    @NestedConfigurationProperty
    private final RemoteFileSettings remoteFile = new RemoteFileSettings();

    @Valid
    @NestedConfigurationProperty
    private final LocalFileSettings localFile = new LocalFileSettings();

    @Valid
    @NestedConfigurationProperty
    private final BackupFileSettings backupFile = new BackupFileSettings();

    @Valid
    @NestedConfigurationProperty
    private final FileNameSettings fileName = new FileNameSettings();

    /**
     * <p>Type containing sftpConnection connection attribute/property value.
     */
    @Data
    @SuppressWarnings("serial")
    public static class SftpConnectionSettings implements Serializable {
        @NotBlank
        private String host = "localhost";

        @Min(0)
        @NotNull
        private Integer port = 22;

        /**
         * <p>Username for accessing sftp server</p>
         */
        @NotBlank
        private String username = "zakyalvan";

        /**
         * <p>Flag determine whether to use password authentication or private key for authentication against sftp server</p>
         */
        private boolean usingPassword = true;

        private String password = "kakikukeko";

        private Resource privateKey = new FileSystemResource("/home/zakyalvan/.ssh/id_rsa");

        private String privateKeyPassPhrase;
    }

    /**
     * <p>Type represent settings of local file source</p>
     */
    @Data
    @SuppressWarnings("serial")
    public static class LocalFileSettings implements Serializable {
        @NotNull
        private Resource sourceDirectory = new FileSystemResource("/home/zakyalvan/Documents/SettlementFiles/Local");

        private boolean autoCreateDirectory = true;

        private boolean removeAfterBackup = true;
    }

    /**
     * <p>Type represent setting of remote file</p>
     */
    @Data
    @SuppressWarnings("serial")
    public static class RemoteFileSettings implements Serializable {
        /**
         * <p>Where unprocessed file will be looked up from remote file repository</p>
         */
        @NotBlank
        private String sourceDirectory = "/home/zakyalvan/Documents/SettlementFiles/Remote";

        /**
         * <p>Where processed file stored in remote file repository</p>
         */
        @NotBlank
        private String backupDirectory = "/home/zakyalvan/Documents/SettlementFiles/Processed";

        @NotBlank
        private String fileSeparator = "/";

        /**
         * <p>Flag determine whether to remove remote source file or not after copying</p>
         */
        private boolean deleteSourceFiles = true;
    }

    /**
     * <p>Type represent settings of local file storage, i.e. storage of copied files</p>
     */
    @Data
    @SuppressWarnings("serial")
    public static class BackupFileSettings implements Serializable {
        @NotNull
        private Resource targetDirectory = new FileSystemResource("/home/zakyalvan/Documents/SettlementFiles/Backup");

        /**
         * <p>Flag determine whether to auto create back up directory</p>
         */
        private boolean autoCreateDirectory = true;

        @NotBlank
        private String temporaryFileSuffix = ".writing";

        private boolean preserveTimestamp = true;
    }

    /**
     * <p>Settings for interface file naming</p>
     */
    @Data
    @SuppressWarnings("serial")
    public static class FileNameSettings implements Serializable {
        /**
         * <p>Name prefix of settlement interface file</p>
         */
        @NotBlank
        private String fileNamePrefix = "OFFTRX_";

        /**
         * <p>Regular expression pattern for reading raw timestamp string from interface file name</p>
         */
        @NotBlank
        private String timestampPattern = "\\d{12}";

        /**
         * <p>Format of timestamp given in settlement interface file name, used to parse interface file</p>
         */
        @NotBlank
        private String timestampFormat = "yyyyMMddHHmm";

        /**
         * <p>Settlement interface file extension without '.'</p>
         */
        @NotBlank
        private String fileExtension = "txt";

        public String getFileNamePattern() {
            return "^" + fileNamePrefix + "(" + timestampPattern + ")\\." + fileExtension + "$";
        }

        public Integer getTimestampGroup() {
            return 1;
        }
    }

    /**
     * <p>Setting properties related to copy file schedule</p>
     */
    @Data
    @SuppressWarnings("serial")
    public static class CopyScheduleSettings extends DefaultSchedulingSettings {
        /**
         * <p>Flag determine whether scheduled copy of file enabled</p>
         */
        private boolean enabled = true;

        @Builder
        public CopyScheduleSettings(TriggerType triggerType, Integer triggerPeriod, Integer initialDelay, String cronExpression, boolean enabled) {
            super(triggerType, triggerPeriod, initialDelay, cronExpression);
            this.enabled = enabled;
        }
    }
}
