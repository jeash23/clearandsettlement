package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.entity.InterfaceFile;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.integration.launch.JobLaunchRequest;
import org.springframework.batch.integration.launch.JobLaunchingGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.integration.aop.AbstractMessageSourceAdvice;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.util.CompoundTrigger;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>Configuration of scheduled launching of settlement batch processing.
 *
 * @author zakyalvan
 * @since 4 Aug 2017
 */
@Configuration
public class BatchJobLaunchFlowConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchJobLaunchFlowConfiguration.class);

    @Autowired
    private BatchJobLaunchSettings launchSettings;

    @Autowired
    private TransactionSettlementService settlementService;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("transactionSettlementJob")
    private Job settlementJob;

    @Bean(BatchJobLaunchSettings.LAUNCH_REQUEST_CHANNEL)
    DirectChannel settlementJobParameters() {
        return MessageChannels.direct().datatype(InterfaceFile.class).get();
    }

    @Bean(BatchJobLaunchSettings.LAUNCH_ERROR_CHANNEL)
    PublishSubscribeChannel settlementJobErrors() {
        return MessageChannels.publishSubscribe().get();
    }

    /**
     * <p>An {@link IntegrationFlow} for launching transaction settlement job.
     *
     * @return
     */
    @Bean
    IntegrationFlow settlementJobLaunchFlow() {
        return IntegrationFlows.from(settlementFileSource(), specs -> specs.poller(Pollers.trigger(launchSettings.getSchedule().get()).maxMessagesPerPoll(100).taskExecutor(settlementJobLaunchTaskExecutor()).advice(settlementJobLaunchEnablerAdvice())))
                .split(specs -> specs.applySequence(false))
                .wireTap(flow -> flow.handle(message -> LOGGER.info("Launch batch processing for interface file '{}', with priority {}", ((InterfaceFile) message.getPayload()).getOriginalName(), ((InterfaceFile) message.getPayload()).getProcessingPriority() )))
                .<InterfaceFile, JobLaunchRequest> transform(source -> {
                    LOGGER.trace("Transform settlement file meta to job launch request object type");
                    JobParameters jobParameters = new JobParametersBuilder()
                            .addString(BatchJobLaunchSettings.FILE_NAME_PARAMETER,source.getStorageDirectory() + File.separatorChar + source.getInternalName(), true)
                            .addString(BatchJobLaunchSettings.INTERFACE_ID_PARAMETER, source.getRecordId(), false)
                            .addString(BatchJobLaunchSettings.FILE_CHECKSUM_PARAMETER, source.getChecksumValue(), false)
                            .addString(BatchJobLaunchSettings.LAUNCHING_TYPE_PARAMETER, "scheduled", false)
                            .toJobParameters();
                    return new JobLaunchRequest(settlementJob, jobParameters);
                })
                .gateway(launchFlow -> launchFlow.handle(settlementJobLaunchingGateway()), specs -> specs.errorChannel(settlementJobErrors()))
                .handle(message -> LOGGER.info("Batch processing for interface file completed..."))
                .get();
    }



    /**
     * <p></p>
     *
     * @return
     */
    @Bean
    CompoundTrigger settlementJobLaunchTrigger() {
        CompoundTrigger trigger = new CompoundTrigger(launchSettings.getSchedule().get());
        return trigger;
    }

    /**
     * <p></p>
     *
     * @return
     */
    @Bean
    AbstractMessageSourceAdvice settlementJobLaunchEnablerAdvice() {
        return new AbstractMessageSourceAdvice() {
            @Override
            public boolean beforeReceive(MessageSource<?> source) {
                if(!launchSettings.getSchedule().isEnabled()) {
                    LOGGER.trace("Scheduled launch of settlement job process is disabled, abort launching.");
                    return false;
                }
                return true;
            }

            @Override
            public Message<?> afterReceive(Message<?> result, MessageSource<?> source) {
                return result;
            }
        };
    }

    /**
     * <p>Task executor used in launching settlement batch job</p>
     *
     * @return
     */
    @Bean
    TaskExecutor settlementJobLaunchTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(20);
        taskExecutor.setMaxPoolSize(100);
        taskExecutor.setQueueCapacity(1000);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setAllowCoreThreadTimeOut(false);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setThreadNamePrefix("settle-job-");
        return taskExecutor;
    }

    /**
     * <p>Message source which responsible for reading unprocessed settlement interface files which persisted in database.</p>
     *
     * @return
     */
    @Bean
    MessageSource<List<InterfaceFile>> settlementFileSource() {
        return () -> {
            if(!launchSettings.getSchedule().isEnabled()) {
                LOGGER.trace("Launch of settlement file processing job disabled, abort processing");
                return null;
            }

            LOGGER.trace("Read unprocessed new interface file from database record");
            List<InterfaceFile> interfaceFiles = settlementService.getInterfaceFiles(InterfaceFile.ProcessingState.NEW);
            if(interfaceFiles.isEmpty()) {
                LOGGER.trace("No unprocessed new interface files, abort processing");
                return null;
            }

            List<String> interfaceFileInfo = interfaceFiles.stream()
                    .map(file -> file.getOriginalName() + ":" + file.getProcessingPriority())
                    .collect(Collectors.toList());
            LOGGER.trace("Unprocessed interface files ({}) found from database record, start processing", StringUtils.collectionToCommaDelimitedString(interfaceFileInfo));

            return MessageBuilder.withPayload(interfaceFiles).build();
        };
    }

    @Bean
    JobLaunchingGateway settlementJobLaunchingGateway() {
        return new JobLaunchingGateway(jobLauncher);
    }

    /**
     * <p>An {@link IntegrationFlow} responsible for handling exception on batch job processing launching</p>
     *
     * @return
     */
    @Bean
    IntegrationFlow settlementJobErrorFlow() {
        return IntegrationFlows.from(settlementJobErrors())
                .<MessagingException> filter(source -> JobExecutionException.class.isAssignableFrom(source.getCause().getClass()))
                .<MessagingException, JobExecutionException> transform(source -> (JobExecutionException) source.getCause())
                .<JobExecutionException, Boolean> route(exception -> launchSettings.getLogging().isSuppressErrors(), mapping -> mapping
                        .subFlowMapping("true", subFlow -> subFlow.<JobExecutionException> handle((payload, headers) -> {
                            LOGGER.error("Processing of batch job failed with exception type : {} ({})", payload.getClass().getName(), payload.getMessage());
                            return payload;
                        }))
                        .subFlowMapping("false", subFlow -> subFlow.<JobExecutionException> handle((payload, headers) -> {
                            LOGGER.error("Processing of batch job failed, see stack traces", payload);
                            return payload;
                        })))
                .handle(message -> LOGGER.info("Launch settlement job completed"))
                .get();
    }
}
