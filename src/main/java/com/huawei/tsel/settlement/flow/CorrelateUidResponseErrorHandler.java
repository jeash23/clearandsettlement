package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.dto.CorrelateUidResult.CheckStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author zakyalvan
 * @since 18 Apr 2017
 */
public class CorrelateUidResponseErrorHandler extends DefaultResponseErrorHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(CorrelateUidResponseErrorHandler.class);

    private boolean responseCodeError = false;

    private ErrorType responseErrorType;

    private CheckStatus failedResponseCode;

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        if(super.hasError(response)) {
            responseCodeError = true;
            return true;
        }

        byte[] responseBytes = getResponseBody(response);
        if(responseBytes.length == 0) {
            LOGGER.trace("Response received without body (0 bytes)");
            responseErrorType = ErrorType.NO_RESPONSE_BODY_ERROR;
            return true;
        }

        String responseString = new String(responseBytes, StandardCharsets.UTF_8);
        String[] responseParts = StringUtils.delimitedListToStringArray(responseString, ":");
        if(responseParts.length != 2) {
            LOGGER.trace("Invalid response format, can not parse response from check uid");
            responseErrorType = ErrorType.CAN_NOT_PARSE_RESPONSE_ERROR;
            return true;
        }

        CheckStatus checkStatus = CheckStatus.withCode(responseParts[0]);
        if(checkStatus == null) {
            LOGGER.trace("Unknown response code ({}) from correlate uid operation", responseParts[0]);
            responseErrorType = ErrorType.UNKNOWN_RESPONSE_CODE_ERROR;
            return true;
        }

        if(!checkStatus.equals(CheckStatus.SUCCESS)) {
            LOGGER.trace("Response code is not a success code representation");
            responseErrorType = ErrorType.FAILED_RESPONSE_CODE_ERROR;
            failedResponseCode = checkStatus;
            return true;
        }

        return false;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        if(responseCodeError) {
            LOGGER.trace("Handle response error code");
            super.handleError(response);
        }

        if(responseErrorType != null) {
            byte[] responseBytes = getResponseBody(response);
            String responseString = new String(responseBytes, StandardCharsets.UTF_8);

            if(responseErrorType.equals(ErrorType.NO_RESPONSE_BODY_ERROR)) {
                LOGGER.error("No response body for correlate uid request");
                throw new CorrelateUidException.NoResponseBodyException();
            }
            else if(responseErrorType.equals(ErrorType.CAN_NOT_PARSE_RESPONSE_ERROR)) {
                LOGGER.error("Can not parse response : '{}'", responseString);
                throw new CorrelateUidException.CantParseResponseException();
            }
            else if(responseErrorType.equals(ErrorType.UNKNOWN_RESPONSE_CODE_ERROR)) {
                LOGGER.error("Unknown correlation code on response : '{}'", responseString);
                throw new CorrelateUidException.UnknownCorrelateCodeException();
            }
            else if(responseErrorType.equals(ErrorType.FAILED_RESPONSE_CODE_ERROR)) {
                LOGGER.error("Failed error code on response : '{}'", responseString);
                throw new CorrelateUidException.FailedCorrelateCodeException(failedResponseCode);
            }
        }
    }

    /**
     * <p>Error type when checking UID.
     */
    public enum ErrorType {
        NO_RESPONSE_BODY_ERROR,
        CAN_NOT_PARSE_RESPONSE_ERROR,
        UNKNOWN_RESPONSE_CODE_ERROR,
        FAILED_RESPONSE_CODE_ERROR
    }
}
