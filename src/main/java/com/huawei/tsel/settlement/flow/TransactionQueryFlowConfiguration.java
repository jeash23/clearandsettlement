package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.common.integration.MultiValueMapBindingHandler;
import com.huawei.tsel.security.user.CredentialsRepository;
import com.huawei.tsel.settlement.dto.TransactionQueryRequest;
import com.huawei.tsel.settlement.dto.TransactionQueryResult;
import com.huawei.tsel.settlement.dto.TransactionQueryForm;
import com.huawei.tsel.settlement.entity.TransactionItem;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import com.huawei.tsel.smsgtwy.dto.SmsDelivery;
import com.huawei.tsel.smsgtwy.dto.SmsMessage;
import com.huawei.tsel.smsgtwy.flow.SmsGatewayChannels;
import com.huawei.tsel.smsgtwy.flow.SmsMessageFactoryTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.number.NumberStyleFormatter;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.dsl.support.GenericHandler;
import org.springframework.integration.http.inbound.HttpRequestHandlingMessagingGateway;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.validation.SmartValidator;

import java.util.Collections;
import java.util.Locale;
import java.util.function.Function;

/**
 * <p>Configuration for integration flow for querying latest customer's offline transaction.</p>
 *
 * <p>Please note, inbound http gateway defined here intended to be invoked by tmenu.</p>
 *
 * @author zakyalvan
 * @since 4 Aug 2017
 */
@Configuration
public class TransactionQueryFlowConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionQueryFlowConfiguration.class);

    @Autowired
    private TransactionSettlementService settlementService;

    @Autowired
    private TransactionQueryChannels transactionQueryChannels;

    @Autowired
    private TransactionQuerySettings transactionQuerySettings;

    @Autowired
    private SmsGatewayChannels smsChannels;

    @Autowired
    @Qualifier("offline-trx.credentials")
    private CredentialsRepository credentialsRepo;

    @Autowired
    @Qualifier("integrationConversionService")
    private ConversionService conversionService;

    @Autowired
    private SmartValidator validator;

    @Bean
    IntegrationFlow queryTransactionMainFlow() {

        return IntegrationFlows.from(inquiryTransactionHttpInboundGateway())
                .handle(inquiryInboundRequestBinder())
                .<TransactionQueryForm> handle((payload, headers) -> {
                    LOGGER.trace("Authenticating request, checking against adn as username and auth as password.");
                    String user = payload.getAdn();
                    String password = payload.getAuth();

                    if(!credentialsRepo.has(user) || !new String(credentialsRepo.get(user).getPassword()).equals(password)) {
                        LOGGER.error("Authentication request failed, invalid credentials provided on request.");
                        throw new TransactionQueryForm.InvalidCredentialsException("Authentication request failed, invalid credentials provided on request.");
                    }
                    return payload;
                })
                .<TransactionQueryForm, TransactionQueryRequest> transform(form -> TransactionQueryRequest.create().targetMsisdn(form.getMsisdn()).get())
                .<TransactionQueryRequest, Boolean> route(request -> settlementService.hasCustomerTransactions(request.getTargetMsisdn()), mapping -> mapping
                        .subFlowMapping("true", subFlow -> subFlow
                                .handle(inquiryTransactionHistoryHandler())
                                .enrich(specs -> specs.headerFunction("CACHED_QUERY_RESULT", message -> message.getPayload()))
                                .transform(inquiryResultToSmsTransformer()))
                        .subFlowMapping("false", subFlow -> subFlow
                                .<TransactionQueryRequest> handle((payload, headers) -> MessageBuilder.withPayload(TransactionQueryResult.builder().inquiryRequest(payload).success(true).build()).copyHeaders(headers).build())
                                .enrich(specs -> specs.headerFunction("CACHED_QUERY_RESULT", message -> message.getPayload()))
                                .transform(noInquiryResultToSmsTransformer())))
                .gateway(smsChannels.deliveryRequest())
                .<SmsDelivery> handle((payload, headers) -> MessageBuilder.withPayload(headers.get("CACHED_QUERY_RESULT"))
                        .copyHeaders(headers).removeHeader("CACHED_QUERY_RESULT")
                        .build())
                .<TransactionQueryResult, Boolean> route(result -> result.isSuccess(), mapping -> mapping
                        .subFlowMapping("true", subFlow -> subFlow
                                .<TransactionQueryResult, String> transform(result -> result.getInquiryRequest().getTargetMsisdn().concat(":").concat(new Long(result.getTransactions().size()).toString())))
                        .subFlowMapping("false", subFlow -> subFlow
                                .<TransactionQueryResult, String> transform(result -> "NOK")))
                .channel(transactionQueryChannels.queryResults())
                .get();
    }

    @Bean
    HttpRequestHandlingMessagingGateway inquiryTransactionHttpInboundGateway() {
        return Http.inboundGateway("/service/offline-trx-history")
                .payloadExpression("#requestParams")
                .requestMapping(mapping -> mapping.methods(HttpMethod.GET).consumes(MediaType.ALL_VALUE).produces(MediaType.TEXT_PLAIN_VALUE))
                .replyChannel(transactionQueryChannels.queryResults())
                .errorChannel(transactionQueryChannels.queryErrors())
                .statusCodeExpression(new SpelExpressionParser().parseExpression("T(org.springframework.http.HttpStatus).ACCEPTED"))
                .get();
    }

    @Bean
    MultiValueMapBindingHandler<TransactionQueryForm> inquiryInboundRequestBinder() {
        MultiValueMapBindingHandler<TransactionQueryForm> bindingHandler = new MultiValueMapBindingHandler<>();
        bindingHandler.setRequiresReply(true);
        return bindingHandler.setTargetFactory(() -> new TransactionQueryForm())
                .setBinderCostumizer(binder -> {
                    binder.setValidator(new TransactionQueryForm.Validator(validator));
                    binder.setConversionService(conversionService);
                })
                .setErrorTranslator(bindings -> new TransactionQueryForm.DataBindingException(bindings));
    }

    /**
     * <p>A {@link GenericHandler} responsible for fetching loaded transaction history from database</p>
     *
     * @return
     */
    @Bean
    GenericHandler<TransactionQueryRequest> inquiryTransactionHistoryHandler() {
        return (payload, headers) -> {
            Pageable pageable = new PageRequest(0, 5);
            Page<TransactionItem> transactionItems = settlementService.getCustomerTransactions(payload.getTargetMsisdn(), pageable);
            TransactionQueryResult queryResult = TransactionQueryResult.builder().inquiryRequest(payload)
                    .transactions(transactionItems.getContent())
                    .success(true)
                    .build();
            return MessageBuilder.withPayload(queryResult).copyHeaders(headers).build();
        };
    }

    @Bean
    GenericTransformer<TransactionQueryResult, SmsMessage> inquiryResultToSmsTransformer() {
        return new SmsMessageFactoryTransformer<>(result -> result.getInquiryRequest().getTargetMsisdn(),
                result -> transactionQuerySettings.getNotification().getSender(),
                inquirySmsNotificationContentFactory(),
                result -> Collections.emptyMap());
    }

    @Bean
    Function<TransactionQueryResult, String> inquirySmsNotificationContentFactory() {
        DateFormatter dateFormatter = new DateFormatter("MMM dd, yyyy HH:mm:ss");
        NumberStyleFormatter numberFormatter = new NumberStyleFormatter("#,###.##");
        return result -> {
            StringBuilder contentBuilder = new StringBuilder("Transaksi offline terakhir And a : ");
            for(TransactionItem transaction : result.getTransactions()) {
                String transactionLine = String.format("[%d- %s|%s|%s|%s]",
                        result.getTransactions().indexOf(transaction) + 1,
                        dateFormatter.print(transaction.getTransactionDate(), Locale.ENGLISH),
                        transaction.getMerchantId(),
                        numberFormatter.print(transaction.getTransactionAmount(), Locale.ENGLISH),
                        numberFormatter.print(transaction.getLastBalance(), Locale.ENGLISH));
                contentBuilder.append(transactionLine).append(" ");
            }
            return contentBuilder.toString();
        };
    }

    @Bean
    GenericTransformer<TransactionQueryResult, SmsMessage> noInquiryResultToSmsTransformer() {
        return new SmsMessageFactoryTransformer<>(result -> result.getInquiryRequest().getTargetMsisdn(),
                result -> transactionQuerySettings.getNotification().getSender(),
                result -> "Transaksi TCASH PASS Anda tidak ditemukan",
                result -> Collections.emptyMap());
    }

    @Bean
    IntegrationFlow queryTransactionErrorFlow() {
        return IntegrationFlows.from(transactionQueryChannels.queryErrors())
                .wireTap(flow -> flow.<MessagingException, Boolean> route(error -> transactionQuerySettings.getLogging().isVerboseError(),
                        mapping -> mapping
                                .subFlowMapping("true", subFlow -> subFlow.handle(message -> LOGGER.error("Errors happen on inquiry coupon process, see stack trace", message.getPayload())))
                                .subFlowMapping("false", subFlow -> subFlow.handle(message -> LOGGER.error("Errors happen on inquiry coupon process with cause type '{}' and message '{}'", ((MessagingException) message.getPayload()).getCause().getClass().getName(), ((MessagingException) message.getPayload()).getCause().getMessage())))))
                .<MessagingException, String> transform(exception -> "NOK")
                .channel(transactionQueryChannels.queryResults())
                .get();
    }
}
