package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.common.schedule.DefaultSchedulingSettings;
import com.huawei.tsel.common.schedule.SchedulingProperties;
import com.huawei.tsel.common.schedule.SchedulingProperties.TriggerType;
import lombok.Builder;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * <p>Settings for settlement batch process launching</p>
 *
 * @author zakyalvan
 * @since 14 Sep 2017
 */
@Data
@Component
@ConfigurationProperties(prefix = "huawei.settlement.job-launch")
public class BatchJobLaunchSettings {
    public static final String LAUNCH_REQUEST_CHANNEL = "BatchJobLaunchSettings.requestChannel";
    public static final String LAUNCH_ERROR_CHANNEL = "BatchJobLaunchSettings.errorChannel";

    public static final String FILE_NAME_PARAMETER = "interfaceFile";
    public static final String INTERFACE_ID_PARAMETER = "interfaceId";
    public static final String FILE_CHECKSUM_PARAMETER = "fileChecksum";
    public static final String LAUNCHING_TYPE_PARAMETER = "launchType";

    @Valid
    @NestedConfigurationProperty
    private final LaunchScheduleSettings schedule = LaunchScheduleSettings.builder()
            .enabled(true).triggerType(TriggerType.CRON).cronExpression("0 0/3 * * * ?").build();

    @Valid
    @NestedConfigurationProperty
    private final LaunchLoggingSettings logging = new LaunchLoggingSettings();

    @Data
    @SuppressWarnings("serial")
    public static class LaunchScheduleSettings extends DefaultSchedulingSettings {
        private boolean enabled;

        @Builder
        public LaunchScheduleSettings(TriggerType triggerType, Integer triggerPeriod, Integer initialDelay, String cronExpression, boolean enabled) {
            super(triggerType, triggerPeriod, initialDelay, cronExpression);
            this.enabled = enabled;
        }
    }

    @Data
    @SuppressWarnings("serial")
    public static class LaunchLoggingSettings implements Serializable {
        private boolean suppressErrors = false;
    }
}
