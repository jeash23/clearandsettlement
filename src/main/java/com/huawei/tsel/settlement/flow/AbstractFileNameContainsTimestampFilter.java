package com.huawei.tsel.settlement.flow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.integration.file.filters.AbstractFileListFilter;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author zakyalvan
 * @since 18 Sep 2017
 * @param <F>
 */
public abstract class AbstractFileNameContainsTimestampFilter<F> extends AbstractFileListFilter<F> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFileNameContainsTimestampFilter.class);

    protected final Pattern namePattern;

    protected final DateFormatter timestampFormat;

    protected final int timestampGroup;

    public AbstractFileNameContainsTimestampFilter(String namePattern, String timestampFormat, int timestampGroup) {
        Assert.hasText(namePattern, "Name pattern parameter must not be null or empty string");
        Assert.hasText(timestampFormat, "Timestamp format parameter must not be null or empty string");

        this.namePattern = Pattern.compile(namePattern);
        this.timestampFormat = new DateFormatter(timestampFormat);
        this.timestampGroup = timestampGroup;
    }

    @Override
    protected final boolean accept(F file) {
        return containsTimestamp(extractName(Objects.requireNonNull(file)));
    }

    /**
     * <p>Extract file name from given file type object</p>
     *
     * @param file
     * @return
     */
    abstract protected String extractName(F file);

    /**
     * <p>Check whether given file name contains timestamp part</p>
     *
     * @param fileName
     * @return
     */
    protected final boolean containsTimestamp(String fileName) {
        LOGGER.trace("Check whether file name ({}) contains valid timestamp", fileName);

        Matcher matcher = namePattern.matcher(fileName);
        if(!(matcher.find() && StringUtils.hasText(matcher.group(timestampGroup)))) {
            LOGGER.error("Filename ({}) not contain timestamp part, reject file", fileName);
            return false;
        }

        try {
            Date timestamp = timestampFormat.parse(matcher.group(timestampGroup), Locale.getDefault());
            LOGGER.trace("Filename ({}) contain valid timestamp ({}), accept file", fileName, timestamp);
            return true;
        }
        catch (ParseException pe) {
            LOGGER.error("Timestamp part ({}) in filename ({}) can not be parsed to date object, reject file", matcher.group(timestampGroup), fileName);
            return false;
        }
    }
}
