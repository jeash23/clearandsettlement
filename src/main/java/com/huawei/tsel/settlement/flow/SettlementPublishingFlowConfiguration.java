package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.cpsasync.model.request.Request;
import com.huawei.tsel.cpsasync.model.request.Request.Body;
import com.huawei.tsel.cpsasync.model.request.Request.Body.Identity;
import com.huawei.tsel.cpsasync.model.request.Request.Body.Identity.Initiator;
import com.huawei.tsel.cpsasync.model.request.Request.Body.Identity.ReceiverParty;
import com.huawei.tsel.cpsasync.model.request.Request.Body.TransactionRequest;
import com.huawei.tsel.cpsasync.model.request.Request.Body.TransactionRequest.Parameters;
import com.huawei.tsel.cpsasync.model.request.Request.Header;
import com.huawei.tsel.cpsasync.model.request.Request.Header.Caller;
import com.huawei.tsel.cpsasync.model.result.Result;
import com.huawei.tsel.settlement.batch.SettlementProcessingSettings;
import com.huawei.tsel.settlement.entity.ResultDescriptor;
import com.huawei.tsel.settlement.entity.SettlementSummary;
import com.huawei.tsel.settlement.entity.SettlementSummary.SettlementStatus;
import com.huawei.tsel.settlement.service.TransactionSettlementService;
import lombok.Data;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.integration.ws.MarshallingWebServiceOutboundGateway;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.ws.client.support.destination.DestinationProvider;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.http.ClientHttpRequestMessageSender;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * <p>Configuration for {@link IntegrationFlow} of...
 *
 * @author zakyalvan
 * @since 8 Aug 2017
 */
@Configuration
public class SettlementPublishingFlowConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(SettlementPublishingFlowConfiguration.class);

    private static final String CPS_INTERFACE_COMMAND_ID = "InitTrans_S2BOfflinePoolAccount";

    public static final String SETTLEMENT_REQUEST_CHANNEL = "pushSettlementInput";

    @Autowired
    private TransactionSettlementService settlementService;

    @Autowired
    private SettlementProcessingSettings processingSettings;

    @Autowired
    private RequestSchemaResources requestSchemas;

    @Autowired
    private ResultSchemaResources resultSchemas;

    private DateFormatter requestTimestampFormatter = new DateFormatter("yyyyMMddHHmmss");

    private DateFormatter conversationTimestampFormatter = new DateFormatter("yyyyMMdd'T'HHmmss");

    @Bean(SETTLEMENT_REQUEST_CHANNEL)
    ExecutorChannel pushSettlementInput() {
        return MessageChannels.executor(pushSettlementExecutor()).datatype(SettlementSummary.class).get();
    }

    @Bean
    PublishSubscribeChannel pushSettlementError() {
        return MessageChannels.publishSubscribe().datatype(MessagingException.class).get();
    }

    @Bean(name = "pushSettlementExecutor")
    ThreadPoolTaskExecutor pushSettlementExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(20);
        taskExecutor.setMaxPoolSize(100);
        taskExecutor.setQueueCapacity(1000);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setAllowCoreThreadTimeOut(false);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setThreadNamePrefix("settlement-");
        return taskExecutor;
    }

    @Bean
    GenericTransformer<SettlementSummary, Request> settlementRequestTransformer() {
        return settlementSummary -> {
            Request settlementRequest = new Request();

            Header header = new Header();
            header.setVersion("1.0");
            header.setCommandID(CPS_INTERFACE_COMMAND_ID);
            header.setOriginatorConversationID("S_X2013012921001");
            header.setConversationID("AG_".concat(conversationTimestampFormatter.print(Calendar.getInstance().getTime(), Locale.ENGLISH)));

            Caller caller = new Caller();
            caller.setCallerType(processingSettings.getEndpoint().getCallerType());
            caller.setThirdPartyID(processingSettings.getEndpoint().getCallerIdentifier());
            caller.setPassword(processingSettings.getEndpoint().getCallerPassword());

            header.setCaller(caller);

            header.setKeyOwner(1);
            header.setTimestamp(requestTimestampFormatter.print(Calendar.getInstance().getTime(), Locale.ENGLISH));

            settlementRequest.setHeader(header);

            Body body = new Body();

            Identity identity = new Identity();

            Initiator initiator = new Initiator();
            initiator.setIdentifierType(processingSettings.getEndpoint().getInitiatorType());
            initiator.setIdentifier(processingSettings.getEndpoint().getInitiatorIdentifier());
            initiator.setSecurityCredential(processingSettings.getEndpoint().getInitiatorCredential());
            identity.setInitiator(initiator);

            ReceiverParty receiverParty = new ReceiverParty();
            receiverParty.setIdentifierType(processingSettings.getEndpoint().getReceiverType());
            receiverParty.setIdentifier(settlementSummary.getMerchantShortCode());
            identity.setReceiverParty(receiverParty);

            body.setIdentity(identity);

            TransactionRequest transactionRequest = new TransactionRequest();
            Parameters transactionParameters = new Parameters();
            transactionParameters.setAmount(new Long(settlementSummary.getTransactionAmount().longValue()).toString());
            transactionParameters.setCurrency(processingSettings.getEndpoint().getTransactionCurrency());
            transactionParameters.setReasonType("SP Offline Pooling Account to Business Account Transfer");
            transactionRequest.setParameters(transactionParameters);

            body.setTransactionRequest(transactionRequest);

            settlementRequest.setBody(body);

            return settlementRequest;
        };
    }

    /**
     * @return
     */
    @Bean
    MarshallingWebServiceOutboundGateway pushSettlementOutboundGateway() {
        MarshallingWebServiceOutboundGateway outboundGateway = new MarshallingWebServiceOutboundGateway(pushSettlementRequestDestination(), settlementRequestMessageMarshaller(), settlementResultMessageUnmarshaller());
        outboundGateway.setMessageSender(pushSettlementWebServiceMessageSender());
        return outboundGateway;
    }

    @Bean
    WebServiceMessageSender pushSettlementWebServiceMessageSender() {
        ClientHttpRequestMessageSender messageSender = new ClientHttpRequestMessageSender();
        messageSender.setRequestFactory(pushSettlementHttpRequestFactory());
        return messageSender;
    }

    @Bean
    OkHttp3ClientHttpRequestFactory pushSettlementHttpRequestFactory() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .retryOnConnectionFailure(false)
                .hostnameVerifier((hostname, session) -> {
                    LOGGER.warn("FIXME : Https host name verification on sending web service message bypassed...");
                    return true;
                });

        OkHttp3ClientHttpRequestFactory requestFactory = new OkHttp3ClientHttpRequestFactory(builder.build());
        return requestFactory;
    }

    /**
     * <p>CPS address {@link DestinationProvider}.
     *
     * @return
     */
    @Bean
    DestinationProvider pushSettlementRequestDestination() {
        return () -> processingSettings.getEndpoint().getAddress().toUriComponentsBuilder().build().toUri();
    }

    /**
     * <p>{@link org.springframework.oxm.Marshaller} bean type responsible for
     * marshalling requestInputs, response soap xml message.
     *
     * @return
     */
    @Bean
    Marshaller settlementRequestMessageMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setCheckForXmlRootElement(false);
        marshaller.setSupportJaxbElementClass(true);

        marshaller.setPackagesToScan("com.huawei.tsel.cpsasync.model.request");
        marshaller.setSchemas(requestSchemas.getElement(), requestSchemas.getCommon(), requestSchemas.getRequest());

        Map<String, Object> properties = new HashMap<>();
        properties.put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setMarshallerProperties(properties);

        return marshaller;
    }

    /**
     * <p>A {@link Jaxb2Marshaller} bean type responsible for marshalling and unmarshalling
     * incoming result message.
     *
     * @return
     */
    @Bean
    Jaxb2Marshaller settlementResultMessageUnmarshaller() {
        Jaxb2Marshaller unmarshaller = new Jaxb2Marshaller();
        unmarshaller.setMappedClass(Result.class);
        unmarshaller.setCheckForXmlRootElement(false);
        unmarshaller.setPackagesToScan("com.huawei.tsel.cpsasync.model.result");
        /**
         * FIXME
         *
         * After testing, no need to define schemas for unmarshalling,
         * this fact can lead to simpler project setup.
         */
        unmarshaller.setSchemas(resultSchemas.getElement(), resultSchemas.getCommon(), resultSchemas.getResult());

        Map<String, Object> properties = new HashMap<>();
        properties.put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
        unmarshaller.setMarshallerProperties(properties);

        return unmarshaller;
    }

    @Bean
    IntegrationFlow pushSettlementFlow() {
        return flow -> flow.channel(pushSettlementInput())
                .enrichHeaders(specs -> specs.<SettlementSummary> headerFunction("summaryId", message -> message.getPayload().getId()))
                .gateway(pushSettlementMainFlow(), specs -> specs.requiresReply(false).errorChannel(pushSettlementError()));
    }

    /**
     * <p>An {@link IntegrationFlow} for handling submission of {@link SettlementSummary} to CPS system</p>
     *
     * @return
     */
    @Bean
    IntegrationFlow pushSettlementMainFlow() {
        return flow -> flow.<SettlementSummary> handle((payload, headers) -> {
                    LOGGER.trace("Update state of settlement summary to 'PROCESS'");
                    settlementService.setSettlementSummaryStatus(payload.getId(), SettlementStatus.PROCESS);
                    return payload;
                })
                .transform(settlementRequestTransformer())
                .handle(pushSettlementOutboundGateway())
                .<Result> handle((payload, headers) -> {
                    String summaryId = (String) headers.get("summaryId");

                    LOGGER.trace("Update state of settlement summary with id {} to 'COMPLETED'", summaryId);
                    settlementService.setSettlementSummaryStatus(summaryId, SettlementStatus.COMPLETED);

                    ResultDescriptor settlementResult = ResultDescriptor.builder()
                            .type(payload.getBody().getResultType())
                            .code(payload.getBody().getResultCode())
                            .description(payload.getBody().getResultDesc())
                            .transactionId(payload.getBody().getTransactionResult().getTransactionID())
                            .build();

                    LOGGER.trace("Update push result of settlement summary with id {} with value : ", summaryId, settlementResult);
                    settlementService.setSettlementSummaryResult(summaryId, settlementResult);

                    return MessageBuilder.withPayload(payload).copyHeaders(headers)
                            .removeHeader("summaryId")
                            .build();
                })
                .handle(message -> LOGGER.info("Publishing settlement summary with id succeed..."));
    }

    /**
     * <p>An {@link IntegrationFlow} for handling error of submission of settlement summary</p>
     *
     * @return
     */
    @Bean
    IntegrationFlow pushSettlementErrorFlow() {
        return flow -> flow.channel(pushSettlementError())
                .<MessagingException> handle((payload, headers) -> {
                    Message<?> failedMessage = payload.getFailedMessage();
                    String summaryId = (String) failedMessage.getHeaders().get("summaryId");

                    /**
                     * FIXME
                     *
                     * Must be able to handle all possible exception type, and log exception type and message into database.
                     */
                    if(payload.getCause() instanceof RestClientException) {
                        LOGGER.error("Handle 'RestClientException' error on publishing settlement summary with id {}. update state to 'FAILED'", summaryId);
                        settlementService.setSettlementSummaryStatus(summaryId, SettlementStatus.FAILED);
                    }
                    else {
                        LOGGER.error("Handle error on publishing settlement summary with id {}. update state to 'FAILED'", summaryId);
                        settlementService.setSettlementSummaryStatus(summaryId, SettlementStatus.FAILED);
                    }
                    return payload;
                })
                .handle(message -> LOGGER.info("Publishing settlement summary with id failed..."));
    }

    /**
     * <p>Utility bean type defining xml schemas related to request soap messages.
     */
    @Data
    @Component
    @ConfigurationProperties(prefix = "huawei.settlement.cpssynch-schema.request")
    static class RequestSchemaResources {
        private Resource element = new ClassPathResource("wsdl/request/CPSInterface_API_Element.xsd");

        private Resource common = new ClassPathResource("wsdl/request/CPSInterface_API_Common.xsd");

        private Resource request = new ClassPathResource("wsdl/request/CPSInterface_API_Request.xsd");

        private Resource response = new ClassPathResource("wsdl/request/CPSInterface_API_Response.xsd");
    }

    /**
     * <p>Utility bean type defining xml schemas related to response soap messages.
     */
    @Data
    @Component
    @ConfigurationProperties(prefix = "huawei.settlement.cpssynch-schema.response")
    static class ResponseSchemaResources {
        private Resource element = new ClassPathResource("wsdl/request/CPSInterface_API_Element.xsd");

        private Resource common = new ClassPathResource("wsdl/request/CPSInterface_API_Common.xsd");

        private Resource response = new ClassPathResource("wsdl/request/CPSInterface_API_Response.xsd");
    }

    /**
     * <p>Utility bean type defining xml schemas related to result soap messages.
     */
    @Data
    @Component
    @ConfigurationProperties(prefix = "huawei.settlement.cpssynch-schema.result")
    static class ResultSchemaResources {
        private Resource element = new ClassPathResource("wsdl/result/CPSInterface_API_Element.xsd");

        private Resource common = new ClassPathResource("wsdl/result/CPSInterface_API_Common.xsd");

        private Resource result = new ClassPathResource("wsdl/result/CPSInterface_API_Result.xsd");
    }
}
