package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.common.integration.RequestRetrySettings;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>Settings for settlement publishing to cps endpoint</p>
 *
 * @author zakyalvan
 * @since 26 Sep 2017
 */
@Component
@ConfigurationProperties("huawei.settlement.settle-summary")
public class SettlementPublishingSettings implements Serializable {
    @Valid
    @NotNull
    @NestedConfigurationProperty
    private final RequestRetrySettings retry = new RequestRetrySettings();
}
