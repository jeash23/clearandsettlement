package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.dto.CorrelateUidResult;

/**
 * @author zakyalvan
 * @since 31 May 2017
 */
public class CorrelateUidException extends RuntimeException {
    public CorrelateUidException() {
    }

    public CorrelateUidException(String message) {
        super(message);
    }

    public CorrelateUidException(String message, Throwable cause) {
        super(message, cause);
    }

    public CorrelateUidException(Throwable cause) {
        super(cause);
    }

    public static class NoResponseBodyException extends CorrelateUidException {

    }

    public static class CantParseResponseException extends CorrelateUidException {

    }

    public static class UnknownCorrelateCodeException extends CorrelateUidException {

    }

    /**
     *
     */
    public static class FailedCorrelateCodeException extends CorrelateUidException {
        private CorrelateUidResult.CheckStatus checkStatus;

        FailedCorrelateCodeException(CorrelateUidResult.CheckStatus checkStatus) {
            this.checkStatus = checkStatus;
        }

        CorrelateUidResult.CheckStatus getCheckStatus() {
            return checkStatus;
        }
    }
}
