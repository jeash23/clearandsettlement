package com.huawei.tsel.settlement.flow;

import com.huawei.tsel.settlement.dto.CorrelateUidRequest;
import com.huawei.tsel.settlement.dto.CorrelateUidResult;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author zakyalvan
 * @since 18 Apr 2017
 */
@Configuration
public class CorrelateUidChannels {
    @Bean
    public ExecutorChannel correlateParameters() {
        return MessageChannels.executor(correlateUidTaskExecutor()).datatype(CorrelateUidRequest.class).get();
    }

    @Bean
    ThreadPoolTaskExecutor correlateUidTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(20);
        taskExecutor.setMaxPoolSize(100);
        taskExecutor.setQueueCapacity(1000);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setAllowCoreThreadTimeOut(false);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setThreadNamePrefix("check-uid-");
        return taskExecutor;
    }

    @Bean
    public DirectChannel correlateResults() {
        return MessageChannels.direct().datatype(CorrelateUidResult.class).get();
    }

    @Bean
    public PublishSubscribeChannel correlateErrors() {
        return MessageChannels.publishSubscribe().get();
    }
}
