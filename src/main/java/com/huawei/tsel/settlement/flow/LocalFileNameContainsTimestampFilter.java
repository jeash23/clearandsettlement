package com.huawei.tsel.settlement.flow;

import com.jcraft.jsch.ChannelSftp.LsEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * <p>A {@link org.springframework.integration.file.filters.FileListFilter} which responsible for filtering
 * local file which not contains valid timestamp in filename</p>
 *
 * @author zakyalvan
 * @since 14 Sep 2017
 */
class LocalFileNameContainsTimestampFilter extends AbstractFileNameContainsTimestampFilter<File> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalFileNameContainsTimestampFilter.class);

    LocalFileNameContainsTimestampFilter(String namePattern, String timestampFormat, int timestampGroup) {
        super(namePattern, timestampFormat, timestampGroup);
    }

    @Override
    protected String extractName(File file) {
        String fileName = file.getName();
        LOGGER.trace("Extracted file name '{}' from file object {}", fileName, file);
        return fileName;
    }
}