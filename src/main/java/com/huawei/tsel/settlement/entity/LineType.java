package com.huawei.tsel.settlement.entity;

/**
 * @author zakyalvan
 * @since 18 Sep 2017
 */
public enum LineType {
    HEADER,
    TRANSACTION,
    FOOTER
}
