package com.huawei.tsel.settlement.entity;

/**
 * <p>Enumeration of {@link InterfaceFile} process rejection reason. This is represent result of validation.</p>
 *
 * @author zakyalvan
 * @since 18 Sep 2017
 */
public enum RejectionReason {
    /**
     * <p>No transaction item(s) given in interface file</p>
     */
    ZERO_TRANSACTION_PROVIDED("No transaction item(s) given in this interface file"),

    /**
     * <p>Error/s found on transaction item given in this interface file</p>
     */
    INVALID_ITEM_ENCOUNTERED("Error/s found on transaction item given in this interface file");

    private final String message;

    RejectionReason(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
