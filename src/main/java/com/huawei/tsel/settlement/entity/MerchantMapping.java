package com.huawei.tsel.settlement.entity;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Builder;
import lombok.Value;

/**
 * 
 * @author zakyalvan
 * @since 15 Aug 2017
 */
@Value
@Builder
@SuppressWarnings("serial")
public class MerchantMapping implements Serializable {
	/**
	 * Merchant id from verifone.
	 */
	@NotBlank
	String merchantId;
	
	/**
	 * Merchant code from mfs.
	 */
	@NotBlank
	String shortCode;
	
	String merchantName;
}
