package com.huawei.tsel.settlement.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Builder;
import lombok.Value;

/**
 * <p>Merchant transaction summary.
 *
 * @author zakyalvan
 * @since 7 Aug 2017
 */
@Value
@Builder
@SuppressWarnings("serial")
public class SettlementSummary implements Serializable {
    @NotBlank
    String id;

    @NotNull
    InterfaceFile interfaceFile;

    @NotBlank
    String merchantId;

    @NotBlank
    String merchantShortCode;

    String merchantName;

    @NotNull
    @Min(value = 0)
    Long transactionCount;

    @NotNull
    BigDecimal transactionAmount;

    //@NotNull
    LocalDateTime summarizedTimestamp;

    LocalDateTime settledTimestamp;

    SettlementStatus status;

    ResultDescriptor result;

    public enum SettlementStatus {
        RAW, NEW, PROCESS, COMPLETED, FAILED
    }
}
