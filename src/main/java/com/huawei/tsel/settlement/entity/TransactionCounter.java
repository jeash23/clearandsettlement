package com.huawei.tsel.settlement.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Value;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * <p>Trace of customer transaction purse counter</p>
 *
 * @author zakyalvan
 * @since 8 Aug 2017
 */
@Value
@Builder
@SuppressWarnings("serial")
public class TransactionCounter implements Serializable {
    /**
     * <p>Customer uid</p>
     */
    @NotBlank
    String customerUid;

    /**
     * <p>Last customer's purse counter value</p>
     */
    @NotNull
    Long lastPurseCounter;

    /**
     * <p>Last customer's balance</p>
     */
    @NotNull
    BigDecimal lastBalance;

    /**
     * <p>Last update of this transaction counter</p>
     */
    LocalDateTime lastUpdate;
}
