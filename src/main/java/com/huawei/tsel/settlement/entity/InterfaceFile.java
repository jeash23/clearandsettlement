package com.huawei.tsel.settlement.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Builder;
import lombok.Data;
import org.springframework.core.io.Resource;

/**
 * <p>A data transfer object represent file containing transaction to be settled into mfs system.
 *
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@Data
@Builder
@SuppressWarnings("serial")
public class InterfaceFile implements Serializable {
    /**
     * <p>Record id of file</p>
     */
    private String recordId;

    /**
     * <p>Original file name when fetched from remote file repository.</p>
     */
    private String originalName;

    /**
     * <p>{@link Resource} of file.</p>
     */
    private Resource fileResource;

    /**
     * In which directory file stored.
     */
    private String storageDirectory;

    /**
     * <p>Stored name in internal</p>.
     */
    private String internalName;

    /**
     * <p>Hex value of calculated check sum</p>
     */
    private String checksumValue;

    /**
     * <p>Processing priority of {@link InterfaceFile}</p>
     */
    private Long processingPriority;

    /**
     * <p>When this file fetched from shared (remote) directory</p>
     */
    private LocalDateTime acquiredTimestamp;

    /**
     * TODO
     *
     * Encapsulate in one value object.
     */
    private Long processedTransactionCount = 0l;
    private Long declaredTransactionCount = 0l;
    private BigDecimal processedTransactionAmount = BigDecimal.ZERO;
    private BigDecimal declaredTransactionAmount = BigDecimal.ZERO;

    /**
     * <p>State of file processing.</p>
     */
    private ProcessingState status;

    /**
     * <p>Rejection reason of interface file</p>
     */
    private RejectionReason rejectionReason;

    /**
     * <p>Version of interface file</p>
     */
    private Integer interfaceVersion;

    /**
     * <p>Enumeration of settlement file processing state.</p>
     */
    public enum ProcessingState {
        /**
         * <p>State represent file is just fetched from remote file repository</p>
         */
        NEW,

        /**
         * <p>State where validation of interface file just started but not finished yet.</p>
         */
        VALIDATING,

        /**
         * <p>State after validation of interface file completed.</p>
         */
        READY,

        /**
         * <p>State represent validation of interface failed.</p>
         */
        INVALID,

        /**
         * <p>Interface file in loading process</p>
         */
        LOADING,

        /**
         * <p>Settlement file loading already completed</p>
         */
        LOADED,

        /**
         * Settlement file in calculation of summary to be settled.
         */
        SUMMARIZING,

        /**
         * Settlement file already calculated.
         */
        SUMMARIZED,

        /**
         * <p>{@link InterfaceFile} in settlement process</p>
         */
        SETTLEMENT,

        /**
         * <p>{@link InterfaceFile} already settled</p>
         */
        SETTLED
    }
}
