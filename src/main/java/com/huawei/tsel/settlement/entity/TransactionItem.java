package com.huawei.tsel.settlement.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * <p>A data transfer object for storing offline transaction item data.<p>
 *
 * @author zakyalvan
 * @since 31 Jul 2017
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class TransactionItem implements InterfaceLine {
    /**
     * Settlement record id.
     */
    private String recordId = UUID.randomUUID().toString();

    private String interfaceFileId;

    /**
     * Uid of customer.
     */
    @NotBlank
    private String customerUid;

    /**
     * Customer msisdn, correlate using customer uid against mfs api.
     */
    private String customerMsisdn;

    /**
     * Merchant id, where customer done the transaction.
     */
    @NotBlank
    private String merchantId;

    /**
     * Merchant short code, look up by verifone's merchant id from short code mapping database.
     */
    private String merchantShortCode;

    /**
     * Terminal id of EDC, where customer done the transaction.
     */
    private String terminalId;

    /**
     * Transaction id from Verifone.
     */
    @NotBlank
    private String transactionId;

    /**
     * Transaction amount.
     */
    @NotNull
    private BigDecimal transactionAmount;

    /**
     * When transaction done.
     */
    @NotNull
    private Date transactionDate;

    /**
     * Customer latest offline balance.
     */
    @NotNull
    private BigDecimal lastBalance;

    /**
     * PC (purse transaction counter) is an integer value,
     * it starts from 0x7fffffff and gets decremented with
     * every transaction that's happening. This can be
     * useful to determine the last transaction and the last
     * remaining balance of the customer (because the
     * transaction date may not be synchronized for each
     * offline gate).
     */
    @NotNull
    private Long purseCounter;

    /**
     * When this transaction item record loaded (batch) into database.
     */
    private Date loadDate;

    /**
     * Flag determine whether this item already cleared or settled.
     */
    private boolean settled;

    /**
     * When this transaction item record settled against mfs system.
     */
    private Date settleDate;

    /**
     * <p>Original line number</p>
     */
    private Integer lineNumber;

    /**
     * <p>Original line content</p>
     */
    private String lineContent;

    /**
     * <p>Type of interface line</p>
     */
    private final LineType lineType = LineType.TRANSACTION;
}
