package com.huawei.tsel.settlement.entity;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

/**
 * <p>Type for containing information of settlement result</p>
 *
 * @author zakyalvan
 * @since 10 Aug 2017
 */
@Value
@Builder
@SuppressWarnings("serial")
public class ResultDescriptor implements Serializable {
    private Integer type;
    private String code;
    private String description;

    /**
     * Reference to cps transaction id.
     */
    private String transactionId;
}