package com.huawei.tsel.settlement.entity;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLong;

/**
 * <p>A data transfer object represent summary of {@link InterfaceFile}</p>
 *
 * @author zakyalvan
 * @since 9 Aug 2017
 */
@SuppressWarnings("serial")
public class InterfaceSummary implements Serializable {
    private final AtomicLong calculatedCount;
    private final BigDecimal calculatedAmount;
    private final AtomicLong declaredCount;
    private final BigDecimal declaredAmount;

    @Builder
    public InterfaceSummary(Long calculatedCount, BigDecimal calculatedAmount, Long declaredCount, BigDecimal declaredAmount) {
        if(calculatedCount == null) {
            this.calculatedCount = new AtomicLong(calculatedCount);
        }
        else {
            this.calculatedCount = new AtomicLong(0l);
        }

        if(calculatedAmount == null) {
            this.calculatedAmount = BigDecimal.ZERO;
        }
        else {
            this.calculatedAmount = calculatedAmount;
        }

        if(declaredCount == null) {
            this.declaredCount = new AtomicLong(declaredCount);
        }
        else {
            this.declaredCount = new AtomicLong(0l);
        }

        if(declaredAmount == null) {
            this.declaredAmount = BigDecimal.ZERO;
        }
        else {
            this.declaredAmount = declaredAmount;
        }
    }

    private Long getCalculatedCount() {
        return calculatedCount.get();
    }
    private Long incrementCalculatedCount() {
        return calculatedCount.addAndGet(1l);
    }

    public BigDecimal getCalculatedAmount() {
        return calculatedAmount;
    }


    private Long getDeclaredCount() {
        return declaredCount.get();
    }
    private void setCalculatedCount(Long value) {
        calculatedCount.set(value);
    }

    public BigDecimal getDeclaredAmount() {
        return declaredAmount;
    }
}
