package com.huawei.tsel.settlement.entity;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

/**
 * @author zakyalvan
 * @since 18 Sep 2017
 */
@Value
@Builder
@SuppressWarnings("serial")
public class InvalidLine implements Serializable {
    LineType type;
    Integer number;
    String content;
    String message;
}
