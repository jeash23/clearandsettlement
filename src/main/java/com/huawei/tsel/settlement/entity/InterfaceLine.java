package com.huawei.tsel.settlement.entity;

import java.io.Serializable;

/**
 * <p>Contract for type represent line in an integration (transaction settlement line).
 * This add for configuration convenience.
 * 
 * @author zakyalvan
 * @since 3 Aug 2017
 */
public interface InterfaceLine extends Serializable {
    /**
     * <p>Retrieve interface line number</p>
     *
     * @return
     */
    Integer getLineNumber();

    /**
     * <p>Set interface line number</p>
     *
     * @param lineNumber
     */
	void setLineNumber(Integer lineNumber);

    /**
     * <p>Retrieve original line content</p>
     *
     * @return
     */
	String getLineContent();

    /**
     * Set original line content.
     *
     * @param lineContent
     */
	void setLineContent(String lineContent);

	LineType getLineType();
}
