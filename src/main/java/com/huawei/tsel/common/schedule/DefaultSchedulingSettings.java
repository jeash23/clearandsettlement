package com.huawei.tsel.common.schedule;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zakyalvan
 * @since 5 Jun 2017
 */
@Data
@AllArgsConstructor
public class DefaultSchedulingSettings implements SchedulingProperties {
    @NotNull
    protected TriggerType triggerType;

    protected Integer triggerPeriod;
    protected Integer initialDelay;
    protected String cronExpression;
}
