package com.huawei.tsel.common.schedule;

import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.util.Assert;

import java.util.function.Supplier;

/**
 * <p>Contract for type defining scheduling time. This type is spring specific, used to produde
 * trigger to be used in integration pollers.
 *
 * @author zakyalvan
 * @since 20 Mar 2017
 */
public interface SchedulingProperties extends Supplier<Trigger> {
    /**
     * Retrieve interval of trigger, only applicable for fixed delay and fixed rate.
     *
     * @return
     */
    Integer getTriggerPeriod();

    /**
     * Retrieve initial delay of trigger, only applicable for fixed delay and fixed rate.
     *
     * @return
     */
    Integer getInitialDelay();

    /**
     *
     * @return
     */
    String getCronExpression();

    /**
     * Retrieve trigger type.
     *
     * @return
     */
    TriggerType getTriggerType();

    default Trigger get() {
        TriggerType triggerType = getTriggerType();
        Assert.notNull(triggerType, "Trigger type for schedule properties settings must not be null");

        if(triggerType == TriggerType.FIX_DELAY || triggerType == TriggerType.FIX_RATE) {
            if(!(getTriggerPeriod() != null && getTriggerPeriod() > 0)) {
                throw new IllegalStateException("Trigger type is either fixed delay or fixed rate, but given trigger interval is null or lower or equals to zero integer");
            }

            PeriodicTrigger trigger = new PeriodicTrigger(getTriggerPeriod());
            trigger.setFixedRate(getTriggerType() == TriggerType.FIX_RATE);
            if(getInitialDelay() != null && getInitialDelay() > 0) {
                trigger.setInitialDelay(getInitialDelay());
            }

            return trigger;
        }
        else {
            Assert.hasText(getCronExpression(), "Trigger type is cron, but no cron expression given");

            CronTrigger trigger = new CronTrigger(getCronExpression());
            return trigger;
        }
    }

    public enum TriggerType {
        FIX_DELAY, FIX_RATE, CRON
    }
}
