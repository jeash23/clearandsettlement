package com.huawei.tsel.common.integration;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;

/**
 * 
 * @author zakyalvan
 * @since 24 Jan 2017
 */
public interface UriSettings {
	String getScheme();
	String getHost();
	int getPort();
	String getPath();
	Map<String, Object[]> getDefaultParams();
	
	default UriComponents toUriComponents() {
		UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
				.scheme(getScheme()).host(getHost()).port(getPort()).path(getPath());
		for(String parameter : getDefaultParams().keySet()) {
			builder.queryParam(parameter, getDefaultParams().get(parameter));
		}
		return builder.build();
	}
	default URI toUri() {
		return toUriComponents().toUri();
	}
	default String toUriString() {
		return toUriComponents().toUriString();
	}
}
