package com.huawei.tsel.common.integration;

import lombok.Data;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>Settings of retry attempts for linkage check.
 *
 * @author zakyalvan
 * @since 18 May 2017
 */
@Data
public class RequestRetrySettings {
    /**
     * <p>Flag determine whether retry is enabled.
     */
    private boolean enabled = true;

    /**
     * <p>Flag whether to traverse exception cause when evaluating exception.
     */
    private boolean traverseExceptionCause = true;

    /**
     * <p>Allowed exception to be retried. By default, retryable exceptions included :
     * - http client i/o problem exception
     * - http response code error, both client or server.
     */
    @NotNull
    private Set<Class<? extends Throwable>> retryableExceptions = new HashSet<>();

    /**
     * <p>Collection of exception which are not retry-able.
     */
    @NotNull
    private Set<Class<? extends Throwable>> unretryableExceptions = new HashSet<>();

    /**
     * <p>Max retry attempts.
     */
    @Min(1)
    @Max(10)
    @NotNull
    private Integer maxAttempts = 3;

    /**
     * <p>Interval of each retry process.
     */
    @Min(100)
    @NotNull
    private Integer periodMillis = 3000;

    /**
     * <p>Type of backoff.
     */
    @NotNull
    private RetryBackoff backOffType = RetryBackoff.EXPONENTIAL;

    /**
     * <p>Only applicable if backoff policy is exponential.
     */
    @Min(1)
    @NotNull
    private Integer backOffMultiplier = 2;

    public RequestRetrySettings() {
        this(true);
    }
    public RequestRetrySettings(boolean defaultRetryHttpException) {
        if(defaultRetryHttpException) {
            this.retryableExceptions.addAll(Arrays.asList(
                    ResourceAccessException.class,
                    HttpClientErrorException.class,
                    HttpServerErrorException.class
            ));
        }
    }

    /**
     * Enumeration of backoff policy type.
     */
    public enum RetryBackoff {
        NONE, FIXED, EXPONENTIAL, RANDOM, EXPONENTIAL_RANDOM
    }
}