package com.huawei.tsel.common.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.boot.bind.RelaxedDataBinder;
import org.springframework.integration.dsl.support.GenericHandler;
import org.springframework.integration.handler.AbstractReplyProducingMessageHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * {@link GenericHandler} handle binding of {@link MultiValueMap} to target object.
 * 
 * @author zakyalvan
 * @since 31 Jan 2017
 */
public class MultiValueMapBindingHandler<T> extends AbstractReplyProducingMessageHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(MultiValueMapBindingHandler.class);
	
	/**
	 * Target bean factory.
	 */
	private Supplier<T> targetFactory;
	
	/**
	 * Customizer for data binder.
	 */
	private Consumer<DataBinder> binderCostumizer;
	
	/**
	 * Binding error translator.
	 */
	private Function<BindingResult, ? extends RuntimeException> errorTranslator;
	
	/**
	 * Flag whether to ignore data binding errors.
	 */
	private boolean ignoreBindErrors = false;
	
	public Supplier<T> getTargetFactory() {
		return targetFactory;
	}
	public MultiValueMapBindingHandler<T> setTargetFactory(Supplier<T> targetFactory) {
		this.targetFactory = targetFactory;
		return this;
	}
	
	public Consumer<DataBinder> getBinderCostumizer() {
		return binderCostumizer;
	}
	public MultiValueMapBindingHandler<T> setBinderCostumizer(Consumer<DataBinder> binderCostumizer) {
		this.binderCostumizer = binderCostumizer;
		return this;
	}
	
	public Function<BindingResult, ? extends Throwable> getErrorTranslator() {
		return errorTranslator;
	}
	public MultiValueMapBindingHandler<T> setErrorTranslator(Function<BindingResult, ? extends RuntimeException> errorTranslator) {
		this.errorTranslator = errorTranslator;
		return this;
	}
	
	public boolean isIgnoreBindErrors() {
		return ignoreBindErrors;
	}
	public void setIgnoreBindErrors(boolean ignoreBindErrors) {
		this.ignoreBindErrors = ignoreBindErrors;
	}
	
	@Override
	protected Object handleRequestMessage(Message<?> requestMessage) {
		LOGGER.trace("Handle binding of MultiValueMap ");
		if(!MultiValueMap.class.isAssignableFrom(requestMessage.getPayload().getClass())) {
			LOGGER.error("Message payload must be Spring's MultiValueMap object type");
			throw new IllegalArgumentException("Payload must be Spring's multivalue map");
		}
		
		Map<Object, String> normalized = new HashMap<>();
		((MultiValueMap<?, ?>) requestMessage.getPayload()).keySet().forEach(key -> {
			normalized.put(key, StringUtils.collectionToCommaDelimitedString( ((MultiValueMap<?, ?>) requestMessage.getPayload()).get(key)));
		});
		
		T bindingTarget = targetFactory.get();
		Assert.notNull(bindingTarget, "Binding target must not be null");
		
		DataBinder dataBinder = createBinder(bindingTarget);
		
		MutablePropertyValues proeprties = new MutablePropertyValues(normalized);
		dataBinder.bind(proeprties);
		dataBinder.validate();
		
		BindingResult bindings = dataBinder.getBindingResult();
		if(bindings.hasErrors()) {
			LOGGER.error("Errors happen on binding request parameters to target object : {}", bindings.toString());
			if(errorTranslator != null) {
				throw errorTranslator.apply(bindings);
			}
			else {
				throw new DataBindingException(bindings);
			}
		}
		
		LOGGER.trace("Binding of MultiValueMap finished -> {}", dataBinder.getTarget().getClass());
		return MessageBuilder.withPayload(dataBinder.getTarget())
				.copyHeaders(requestMessage.getHeaders())
				.build();
	}
	
	/**
	 * 
	 * @param bindingTarget
	 * @return
	 */
	private DataBinder createBinder(Object bindingTarget) {
		RelaxedDataBinder dataBinder = new RelaxedDataBinder(bindingTarget);
		if(binderCostumizer != null) {
			binderCostumizer.accept(dataBinder);
		}
		return dataBinder;
	}
}
