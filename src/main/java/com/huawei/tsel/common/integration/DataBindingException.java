package com.huawei.tsel.common.integration;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.validation.*;

import java.beans.PropertyEditor;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>Custom {@link Exception} to be thrown when a errors on binding result. This is preferred
 * than checked exception one {@link BindException} bundled by default if in Spring distribution.
 * 
 * @author zakyalvan
 * @since 31 Jan 2017
 */
@SuppressWarnings("serial")
public class DataBindingException extends RuntimeException implements BindingResult {
	private final BindingResult result;
	
	public DataBindingException(BindingResult result) {
		this.result = Objects.requireNonNull(result);
	}

	public BindingResult getResult() {
		return result;
	}

	public Object getTarget() {
		return result.getTarget();
	}

	public String getObjectName() {
		return result.getObjectName();
	}

	public Map<String, Object> getModel() {
		return result.getModel();
	}

	public void setNestedPath(String nestedPath) {
		result.setNestedPath(nestedPath);
	}

	public String getNestedPath() {
		return result.getNestedPath();
	}

	public void pushNestedPath(String subPath) {
		result.pushNestedPath(subPath);
	}

	public Object getRawFieldValue(String field) {
		return result.getRawFieldValue(field);
	}

	public PropertyEditor findEditor(String field, Class<?> valueType) {
		return result.findEditor(field, valueType);
	}

	public void popNestedPath() throws IllegalStateException {
		result.popNestedPath();
	}

	public void reject(String errorCode) {
		result.reject(errorCode);
	}

	public void reject(String errorCode, String defaultMessage) {
		result.reject(errorCode, defaultMessage);
	}

	public PropertyEditorRegistry getPropertyEditorRegistry() {
		return result.getPropertyEditorRegistry();
	}

	public void addError(ObjectError error) {
		result.addError(error);
	}

	public void reject(String errorCode, Object[] errorArgs, String defaultMessage) {
		result.reject(errorCode, errorArgs, defaultMessage);
	}

	public String[] resolveMessageCodes(String errorCode) {
		return result.resolveMessageCodes(errorCode);
	}

	public void rejectValue(String field, String errorCode) {
		result.rejectValue(field, errorCode);
	}

	public String[] resolveMessageCodes(String errorCode, String field) {
		return result.resolveMessageCodes(errorCode, field);
	}

	public void recordSuppressedField(String field) {
		result.recordSuppressedField(field);
	}

	public void rejectValue(String field, String errorCode, String defaultMessage) {
		result.rejectValue(field, errorCode, defaultMessage);
	}

	public String[] getSuppressedFields() {
		return result.getSuppressedFields();
	}

	public void rejectValue(String field, String errorCode, Object[] errorArgs, String defaultMessage) {
		result.rejectValue(field, errorCode, errorArgs, defaultMessage);
	}

	public void addAllErrors(Errors errors) {
		result.addAllErrors(errors);
	}

	public boolean hasErrors() {
		return result.hasErrors();
	}

	public int getErrorCount() {
		return result.getErrorCount();
	}

	public List<ObjectError> getAllErrors() {
		return result.getAllErrors();
	}

	public boolean hasGlobalErrors() {
		return result.hasGlobalErrors();
	}

	public int getGlobalErrorCount() {
		return result.getGlobalErrorCount();
	}

	public List<ObjectError> getGlobalErrors() {
		return result.getGlobalErrors();
	}

	public ObjectError getGlobalError() {
		return result.getGlobalError();
	}

	public boolean hasFieldErrors() {
		return result.hasFieldErrors();
	}

	public int getFieldErrorCount() {
		return result.getFieldErrorCount();
	}

	public List<FieldError> getFieldErrors() {
		return result.getFieldErrors();
	}

	public FieldError getFieldError() {
		return result.getFieldError();
	}

	public boolean hasFieldErrors(String field) {
		return result.hasFieldErrors(field);
	}

	public int getFieldErrorCount(String field) {
		return result.getFieldErrorCount(field);
	}

	public List<FieldError> getFieldErrors(String field) {
		return result.getFieldErrors(field);
	}

	public FieldError getFieldError(String field) {
		return result.getFieldError(field);
	}

	public Object getFieldValue(String field) {
		return result.getFieldValue(field);
	}

	public Class<?> getFieldType(String field) {
		return result.getFieldType(field);
	}
}
