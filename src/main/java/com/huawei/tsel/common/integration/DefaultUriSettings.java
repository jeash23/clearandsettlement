package com.huawei.tsel.common.integration;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Default implementation of {@link UriSettings}.
 *
 * @author zakyalvan
 * @since 24 Jan 2017
 */
public class DefaultUriSettings implements UriSettings {

    private String scheme = "http";
    private String host = "10.49.0.10";
    private int port = 31943;
    private String path = "payment/services/APIRequestMgrService";
    private Map<String, Object[]> defaultParams = new HashMap<>();

    public DefaultUriSettings() {}

    public DefaultUriSettings(String scheme, String host, int port, String path) {
        this.scheme = scheme;
        this.host = host;
        this.port = port;
        this.path = path;
    }

    public DefaultUriSettings(String scheme, String host, int port, String path, Map<String, Object[]> defaultParams) {
        this.scheme = scheme;
        this.host = host;
        this.port = port;
        this.path = path;
        this.defaultParams = defaultParams;
    }

    @Override
    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    @Override
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public Map<String, Object[]> getDefaultParams() {
        return defaultParams;
    }

    public void setDefaultParams(Map<String, Object[]> defaultParams) {
        this.defaultParams = defaultParams;
    }
    
    public DefaultUriSettings setDefaultParams(String key, Object... values) {
    	defaultParams.put(key, values);
    	return this;
    }

    @Override
    public String toString() {
        URI uri = toUri();
        return uri != null ? uri.toString() : "";
    }
}
