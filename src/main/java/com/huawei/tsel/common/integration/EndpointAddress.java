package com.huawei.tsel.common.integration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author zakyalvan
 * @since 4 May 2017
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class EndpointAddress implements Serializable {
    @NotBlank
    private String scheme;

    @NotBlank
    private String host;

    @NotNull
    private Integer port;

    @NotBlank
    private String path;

    /**
     * <p>Create {@link UriComponentsBuilder} object from this definition.
     *
     * @return
     */
    public UriComponentsBuilder toUriComponentsBuilder() {
        return UriComponentsBuilder.newInstance()
                .scheme(this.scheme).host(host).port(port).path(path);
    }
}
