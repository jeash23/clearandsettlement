package com.huawei.tsel.common.xmlbind;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

/**
 * @author zakyalvan
 * @since 13 Jan 2017
 */
public class DataBindingConverter {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataBindingConverter.class);

    private static final DateFormatter DATE_FORMATTER = new DateFormatter("yyyyMMddHHmmss");

    public static String printTimestamp(Date timestamp) {
        if (timestamp == null) {
            LOGGER.trace("Given timestamp is null, just return empty text");
            return "";
        }

        return DATE_FORMATTER.print(timestamp, Locale.ENGLISH);
    }

    public static Date parseTimestamp(String timestampString) {
        if (!StringUtils.hasText(timestampString)) {
            LOGGER.trace("Given timestamp string is null, just return null");
            return null;
        }

        try {
            return DATE_FORMATTER.parse(timestampString, Locale.ENGLISH);
        } catch (ParseException pe) {
            LOGGER.error("Can't parse timestamp string representation");
            throw new IllegalArgumentException("Can't parse timestamp string representation", pe);
        }
    }

    public static String printInteger(Integer integer) {
        if (integer == null) {
            return null;
        }

        return integer.toString();
    }

    public static Integer parseInteger(String integerString) {
        if (!StringUtils.hasText(integerString)) {
            return null;
        }

        try {
            return Integer.parseInt(integerString);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("String is not integer representation", e);
        }
    }
}
